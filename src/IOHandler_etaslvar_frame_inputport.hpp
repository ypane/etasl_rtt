#ifndef EXPRESSIONGRAPH_IOHANDLER_ETASLVAR_FRAME_INPUTPORT_HPP
#define EXPRESSIONGRAPH_IOHANDLER_ETASLVAR_FRAME_INPUTPORT_HPP
#include <rtt/RTT.hpp>
#include "IOHandler.hpp"
#include <expressiongraph/context.hpp>
#include <kdl/expressiontree_var.hpp>
#include <string>
#include <boost/shared_ptr.hpp>
#include <kdl/frames.hpp>

namespace KDL{

    /**
     * gets the values of the eTaSL inputchannels from an array input port.
     * No error is detected when the variables are not used within eTaSL. 
     *
     */
    class IOHandler_etaslvar_frame_inputport:
        public IOHandler {
            RTT::TaskContext*          tc;
            RTT::InputPort<KDL::Frame > inPort;
            boost::shared_ptr<Context::Ptr> ctx;
            std::string                varname;
            std::string                portname;
            std::string                portdocstring;
            KDL::Frame                 value;
            KDL::Frame                 default_value;
            VariableType<KDL::Frame>::Ptr  inps;
            bool                       derivatives; 
            int                        time_ndx;
    public:
            IOHandler_etaslvar_frame_inputport(
                RTT::TaskContext*          _tc,
                boost::shared_ptr<Context::Ptr> _ctx,
                const std::string&         _varname,
                const KDL::Frame&          _default_value,
                const std::string&         _portname,
                const std::string&         _portdocstring
            );
            virtual bool initialize();
            virtual bool configure_component();
            virtual bool attach_to_etasl();
            virtual bool verify();
            virtual bool update();
            virtual void finish();
            virtual bool detach_from_etasl();
            virtual bool cleanup_component();
            virtual int getPriorityLevel();
            virtual ~IOHandler_etaslvar_frame_inputport();
            virtual std::string getName();
    };

}//namespace KDL

#endif


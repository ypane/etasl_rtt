#ifndef RTTLUACONTEXT_HPP
#define RTTLUACONTEXT_HPP

#include <expressiongraph/context.hpp>
#include <luabind/luabind.hpp>
#include <rtt/RTT.hpp>

/**
 * class that maintains the state of the LUA interpreter.
 * This class should not be deleted or go out of scope as long as the
 * context and the expression graphs defined in this class are used.
 *
 * This version is specific to Orocos (using Orocos logging)
 */
class RTTLuaContext {
    /**
     * pointer to the context given by init(ctx), just to be sure
     * to keep a reference to ctx, such that it is not destroyed.
     */
    KDL::Context::Ptr ctx; 
public:
    typedef boost::shared_ptr<RTTLuaContext> Ptr;

    /**
     * the LUA scripting environment.
     */ 
    lua_State *L;


    RTTLuaContext();

    /**
     * executes the commands in the striing.
     * \return 0 if succesful, non-zero if there is an error. 
     */
    int executeString(const std::string& cmds);
 
    /**
     * executes the commands in the file with the given filename.
     * \return 0 if succesful, non-zero if there is an error. 
     */
    int executeFile(const std::string& filename);

    /** 
     * initialize the LUA environment with a time and ctx variable.
     * \warning typically called before executeFile/executeString
     */    
    void initContext(KDL::Context::Ptr ctx);

    /**
     * calls a LUA console for interactive sessions in your program.
     * returns false if error occurred
     * returns true if no error occurred.
     */
    bool call_console();

    /**
     * define a VariableType expression that is accessible in the script and 
     * from the C++ side.
     * \param [in] name specifies the name used in LUA
     * \param [in] depend specifies what dependencies the variable has. (See VariableType)
     */
    template<typename R>
    typename KDL::VariableType<R>::Ptr define_var(const std::string& name, const std::vector<int>& depend) {
        typename KDL::VariableType<R>::Ptr var = KDL::Variable<R>(depend);
        typename KDL::Expression<R>::Ptr tmp = var;
        luabind::globals(L)[name] = tmp;
        return var;
    }

    /**
     * define a VariableType expression that is accessible in the script and 
     * from the C++ side.
     * \param [in] name specifies the name used in LUA
     * \param [in] depend specifies what dependencies the variable has. (See VariableType)
     */
    template<typename R>
    typename KDL::VariableType<R>::Ptr define_var(const std::string& name, int startndx, int nrofderiv) {
        typename KDL::VariableType<R>::Ptr var = KDL::Variable<R>(startndx,nrofderiv);
        typename KDL::Expression<R>::Ptr tmp = var;
        luabind::globals(L)[name] = tmp;
        return var;
    }


    /**
     * executes a statement and returns its double value
     * \param [in] str  statement (e.g. function call or something like "return varname;")
     * \param [out] value that will contain the return value
     * \returns non-zero value in case of error
     */
    int executeAndReturnDouble(const std::string& str, double& value );

    /**
     * executes a statement and returns its string value
     * \param [in] str  statement (e.g. function call or something like "return varname;")
     * \param [out] value that will contain the return value
     * \returns non-zero value in case of error
     */
    int executeAndReturnString(const std::string& str, std::string& value );

    ~RTTLuaContext();
};

#endif

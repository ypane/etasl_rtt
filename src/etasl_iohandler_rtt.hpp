#ifndef EXPRESSIONGRAPH_ETASL_RTT_IOHANDLER_RTT
#define EXPRESSIONGRAPH_ETASL_RTT_IOHANDLER_RTT

#include <rtt/RTT.hpp>
#include "IOHandler.hpp"
#include "IOHandler_collection.hpp"
#include "errormsg-component.hpp"
#include <expressiongraph/context.hpp>
/**
 * \brief contains factory methods for common IOHandlers
 *
 * A class that encapsulates factory methods for the IOHandlers that
 * are common to the etasl components. It is a base class for
 * all components that use an eTaSL context (KDL::Context)
 * (controller, estimator, simulator...)
 * 
 * \warning the ctx member has to be initialized by the derived class
 *
 * usage:
 * \code{.cpp} 
 *     class my_component: public etasl_iohandler_rtt {
 *          public:
 *              my_component():etasl_iohandler_rtt(name,..)  {
 *              }
 *     }
 * \endcode 
 */
class etasl_iohandler_rtt: public RTT::TaskContext {
    protected:
   
        virtual bool check_is_preoperational(const std::string& name);
        virtual bool check_is_stopped(const std::string& name);
        inline void handler_config_failed(const std::string& vname, const std::string& iohname) {
          RTT::log(RTT::Error) << errormsg::gen_io_error(errormsg::ERROR_IO_CONFIG,errormsg::BOLDMAGENTA) << ": " << vname << " (@" << iohname << ") "  << RTT::endlog();
        }
    public:
        boost::shared_ptr<KDL::Context::Ptr>  cctx; ///<Context container that is used by the handlers
        KDL::IOHandlerCollection::Ptr    ihc; ///< collection of input handlers
        KDL::IOHandlerCollection::Ptr    ohc; ///< collection of output handlers
 
        etasl_iohandler_rtt(std::string const& name);

        /**
         * \name I/O Handlers connecting to ports
         */
        /// @{
        /**
         * \brief defines an output port of a vector of scalar output expressions
         * \param portname name of the output port
         * \param portdocstring documentation string for the output port
         * \param varnames names of the scalar output expressions that together defines the output vector
         * \warning if only one variable is given in varnames, an output port of type double will be created instead
         * of type std::vector.
         */
        virtual bool add_etaslvar_outputport( const std::string& portname, const std::string& portdocstring, const std::vector<std::string>& varnames);
        virtual bool add_etaslvar_vector_outputport( const std::string& portname, const std::string& portdocstring, const std::string& varname);
        virtual bool add_etaslvar_rotation_outputport( const std::string& portname, const std::string& portdocstring, const std::string& varname);
        virtual bool add_etaslvar_inputport( const std::string& portname, const std::string& portdocstring, const std::vector<std::string>& varnames, const std::vector<double>& default_values);
        virtual bool add_etaslvar_deriv_inputport(const std::string& portname, const std::string& portdocstring, const std::vector<std::string>& varnames);
        virtual bool add_etaslvar_frame_inputport( const std::string& portname, const std::string& portdocstring, const std::string& varname, const KDL::Frame& default_value);
        virtual bool add_etaslvar_frame_outputport( const std::string& portname, const std::string& portdocstring, const std::string& varname);
        virtual bool add_etaslvar_deriv_twist_inputport(const std::string& portname, const std::string& portdocstring, const std::string& varname);
       /// @}
        //
        /**
         * \name Operations to set/get eTaSL EXPRESSIONS (inputchannels and outputexpressions)
         */
        /// @{
        virtual bool set_etaslvar(const std::string& varname, double value);
        virtual bool set_etaslvars(const std::vector<std::string>& varnames, std::vector<double> values);
        virtual bool set_etaslvar_frame(const std::string& varname, const KDL::Frame& value);
        virtual bool set_etaslvar_vector(const std::string& varname, const KDL::Vector& value);
        virtual bool set_etaslvar_rotation(const std::string& varname, const KDL::Rotation& value);
        virtual double get_etaslvar(const std::string& varname);
        virtual KDL::Frame get_etaslvar_frame(const std::string& varname);
        virtual KDL::Vector get_etaslvar_vector(const std::string& varname);
        virtual KDL::Rotation get_etaslvar_rotation(const std::string& varname);
        /// @}
        /**
         * operations to list input and output variables (inputchannels and outputexpressions)
         */ 
        /// @{
        virtual std::vector< std::string > list_etasl_outputs_scalar();
        virtual std::vector< std::string > list_etasl_outputs_vector();
        virtual std::vector< std::string > list_etasl_outputs_rotation();
        virtual std::vector< std::string > list_etasl_outputs_frame();
        virtual std::vector< std::string > list_etasl_outputs_all();

        virtual std::vector< std::string > list_etasl_inputs_scalar();
        virtual std::vector< std::string > list_etasl_inputs_vector();
        virtual std::vector< std::string > list_etasl_inputs_rotation();
        virtual std::vector< std::string > list_etasl_inputs_frame();
        virtual std::vector< std::string > list_etasl_inputs_all();
        /// @}
        virtual ~etasl_iohandler_rtt();
  

};

#endif


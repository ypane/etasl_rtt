#include "IOHandler_etaslvar_deriv_outputport.hpp"
#include <algorithm>
namespace KDL {
    IOHandler_etaslvar_deriv_outputport::IOHandler_etaslvar_deriv_outputport(
                    RTT::TaskContext*   _tc,
                    boost::shared_ptr<Context::Ptr> _ctx,
                    boost::shared_ptr<SolverState::Ptr>    _state,
                    const std::vector<std::string>& _varnames,
                    const std::string&  _portname,
                    const std::string&  _portdocstring

            ):
        tc(_tc),
        ctx(_ctx),
        state(_state),
        varnames(_varnames),
        portname(_portname),
        portdocstring(_portdocstring),
        time_ndx(0) {
    }
    
    bool IOHandler_etaslvar_deriv_outputport::configure_component(){
        if (varnames.size()!=1) {
           tc->ports()->addPort(portname,outPort).doc(portdocstring);
        } else {
	   tc->ports()->addPort(portname,outPortScalar).doc(portdocstring);
        }
        values.resize(varnames.size());
        std::fill(values.begin(), values.end(), 0.0);
        if (varnames.size()!=1) {
           outPort.setDataSample( values );
        } else {
           double value;
           value = values[0];
           outPortScalar.setDataSample(value);
        }
        return true;
    }

    bool IOHandler_etaslvar_deriv_outputport::attach_to_etasl() {
        outp.resize(varnames.size());
        for (unsigned int i=0;i<varnames.size();++i) {
            outp[i] = (*ctx)->getOutputExpression<double>(varnames[i]); 
            if (!outp[i]) {
                errormsg=portname+" (etaslvar_deriv): eTaSL output variable '"+varnames[i]+"' does not exists";
                outp[i] = Constant(0.0);
                return false;
            } 
        }
        time_ndx = (*ctx)->getScalarNdx("time");
        return true;
    }
    
    bool IOHandler_etaslvar_deriv_outputport::initialize() {
        return true;
    }

    bool IOHandler_etaslvar_deriv_outputport::verify(){
        return true;
    }

    bool IOHandler_etaslvar_deriv_outputport::update(){
        for (unsigned int i=0;i<outp.size();++i) {
            outp[i]->value();  // always call value() before derivative() !
            values[i] = outp[i]->derivative(time_ndx);
            for (unsigned int j=0;j< (*state)->jnames_ctx_ndx.size();++j) {
                values[i] += outp[i]->derivative((*state)->jnames_ctx_ndx[j]) * (*state)->jvelocities[j];
            }
            for (unsigned int j=0;j< (*state)->fnames_ctx_ndx.size();++j) {
                values[i] += outp[i]->derivative((*state)->fnames_ctx_ndx[j]) * (*state)->fvelocities[j];
            }
        }
        if (varnames.size()!=1) {
           outPort.write( values );
        } else {
           double value;
           value = values[0];
           outPortScalar.write(value);
        }
        return true;
    }
    void IOHandler_etaslvar_deriv_outputport::finish(){
         if (varnames.size()!=1) {
            outPort.write( values );
         } else {
            double value;
            value = values[0];
            outPortScalar.write(value);
         }
    }
    bool IOHandler_etaslvar_deriv_outputport::detach_from_etasl(){
        return true;
    }
    int IOHandler_etaslvar_deriv_outputport::getPriorityLevel(){
        return 20;
    }
    bool IOHandler_etaslvar_deriv_outputport::cleanup_component(){
        tc->ports()->removePort(portname);
        return true;
    }
    IOHandler_etaslvar_deriv_outputport::~IOHandler_etaslvar_deriv_outputport(){
        tc->ports()->removePort(portname);
    }
    std::string IOHandler_etaslvar_deriv_outputport::getName() {
        return varnames[0];
    }


} // namespace KDL

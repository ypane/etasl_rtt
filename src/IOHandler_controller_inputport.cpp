#include "IOHandler_controller_inputport.hpp"
#include <iostream>

namespace KDL {
    IOHandler_controller_inputport::IOHandler_controller_inputport(
                RTT::TaskContext*          _tc,
                boost::shared_ptr<SolverState::Ptr> _state,
                std::vector<std::string>   _jointnames,
                std::string                _portname,
                std::string                _portdocstring
    ): 
        tc(_tc),
        state(_state),
        jointnames(_jointnames),
        portname(_portname),
        portdocstring(_portdocstring) {
            //std::cout << "IOHandler_controller_inputport constructed" << std::endl;
        }
    bool IOHandler_controller_inputport::initialize() {
        return true;
    }
    bool IOHandler_controller_inputport::configure_component() {
        tc->ports()->addPort(portname,inPort).doc(portdocstring);
        jvals.resize(jointnames.size(), 0.0);
        return true;
    }
    bool IOHandler_controller_inputport::attach_to_etasl() {
        return true;
    }
    bool IOHandler_controller_inputport::detach_from_etasl() {
        return true;
    }
    bool IOHandler_controller_inputport::cleanup_component() {
        tc->ports()->removePort(portname);
        return true;
    }
    bool IOHandler_controller_inputport::verify() {
        return true;
    }
    bool IOHandler_controller_inputport::update() {
        RTT::FlowStatus fs = inPort.read( jvals );
        if (fs==RTT::NewData) {
            for (unsigned int i=0;i<jointnames.size();++i) {
                std::map<std::string,int>::iterator p = (*state)->jindex.find(jointnames[i]);
                if (p!=(*state)->jindex.end()) {
                    (*state)->jvalues[p->second] = jvals[i];
                }
            } 
        }
        return true;
    }
    void IOHandler_controller_inputport::finish() {
    }
    int IOHandler_controller_inputport::getPriorityLevel() {
        return 20;
    }
    IOHandler_controller_inputport::~IOHandler_controller_inputport() {
            //std::cout << "IOHandler_controller_inputport destructed" << std::endl;
//        tc->ports()->removePort(portname);
    }
    std::string IOHandler_controller_inputport::getName() {
        return portname;
    }
} // namespace KDL

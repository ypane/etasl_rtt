/* 
 * Error definitions and color scheme for etasl_rtt component
 * Author: Enea Scioni
 *  email: <enea.scioni@kuleuven.be>
 *  2017, KU Leuven, Belgium
 *  
*/

#ifndef _ETASL_RTT_ERRORMSG_COMPONENT_HPP_
#define _ETASL_RTT_ERRORMSG_COMPONENT_HPP_

#include <string>

namespace errormsg {
  using namespace std;
  
  typedef struct {
    unsigned int id;
    string   colorcode;
  } ansicolor_t;
  
  typedef struct {
    unsigned int   id;
    string    message;
    string      event;
  } emsg_t;
  
  enum colorname{
    NONE=-1,
    RESET,
    BLACK,
    RED,
    GREEN,
    YELLOW,
    BLUE,
    MAGENTA,
    CYAN,
    WHITE,
    BOLDBLACK,
    BOLDRED,
    BOLDGREEN,
    BOLDYELLOW,
    BOLDBLUE,
    BOLDMAGENTA,
    BOLDCYAN,
    BOLDWHITE
  };
  
  static const ansicolor_t colormap[] = {
    { .id=0,  .colorcode="\033[0m"  },
    { .id=1,  .colorcode="\033[30m" },
    { .id=2,  .colorcode="\033[31m" },
    { .id=3,  .colorcode="\033[32m" },
    { .id=4,  .colorcode="\033[33m" },
    { .id=5,  .colorcode="\033[34m" },
    { .id=6,  .colorcode="\033[35m" },
    { .id=7,  .colorcode="\033[36m" },
    { .id=8,  .colorcode="\033[37m" },
    { .id=9,  .colorcode="\033[1m\033[30m" },
    { .id=10, .colorcode="\033[1m\033[31m" },
    { .id=11, .colorcode="\033[1m\033[32m" },
    { .id=12, .colorcode="\033[1m\033[33m" },
    { .id=13, .colorcode="\033[1m\033[34m" },
    { .id=14, .colorcode="\033[1m\033[35m" },
    { .id=15, .colorcode="\033[1m\033[36m" },
    { .id=16, .colorcode="\033[1m\033[37m" }
  };
  
  static const emsg_t etasl_rtt_errors[] = {
    { .id=0, .message="unknown error",                           .event="e_error"},
    { .id=1, .message=" valid only in the PreOperational state", .event="e_error_state"},
    { .id=2, .message=" valid only in the Stopped state",        .event="e_error_state"},
    { .id=3, .message=" valid only if the eTaSL has been initialized (use ':initialize()')",        .event="e_error_state"},
    { .id=4, .message=" called before the loading of a valid eTaSL specification (use ':readTaskSpecificationFile()')",        .event="e_error_state"}
  };
  
  enum ertt {
    ERROR_UNKNOWN=0,
    ERROR_PREOPERATIONAL,
    ERROR_STOPPED,
    ERROR_INIT,
    ERROR_ELOAD
  };
  
  static const emsg_t iohandlers_errors[] = {
    { .id=0, .message="failed to configure RTT/Component interface",.event="e_io_config"},
    { .id=1, .message="failed to attach an handler to the eTaSL specification",  .event="e_io_attach"},
    { .id=2, .message="an handler failed to initialize",            .event="e_io_init"},
    { .id=3, .message="an handler is not verified",                 .event="e_io_verify"},
    { .id=4, .message="an hander failed on update",                 .event="e_io_update"},
    { .id=5, .message="error on detaching an handler from the eTaSL specification",   .event="e_io_detach"},
    { .id=6, .message="failure during cleanup RTT/Component",       .event="e_io_cleanup"}
  };
  
  enum iohe{
    ERROR_IO_CONFIG,
    ERROR_IO_ATTACH,
    ERROR_IO_INIT,
    ERROR_IO_VERIFY,
    ERROR_IO_UPDATE,
    ERROR_IO_DETACH,
    ERROR_IO_CLEANUP
  };
  
  /*
   * Utility functions
   */
  string colorme(enum colorname color, const string& str) {
    return colormap[color].colorcode+str+colormap[RESET].colorcode;
  }
  
  string gen_rtt_error(enum ertt id, enum colorname color=NONE) {
    const emsg_t* err = &etasl_rtt_errors[id];
    if (color==NONE)
      return err->message;
    return colorme(color,err->message);
  }
  
  string gen_io_error(enum iohe etype, enum colorname color=NONE) {
    const emsg_t* err = &iohandlers_errors[etype];
    if (color==NONE)
      return err->message;
    return colorme(color,err->message);
  }
}


#endif
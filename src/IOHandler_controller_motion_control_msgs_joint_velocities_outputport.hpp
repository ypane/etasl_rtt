#ifndef EXPRESSIONGRAPH_IOHANDLER_CONTROLLER_MOTION_CONTROL_MSGS_JOINT_VELOCITIES_OUTPUTPORT_HPP
#define EXPRESSIONGRAPH_IOHANDLER_CONTROLLER_MOTION_CONTROL_MSGS_JOINT_VELOCITIES_OUTPUTPORT_HPP

#include <rtt/RTT.hpp>
#include "IOHandler.hpp"
#include <expressiongraph/context.hpp>
#include <string>
#include <boost/shared_ptr.hpp>
#include "solver_state.hpp"
#include <motion_control_msgs/JointVelocities.h>
#include <ros/ros.h>

namespace KDL{

    /**
     * gets the joint velocity output of the controller and send it to an Orocos output port.
     * The port will be a JointState mesasge and the joints will be in the order as given
     * by jointnames.  The joints not present in jointnames will not be present at the port.
     * If you add non-existing names to jointnames, the corresponding value is always zero.
     * (such that there are no problems when a joint has disappeared from the state because of 
     * optimizations of the execution of the eTaSL specification).
     */
    class IOHandler_controller_motion_control_msgs_joint_velocities_outputport:
        public IOHandler {
            RTT::TaskContext*          tc;
            RTT::OutputPort<motion_control_msgs::JointVelocities> outPort;
            boost::shared_ptr<SolverState::Ptr> state;
            std::vector<std::string>   jointnames;
            std::string                portname;
            std::string                portdocstring;
            motion_control_msgs::JointVelocities jv;
    public:
            typedef boost::shared_ptr<IOHandler> Ptr;

            IOHandler_controller_motion_control_msgs_joint_velocities_outputport(
                    RTT::TaskContext*   _tc,
                    boost::shared_ptr<SolverState::Ptr>  _state,
                    const std::vector<std::string>& _jointnames,
                    const std::string&  _portname,
                    const std::string&  _portdocstring
            );
            virtual bool initialize();
            virtual bool configure_component();
            virtual bool attach_to_etasl();
            virtual bool detach_from_etasl();
            virtual bool cleanup_component();
            virtual bool verify();
            virtual bool update();
            virtual void finish();
            virtual int getPriorityLevel();
            virtual ~IOHandler_controller_motion_control_msgs_joint_velocities_outputport();
            virtual std::string getName();            
    };

}//namespace KDL

#endif


#include "IOHandler_etaslvar_frame_outputport.hpp"
#include <algorithm>
namespace KDL {
    IOHandler_etaslvar_frame_outputport::IOHandler_etaslvar_frame_outputport(
                    RTT::TaskContext*   _tc,
                    boost::shared_ptr<Context::Ptr>    _ctx,
                    const std::string&  _varname,
                    const std::string&  _portname,
                    const std::string&  _portdocstring

            ):
        tc(_tc),
        ctx(_ctx),
        varname(_varname),
        portname(_portname),
        portdocstring(_portdocstring) {
    }

    bool IOHandler_etaslvar_frame_outputport::configure_component() {
        tc->ports()->addPort(portname,outPort).doc(portdocstring);
        outPort.setDataSample( value );
        return true;
    }
    
     bool IOHandler_etaslvar_frame_outputport::attach_to_etasl() {
        outp = (*ctx)->getOutputExpression<KDL::Frame>(varname); 
        if (!outp) {
                errormsg=portname+" (etaslvar_frame): eTaSL output variable '"+ varname +"' does not exists";
                outp = Constant(KDL::Frame::Identity());
                return false;
        } 
        return true;
    }
    
    bool IOHandler_etaslvar_frame_outputport::initialize() {
        return true;
    }

    bool IOHandler_etaslvar_frame_outputport::verify(){
        return true;
    }

    bool IOHandler_etaslvar_frame_outputport::update(){
        value = outp->value();
        outPort.write( value );
        return true;
    }

    void IOHandler_etaslvar_frame_outputport::finish(){
        outPort.write( value );
    }

    int IOHandler_etaslvar_frame_outputport::getPriorityLevel(){
        return 20;
    }
    bool IOHandler_etaslvar_frame_outputport::detach_from_etasl() {
      return true;
    }
    bool IOHandler_etaslvar_frame_outputport::cleanup_component() {
      tc->ports()->removePort(portname);
      return true;
    }

    IOHandler_etaslvar_frame_outputport::~IOHandler_etaslvar_frame_outputport(){
    }
    std::string IOHandler_etaslvar_frame_outputport::getName() {
        return varname;
    }
} // namespace KDL

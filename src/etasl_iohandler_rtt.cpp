#include <rtt/RTT.hpp>
#include "IOHandler.hpp"
#include "etasl_iohandler_rtt.hpp"

#include "IOHandler_etaslvar_outputport.hpp"
#include "IOHandler_etaslvar_vector_outputport.hpp"
#include "IOHandler_etaslvar_rotation_outputport.hpp"
#include "IOHandler_etaslvar_inputport.hpp"
#include "IOHandler_etaslvar_deriv_inputport.hpp"
#include "IOHandler_etaslvar_frame_inputport.hpp"
#include "IOHandler_etaslvar_frame_outputport.hpp"
#include "IOHandler_etaslvar_deriv_twist_inputport.hpp"

using namespace KDL;

etasl_iohandler_rtt::etasl_iohandler_rtt( std::string const& name ):
    RTT::TaskContext(name, PreOperational),
    cctx(new Context::Ptr()),
    ihc(new IOHandlerCollection() ),
    ohc(new IOHandlerCollection() )

{
     RTT::log(RTT::Info) << "etasl_iohandler_rtt constructed" << RTT::endlog();
    // factory operations for IOhandlers:
    this->addOperation("add_etaslvar_outputport",&etasl_iohandler_rtt::add_etaslvar_outputport,this,RTT::OwnThread)
        .doc("adds an IOHandler that puts the an eTaSL output scalar expression on an outputport")
        .arg("portname","name of the port")
        .arg("portdocstring","documentation for the port")
        .arg("varnames","list of variable names describing the contents of the outputport");
    this->addOperation("add_etaslvar_vector_outputport",&etasl_iohandler_rtt::add_etaslvar_vector_outputport,this,RTT::OwnThread)
        .doc("adds an IOHandler that puts the an eTaSL output vectorexpression on an outputport")
        .arg("portname","name of the port")
        .arg("portdocstring","documentation for the port")
        .arg("varname","etasl variable name to put on the outputport");
    this->addOperation("add_etaslvar_rotation_outputport",&etasl_iohandler_rtt::add_etaslvar_rotation_outputport,this,RTT::OwnThread)
        .doc("adds an IOHandler that puts the an eTaSL output expression on an outputport")
        .arg("portname","name of the port")
        .arg("portdocstring","documentation for the port")
        .arg("varname","etasl variable name to put on the outputport");

    this->addOperation("add_etaslvar_inputport",&etasl_iohandler_rtt::add_etaslvar_inputport,this,RTT::OwnThread)
        .doc("adds an IOHandler that gets values from a port into eTaSL")
        .arg("portname","name of the port")
        .arg("portdocstring","documentation for the port")
        .arg("varnames","list of variable names describing the contents of the inputport")
        .arg("defaultvalues","list of values describing the default value (can be empty)");
    this->addOperation("add_etaslvar_deriv_inputport",&etasl_iohandler_rtt::add_etaslvar_deriv_inputport,this,RTT::OwnThread)
        .doc("adds an IOHandler that gets derivative values from a port into eTaSL")
        .arg("portname","name of the port")
        .arg("portdocstring","documentation for the port")
        .arg("varnames","list of variable names describing the contents of the inputport");
    this->addOperation("add_etaslvar_frame_inputport",&etasl_iohandler_rtt::add_etaslvar_frame_inputport,this,RTT::OwnThread)
        .doc("adds an IOHandler that gets values from a port into eTaSL")
        .arg("portname","name of the port")
        .arg("portdocstring","documentation for the port")
        .arg("varname","variable describing the contents of the inputport")
        .arg("defaultvalue","value describing the default value (can be empty)");
    this->addOperation("add_etaslvar_frame_outputport",&etasl_iohandler_rtt::add_etaslvar_frame_outputport,this,RTT::OwnThread)
        .doc("adds an IOHandler that gets values of type Frame from eTaSL to an outputport")
        .arg("portname","name of the port")
        .arg("portdocstring","documentation for the port")
        .arg("varname","variable describing the contents of the outputport");
    this->addOperation("add_etaslvar_deriv_twist_inputport",&etasl_iohandler_rtt::add_etaslvar_deriv_twist_inputport,this,RTT::OwnThread)
        .doc("adds an IOHandler that gets derivative values from a port into eTaSL")
        .arg("portname","name of the port")
        .arg("portdocstring","documentation for the port")
        .arg("varname","variable name describing the contents of the inputport");

    this->addOperation("set_etaslvar",&etasl_iohandler_rtt::set_etaslvar,this,RTT::OwnThread)
        .doc("sets the value of an eTaSL input variable")
        .arg("varname","name of the variable")
        .arg("value","value to be used");
    this->addOperation("set_etaslvars",&etasl_iohandler_rtt::set_etaslvars,this,RTT::OwnThread)
        .doc("sets the values of eTaSL input variables")
        .arg("varnames","names of the variables")
        .arg("values","values to be used");
    this->addOperation("set_etaslvar_frame",&etasl_iohandler_rtt::set_etaslvar_frame,this,RTT::OwnThread)
        .doc("sets the value of an eTaSL input variable of type KDL::Frame")
        .arg("varname","name of the variable")
        .arg("value","value to be used");
    this->addOperation("set_etaslvar_rotation",&etasl_iohandler_rtt::set_etaslvar_rotation,this,RTT::OwnThread)
        .doc("sets the value of an eTaSL input variable of type KDL::Rotation")
        .arg("varname","name of the variable")
        .arg("value","value to be used");
    this->addOperation("set_etaslvar_vector",&etasl_iohandler_rtt::set_etaslvar_vector,this,RTT::OwnThread)
        .doc("sets the value of an eTaSL input variable of type KDL::Vector")
        .arg("varname","name of the variable")
        .arg("value","value to be used");



    this->addOperation("get_etaslvar",&etasl_iohandler_rtt::get_etaslvar,this,RTT::OwnThread)
        .doc("gets the value of an eTaSL output variable of type double")
        .arg("varname","name of the variable");
    this->addOperation("get_etaslvar_frame",&etasl_iohandler_rtt::get_etaslvar_frame,this,RTT::OwnThread)
        .doc("gets the value of an eTaSL output variable of type Frame")
        .arg("varname","name of the variable");
    this->addOperation("get_etaslvar_vector",&etasl_iohandler_rtt::get_etaslvar_vector,this,RTT::OwnThread)
        .doc("gets the value of an eTaSL output variable of type Vector")
        .arg("varname","name of the variable");
    this->addOperation("get_etaslvar_rotation",&etasl_iohandler_rtt::get_etaslvar_rotation,this,RTT::OwnThread)
        .doc("gets the value of an eTaSL output variable")
        .arg("varname","name of the variable");

    this->addOperation("list_etasl_outputs_scalar",&etasl_iohandler_rtt::list_etasl_outputs_scalar,this,RTT::OwnThread)
        .doc("lists all scalar etasl output variables and returns them as a vector of strings");
    this->addOperation("list_etasl_outputs_vector",&etasl_iohandler_rtt::list_etasl_outputs_vector,this,RTT::OwnThread)
        .doc("lists all vector etasl output variables and returns them as a vector of strings");
    this->addOperation("list_etasl_outputs_rotation",&etasl_iohandler_rtt::list_etasl_outputs_rotation,this,RTT::OwnThread)
        .doc("lists all rotation etasl output variables and returns them as a vector of strings");
    this->addOperation("list_etasl_outputs_frame",&etasl_iohandler_rtt::list_etasl_outputs_frame,this,RTT::OwnThread)
        .doc("lists all frame etasl output variables and returns them as a vector of strings");
    this->addOperation("list_etasl_outputs_all",&etasl_iohandler_rtt::list_etasl_outputs_all,this,RTT::OwnThread)
        .doc("lists all etasl output variables and returns them as a vector of strings");
 

    this->addOperation("list_etasl_inputs_scalar",&etasl_iohandler_rtt::list_etasl_inputs_scalar,this,RTT::OwnThread)
        .doc("lists all scalar etasl input variables and returns them as a vector of strings");
    this->addOperation("list_etasl_inputs_vector",&etasl_iohandler_rtt::list_etasl_inputs_vector,this,RTT::OwnThread)
        .doc("lists all vector etasl input variables and returns them as a vector of strings");
    this->addOperation("list_etasl_inputs__rotation",&etasl_iohandler_rtt::list_etasl_inputs_rotation,this,RTT::OwnThread)
        .doc("lists all rotation etasl input variables and returns them as a vector of strings");
    this->addOperation("list_etasl_inputs_frame",&etasl_iohandler_rtt::list_etasl_inputs_frame,this,RTT::OwnThread)
        .doc("lists all frame etasl input variables and returns them as a vector of strings");
    this->addOperation("list_etasl_inputs_all",&etasl_iohandler_rtt::list_etasl_inputs_all,this,RTT::OwnThread)
        .doc("lists all etasl input variables and returns them as a vector of strings");
 
} 

bool etasl_iohandler_rtt::check_is_preoperational(const std::string& name) {
    if ( this->getTaskState()!=PreOperational ) {
        RTT::log(RTT::Error) << name << errormsg::gen_rtt_error(errormsg::ERROR_PREOPERATIONAL) << RTT::endlog();
        return false;
    }
    return true;
}

bool etasl_iohandler_rtt::check_is_stopped(const std::string& name) {
    if ( this->getTaskState()!=Stopped ) {
        RTT::log(RTT::Error) << name << errormsg::gen_rtt_error(errormsg::ERROR_STOPPED) << RTT::endlog();
        return false;
    }
    return true;
}

bool etasl_iohandler_rtt::add_etaslvar_outputport( const std::string& portname, const std::string& portdocstring, const std::vector<std::string>& varnames) {
    /*if (this->getTaskState()!=PreOperational) {
        RTT::log(RTT::Error) << "add_etasl_outputport() can only be used in the PreOperational state"<<RTT::endlog();
        return false;
    }*/
    if (!check_is_preoperational("add_etaslvar_outputport")) return false;
    IOHandler::Ptr h( new KDL::IOHandler_etaslvar_outputport( this, cctx, varnames, portname, portdocstring) );
    if (h->configure_component() ) {
        ohc->addHandler( h );
        return true;
    } else {
        RTT::log(RTT::Error) << "add_etaslvar_outputport() failed to initialize"<<RTT::endlog();
        return false;
    }
}

bool etasl_iohandler_rtt::add_etaslvar_vector_outputport( const std::string& portname, const std::string& portdocstring, const std::string& varname) {
    if (!check_is_preoperational("add_etaslvar_vector_outputport")) return false;
    IOHandler::Ptr h( new KDL::IOHandler_etaslvar_vector_outputport( this, cctx, varname, portname, portdocstring) );
    if (h->configure_component() ) {
        ohc->addHandler( h );
        return true;
    }
    handler_config_failed(portname,"add_etaslvar_vector_outputport");
    return false;
}

bool etasl_iohandler_rtt::add_etaslvar_rotation_outputport( const std::string& portname, const std::string& portdocstring, const std::string& varname) {
    if (!check_is_preoperational("add_etaslvar_rotation_outputport")) return false;
    IOHandler::Ptr h( new KDL::IOHandler_etaslvar_rotation_outputport( this, cctx, varname, portname, portdocstring) );
    if (h->configure_component() ) {
        ohc->addHandler( h );
        return true;
    }
    handler_config_failed(portname,"add_etaslvar_rotation_outputport");
    return false;
}


bool etasl_iohandler_rtt::add_etaslvar_inputport( 
    const std::string& portname, 
    const std::string& portdocstring, 
    const std::vector<std::string>& varnames, 
    const std::vector<double>& default_values) {
    if (!check_is_preoperational("add_etaslvar_inputport")) return false;
    IOHandler::Ptr h( new KDL::IOHandler_etaslvar_inputport( this, cctx, varnames, default_values,portname, portdocstring) );
    if (h->configure_component() ) {
        ihc->addHandler( h );
        return true;
    } else {
        RTT::log(RTT::Error) << "add_etaslvar_inputport() failed to initialize"<<RTT::endlog();
        return false;
    }
}

bool etasl_iohandler_rtt::add_etaslvar_deriv_inputport(const std::string& portname, 
                                             const std::string& portdocstring, 
                                             const std::vector<std::string>& varnames) {
    if (!check_is_preoperational("add_etaslvar_deriv_inputport")) return false;
    IOHandler::Ptr h( new KDL::IOHandler_etaslvar_deriv_inputport( this, cctx, varnames, portname, portdocstring) );
    if (h->configure_component() ) {
        ihc->addHandler( h );
        return true;
    }
    handler_config_failed(portname,"add_etaslvar_deriv_inputport");
    return false;
}

bool etasl_iohandler_rtt::add_etaslvar_frame_inputport( 
    const std::string& portname, 
    const std::string& portdocstring, 
    const std::string& varname, 
    const KDL::Frame& default_value) {
    if (!check_is_preoperational("add_etaslvar_frame_inputport")) return false;
    IOHandler::Ptr h( new KDL::IOHandler_etaslvar_frame_inputport( this, cctx, varname, default_value,portname, portdocstring) );
    if (h->configure_component() ) {
        ihc->addHandler( h );
        return true;
    }
    handler_config_failed(portname,"add_etaslvar_frame_inputport");
    return false;
}

bool etasl_iohandler_rtt::add_etaslvar_frame_outputport( const std::string& portname, const std::string& portdocstring, const std::string& varname) {
    if (!check_is_preoperational("add_etaslvar_frame_outputport")) return false;
    IOHandler::Ptr h( new KDL::IOHandler_etaslvar_frame_outputport( this, cctx, varname, portname, portdocstring) );
    if (h->configure_component() ) {
        ohc->addHandler( h );
        return true;
    }
    handler_config_failed(portname,"add_etaslvar_frame_outputport");
    return false;
}


bool etasl_iohandler_rtt::add_etaslvar_deriv_twist_inputport(const std::string& portname, 
                                             const std::string& portdocstring, 
                                             const std::string& varname) {
    if (!check_is_preoperational("add_etaslvar_deriv_twist_inputport")) return false;
    IOHandler::Ptr h( new KDL::IOHandler_etaslvar_deriv_twist_inputport( this, cctx, varname, portname, portdocstring) );
    if (h->configure_component() ) {
        ihc->addHandler( h );
        return true;
    }
    handler_config_failed(portname,"add_etaslvar_deriv_twist_inputport");
    return false;
}
   
//-----------------------------------------
bool etasl_iohandler_rtt::set_etaslvar(const std::string& varname, double value) {
    VariableType<double>::Ptr v = (*cctx)->getInputChannel<double>(varname);
    if (!v) {
        RTT::log(RTT::Error)<<"set_etaslvar: eTaSL input variable '"<< varname <<"' does not exists or has the wrong type" << RTT::endlog();
	return false;
    } else {
        v->setValue(value);        
	return true;
    }
}

bool etasl_iohandler_rtt::set_etaslvars(const std::vector<std::string>& varnames, std::vector<double> values) {
  bool retval = false;
  for (int i=0; i< varnames.size(); ++i)
  {
    retval = set_etaslvar(varnames[i], values[i]);
    if (!retval)
      return false;
  }
  return true;
}

bool etasl_iohandler_rtt::set_etaslvar_frame(const std::string& varname, const KDL::Frame& value) {
    VariableType<KDL::Frame>::Ptr v = (*cctx)->getInputChannel<KDL::Frame>(varname);
    if (!v) {
        RTT::log(RTT::Error)<<"set_etaslvar: eTaSL input variable '"<< varname <<"' does not exists or has the wrong type" << RTT::endlog();
    } else {
        v->setValue(value);        
    }
}
    
bool etasl_iohandler_rtt::set_etaslvar_vector(const std::string& varname, const KDL::Vector& value) {
    VariableType<KDL::Vector>::Ptr v = (*cctx)->getInputChannel<KDL::Vector>(varname);
    if (!v) {
        RTT::log(RTT::Error)<<"set_etaslvar: eTaSL input variable '"<< varname <<"' does not exists or has the wrong type" << RTT::endlog();
    } else {
        v->setValue(value);        
    }
}
    
bool etasl_iohandler_rtt::set_etaslvar_rotation(const std::string& varname, const KDL::Rotation& value) {
    VariableType<KDL::Rotation>::Ptr v = (*cctx)->getInputChannel<KDL::Rotation>(varname);
    if (!v) {
        RTT::log(RTT::Error)<<"set_etaslvar: eTaSL input variable '"<< varname <<"' does not exists or has the wrong type" << RTT::endlog();
    } else {
        v->setValue(value);        
    }
}



//-----------------------------------------
double etasl_iohandler_rtt::get_etaslvar(const std::string& varname) {
    Expression<double>::Ptr e = (*cctx)->getOutputExpression<double>(varname); 
    if (!e) {
        RTT::log(RTT::Error)<<"get_etaslvar: eTaSL output variable '"<< varname <<"' does not exists or has the wrong type" << RTT::endlog();
        return 0.0;
    } else {
        return e->value();
    }
}

KDL::Frame etasl_iohandler_rtt::get_etaslvar_frame(const std::string& varname) {
    Expression<Frame>::Ptr e = (*cctx)->getOutputExpression<Frame>(varname); 
    if (!e) {
        RTT::log(RTT::Error)<<"get_etaslvar: eTaSL output variable '"<< varname <<"' does not exists or has the wrong type" << RTT::endlog();
        return Frame::Identity();
    } else {
        return e->value();
    }
}

KDL::Vector etasl_iohandler_rtt::get_etaslvar_vector(const std::string& varname) {
    Expression<Vector>::Ptr e = (*cctx)->getOutputExpression<Vector>(varname); 
    if (!e) {
        RTT::log(RTT::Error)<<"get_etaslvar: eTaSL output variable '"<< varname <<"' does not exists or has the wrong type" << RTT::endlog();
        return Vector::Zero();
    } else {
        return e->value();
    }
}

KDL::Rotation etasl_iohandler_rtt::get_etaslvar_rotation(const std::string& varname) {
    Expression<Rotation>::Ptr e = (*cctx)->getOutputExpression<Rotation>(varname); 
    if (!e) {
        RTT::log(RTT::Error)<<"get_etaslvar: eTaSL output variable '"<< varname <<"' does not exists or has the wrong type" << RTT::endlog();
        return Rotation::Identity();
    } else {
        return e->value();
    }
}
//-----------------------------------------
std::vector< std::string > etasl_iohandler_rtt::list_etasl_outputs_scalar() {
    std::vector< std::string > list;
    Context::OutputVarMap::iterator it;
    for (it=(*cctx)->output_vars.begin(); it!= (*cctx)->output_vars.end(); ++it) {
        Expression<double>::Ptr p=boost::dynamic_pointer_cast< Expression<double> >(it->second);
        if (p) list.push_back( it->first );
    }
    return list; 
}

std::vector< std::string > etasl_iohandler_rtt::list_etasl_outputs_vector() {
    std::vector< std::string > list;
    Context::OutputVarMap::iterator it;
    for (it=(*cctx)->output_vars.begin(); it!= (*cctx)->output_vars.end(); ++it) {
        Expression<Vector>::Ptr p=boost::dynamic_pointer_cast< Expression<Vector> >(it->second);
        if (p) list.push_back( it->first );
    }
    return list; 
}


std::vector< std::string > etasl_iohandler_rtt::list_etasl_outputs_rotation() {
    std::vector< std::string > list;
    Context::OutputVarMap::iterator it;
    for (it=(*cctx)->output_vars.begin(); it!= (*cctx)->output_vars.end(); ++it) {
        Expression<Rotation>::Ptr p=boost::dynamic_pointer_cast< Expression<Rotation> >(it->second);
        if (p) list.push_back( it->first );
    }
    return list; 
}

std::vector< std::string > etasl_iohandler_rtt::list_etasl_outputs_frame() {
    std::vector< std::string > list;
    Context::OutputVarMap::iterator it;
    for (it=(*cctx)->output_vars.begin(); it!= (*cctx)->output_vars.end(); ++it) {
        Expression<Frame>::Ptr p=boost::dynamic_pointer_cast< Expression<Frame> >(it->second);
        if (p) list.push_back( it->first );
    }
    return list; 
}

std::vector< std::string > etasl_iohandler_rtt::list_etasl_outputs_all() {
    std::vector< std::string > list;
    Context::OutputVarMap::iterator it;
    for (it=(*cctx)->output_vars.begin(); it!= (*cctx)->output_vars.end(); ++it) {
        list.push_back( it->first );
    }
    return list; 
}


//-----------------------------------------



std::vector< std::string > etasl_iohandler_rtt::list_etasl_inputs_scalar() {
    std::vector< std::string > list;
    Context::InputVarMap::iterator it;
    for (it=(*cctx)->input_vars.begin(); it!= (*cctx)->input_vars.end(); ++it) {
            if (it->second.type() == typeid(typename VariableType<double>::Ptr) ) {
                list.push_back( it->first );
            }
    }
    return list; 
}
std::vector< std::string > etasl_iohandler_rtt::list_etasl_inputs_vector() {
    std::vector< std::string > list;
    Context::InputVarMap::iterator it;
    for (it=(*cctx)->input_vars.begin(); it!= (*cctx)->input_vars.end(); ++it) {
            if (it->second.type() == typeid(typename VariableType<Vector>::Ptr) ) {
                list.push_back( it->first );
            }
    }
    return list; 
}
std::vector< std::string > etasl_iohandler_rtt::list_etasl_inputs_rotation() {
    std::vector< std::string > list;
    Context::InputVarMap::iterator it;
    for (it=(*cctx)->input_vars.begin(); it!= (*cctx)->input_vars.end(); ++it) {
            if (it->second.type() == typeid(typename VariableType<Rotation>::Ptr) ) {
                list.push_back( it->first );
            }
    }
    return list; 
}
std::vector< std::string > etasl_iohandler_rtt::list_etasl_inputs_frame() {
    std::vector< std::string > list;
    Context::InputVarMap::iterator it;
    for (it=(*cctx)->input_vars.begin(); it!= (*cctx)->input_vars.end(); ++it) {
            if (it->second.type() == typeid(typename VariableType<Frame>::Ptr) ) {
                list.push_back( it->first );
            }
    }
    return list; 
}
std::vector< std::string > etasl_iohandler_rtt::list_etasl_inputs_all() {
    std::vector< std::string > list;
    Context::InputVarMap::iterator it;
    for (it=(*cctx)->input_vars.begin(); it!= (*cctx)->input_vars.end(); ++it) {
        list.push_back( it->first );
    }
    return list; 
}


etasl_iohandler_rtt::~etasl_iohandler_rtt() {
     RTT::log(RTT::Info) << "etasl_iohandler_rtt destructed" << RTT::endlog();
}


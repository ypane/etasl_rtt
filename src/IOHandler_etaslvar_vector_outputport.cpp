#include "IOHandler_etaslvar_vector_outputport.hpp"
#include <algorithm>
namespace KDL {
    IOHandler_etaslvar_vector_outputport::IOHandler_etaslvar_vector_outputport(
                    RTT::TaskContext*   _tc,
                    boost::shared_ptr<Context::Ptr> _ctx,
                    const std::string&  _varname,
                    const std::string&  _portname,
                    const std::string&  _portdocstring

            ):
        tc(_tc),
        ctx(_ctx),
        varname(_varname),
        portname(_portname),
        portdocstring(_portdocstring) {
    }
    
    bool IOHandler_etaslvar_vector_outputport::configure_component() {
        tc->ports()->addPort(portname,outPort).doc(portdocstring);
        outPort.setDataSample( value );
        return true;
    }

    bool IOHandler_etaslvar_vector_outputport::attach_to_etasl() {
        outp = (*ctx)->getOutputExpression<KDL::Vector>(varname); 
        if (!outp) {
                errormsg = "eTaSL output variable '"+varname+"' does not exists or does not have expression_vector type";
                outp = Constant(KDL::Vector::Zero());
                return false;
        } 
        return true;
    }
    
    bool IOHandler_etaslvar_vector_outputport::initialize() {
        return true;
    }

    bool IOHandler_etaslvar_vector_outputport::verify(){
        return true;
    }

    bool IOHandler_etaslvar_vector_outputport::update(){
        value = outp->value();
        outPort.write( value );
        return true;
    }

    void IOHandler_etaslvar_vector_outputport::finish(){
        outPort.write( value );
    }
    bool IOHandler_etaslvar_vector_outputport::detach_from_etasl() {
        return true;
    }

    bool IOHandler_etaslvar_vector_outputport::cleanup_component() {
      tc->ports()->removePort(portname);
      return true;
    }
    int IOHandler_etaslvar_vector_outputport::getPriorityLevel(){
        return 20;
    }

    IOHandler_etaslvar_vector_outputport::~IOHandler_etaslvar_vector_outputport(){
        tc->ports()->removePort(portname);
    }
    std::string IOHandler_etaslvar_vector_outputport::getName() {
        return varname;
    }
} // namespace KDL

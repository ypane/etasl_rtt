#ifndef EXPRESSIONGRAPH_ETASL_RTT_SOLVER_STATE
#define EXPRESSIONGRAPH_ETASL_RTT_SOLVER_STATE

#include <expressiongraph/solver.hpp>
#include <boost/shared_ptr.hpp>
#include <vector>
#include <string>
#include <Eigen/Dense>

namespace KDL {

class SolverState {
    public:
        typedef boost::shared_ptr<SolverState> Ptr;
        /**
         * current time of the controller
         */
        double time;


        /**
         * velocity scale factor, this influences the output velocities, the feature variable velocities
         * and the progression of time. The default value is 1. 
         */
        double vel_scale;


        /**
         * norm of the output velocities (of the last time that getSolverOutput was called)
         */
        double vel_norm;

        /**
         * norm of the feature variable velocities ( of the last time that getSolverOutput was called)
         */
        double fv_norm;

        /**
         * joint names corresponding to joint values 
         */
        std::vector< std::string> jnames;
        /**
         * index into the list of context variables of the joint names. (result of context->getScalarNdx)
         */
        std::vector<int>          jnames_ctx_ndx;
        /**
         * maps joint names to index in jvalues/jvelocities. 
         */
        std::map< std::string, int> jindex;

        /**
         * joint values, these are used as an input to the solver (to evaluate all expressions).
         */
        Eigen::VectorXd           jvalues;
        /**
         * joint velocities, this is an output of the controller.
         */
        Eigen::VectorXd           jvelocities;

        /**
         * names corresponding to the feature variables.
         */
        std::vector< std::string> fnames;
        /**
         * index into the list of context variables of the feature variable names. (result of context->getScalarNdx)
         */
        std::vector<int>          fnames_ctx_ndx;
 
        /**
         * maps feature names to index in jvalues/jvelocities. 
         */
        std::map< std::string, int> findex;

        /**
         * value of the feature variables, this is used as an input to the solver.
         */
        Eigen::VectorXd           fvalues;

        /**
         * feature velocities, this is an output of the controller.
         */
        Eigen::VectorXd           fvelocities;

        SolverState() : time(0), vel_scale(1), vel_norm(1E10), fv_norm(1E10)  
        {}

        /**
         * intializes the solver_state, i.e. resizes all vectors to the correct values,
         * gets the names of joint and feature variables, and sets sample time and
         * whether or not an internal integrator should be active.
         */
        void initialize(const Context::Ptr& ctx,solver& solver) {
            vel_scale   = 1;
            vel_norm    = 1E10;
            fv_norm     = 1E10;
            solver.getJointNameVector(jnames);
            jnames_ctx_ndx.resize(jnames.size());
            for (unsigned int i=0;i<jnames.size();++i) {
                int r = ctx->getScalarNdx( jnames[i] );
                assert( (r!= -1) && "BUG: solver cannot contain variables that are outside the context");
                jnames_ctx_ndx[i] = r; 
            }
            solver.getJointNameToIndex(jindex);
            solver.getFeatureNameVector(fnames);  
            fnames_ctx_ndx.resize(fnames.size());
            for (unsigned int i=0;i<fnames.size();++i) {
                int r = ctx->getScalarNdx( fnames[i] );
                assert( (r!= -1) && "BUG: solver cannot contain variables that are outside the context");
                fnames_ctx_ndx[i] = r; 
            }
            solver.getFeatureNameToIndex(findex);
            jvalues.resize( jnames.size() );
            solver.getJointStates(jvalues);

            jvelocities.resize( jnames.size() );
            jvelocities = Eigen::VectorXd::Zero( jnames.size() );

//             std::cout << "fvalues before conservative resizing: " << std::endl;            
// 	    for (unsigned int i=0; i<fvalues.size(); ++i) 
// 	      std::cout << fvalues[i] << " ";
// 	    std::cout << std::endl;
	    
            fvalues.conservativeResize( fnames.size() );

//             std::cout << "fvalues initialized " << std::endl;
// 	    for (unsigned int i=0; i<fvalues.size(); ++i) 
// 	      std::cout << fvalues[i] << " ";
// 	    std::cout << std::endl;

	    
	    solver.getFeatureStates(fvalues);

	    
// 	    for (unsigned int i=0; i<fvalues.size(); ++i) 
// 	      std::cout << fvalues[i] << " ";
// 	    std::cout << std::endl;

            fvelocities.resize( fnames.size() ); 
            fvelocities = Eigen::VectorXd::Zero( fnames.size() );
        }

        /**
         * uses this solver_state to set the controller input.
         */
        void setSolverInput(solver& solver) {
            solver.setJointStates(jvalues);
            solver.setFeatureStates(fvalues);
            solver.setTime(time);
        }

        /**
         * gets the output of the controller and stores it into the solver_state
         */
        void getSolverOutput(solver& solver) {
            solver.getJointVelocities(jvelocities);
            vel_norm = jvelocities.norm();
            jvelocities *= vel_scale;
            solver.getFeatureVelocities(fvelocities);
            fv_norm  = fvelocities.norm();
            fvelocities *= vel_scale;
        }

        void setJointValues(const std::vector<double>& jval, const std::vector<std::string>& jvalnames) {
            assert( jval.size() == jvalnames.size() );
            for (int i=1;i<jvalnames.size();++i) {
                std::map< std::string, int>::iterator it=jindex.find( jvalnames[i] );
                if (it!=jindex.end()) {
                    jvalues[it->second] = jval[i];
                } 
            }     
        }
        void setTime(double _time) {
            time = _time;
        }
};

inline std::ostream& operator << ( std::ostream& os, SolverState& s) {
    os << "SolverState {\n";
    os << "    " << "time\t\t" << s.time << "\n"; 
    for (unsigned int i=0;i<s.jnames.size();++i) {
        os << "    " << s.jnames[i] << "\t\t" << s.jvalues[i] << "\n"; 
    }
    for (unsigned int i=0;i<s.fnames.size();++i) {
        os << "    " << s.fnames[i] << "\t\t" << s.fvalues[i] << "\n"; 
    }
    os << "}"<< std::endl;
} 

} // namespace KDL
#endif

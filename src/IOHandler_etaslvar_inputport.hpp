#ifndef EXPRESSIONGRAPH_IOHANDLER_ETASLVAR_INPUTPORT_HPP
#define EXPRESSIONGRAPH_IOHANDLER_ETASLVAR_INPUTPORT_HPP
#include <rtt/RTT.hpp>
#include "IOHandler.hpp"
#include <expressiongraph/context.hpp>
#include <string>
#include <boost/shared_ptr.hpp>

namespace KDL{

    /**
     * gets the values of the eTaSL inputchannels from an array input port.
     * No error is detected when the variables are not used within eTaSL. 
     *
     * When only 1 variable is specified, the input port will be a double input port instead
     * of an array.
     *
     */
    class IOHandler_etaslvar_inputport:
        public IOHandler {
            RTT::TaskContext*          tc;
            RTT::InputPort<std::vector<double> > inPort;
            RTT::InputPort< double > inPortScalar;
            boost::shared_ptr<Context::Ptr>  ctx;
            std::vector<std::string>   varnames;
            std::string                portname;
            std::string                portdocstring;
            std::vector<double>        values;
            std::vector<double>        default_values;
            std::vector< VariableType<double>::Ptr > inps;
    public:
            /**
             * default_values may be zero size, in this case 0.0 is taken for all variables.
             *
             */
            IOHandler_etaslvar_inputport(
                RTT::TaskContext*          _tc,
                boost::shared_ptr<Context::Ptr>        _ctx,
                const std::vector<std::string>&   _varnames,
                const std::vector<double>&        _default_values,
                const std::string&                _portname,
                const std::string&                _portdocstring
            );
            virtual bool initialize();
            virtual bool configure_component();
            virtual bool attach_to_etasl();
            virtual bool verify();
            virtual bool update();
            virtual void finish();
            virtual bool detach_from_etasl();
            virtual bool cleanup_component();
            virtual int getPriorityLevel();
            virtual ~IOHandler_etaslvar_inputport();
            virtual std::string getName();
    };

}//namespace KDL

#endif


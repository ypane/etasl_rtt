#include "IOHandler_activation_inputport.hpp"

#define MAX_CMD_SIZE 255

namespace KDL {
//------------------------------------------------------------------------------------------------
//     IOHandler_activation_inputport
//------------------------------------------------------------------------------------------------
    IOHandler_activation_inputport::IOHandler_activation_inputport(
                    RTT::TaskContext*   _tc,
                    boost::shared_ptr<Context::Ptr>   _ctx,
                    const std::string& _portname,
                    const std::string& _portdocstring,
                    const std::string& _initial_cmd
    ):tc(_tc),ctx(_ctx), portname(_portname), portdocstring(_portdocstring),initial_cmd(_initial_cmd) {
       cmd.reserve(MAX_CMD_SIZE);
    }
    
    bool IOHandler_activation_inputport::configure_component() {
        tc->ports()->addPort(portname, cmdPort).doc( portdocstring); 
        return true;
    }
    
    bool IOHandler_activation_inputport::attach_to_etasl() {
        (*ctx)->activate_cmd(initial_cmd);
        return true;
    }

    bool IOHandler_activation_inputport::initialize(){
        return true; 
    }

    bool IOHandler_activation_inputport::verify(){
        return true;
    }

    bool IOHandler_activation_inputport::update(){
        RTT::FlowStatus rv = cmdPort.read( cmd );
        if (rv == RTT::NewData) {
            (*ctx)->activate_cmd( cmd );
        } 
        return true;
    }

    void IOHandler_activation_inputport::finish(){
    }
    int IOHandler_activation_inputport::getPriorityLevel(){
        return 20;
    }
    
    bool IOHandler_activation_inputport::detach_from_etasl(){
      return true;
    }
    
    bool IOHandler_activation_inputport::cleanup_component(){
      tc->ports()->removePort(portname);
      return true;
    }

    IOHandler_activation_inputport::~IOHandler_activation_inputport(){
    }
    std::string IOHandler_activation_inputport::getName() {
        return portname;
    }

} // namespace KDL

#include "IOHandler_etaslvar_outputport.hpp"
#include <algorithm>
namespace KDL {
    IOHandler_etaslvar_outputport::IOHandler_etaslvar_outputport(
                    RTT::TaskContext*   _tc,
                    boost::shared_ptr<Context::Ptr>    _ctx,
                    const std::vector<std::string>& _varnames,
                    const std::string&  _portname,
                    const std::string&  _portdocstring

            ):
        tc(_tc),
        ctx(_ctx),
        varnames(_varnames),
        portname(_portname),
        portdocstring(_portdocstring) {
    }
    
    bool IOHandler_etaslvar_outputport::configure_component() {
        if (varnames.size()!=1) {
           tc->ports()->addPort(portname,outPort).doc(portdocstring);
        } else {
            tc->ports()->addPort(portname,outPortScalar).doc(portdocstring);
        }
        values.resize(varnames.size());
        std::fill(values.begin(), values.end(), 0.0);
        if (varnames.size()!=1) {
           outPort.setDataSample( values );
        } else {
           double value;
           value = values[0];
           outPortScalar.setDataSample(value);
        }
        return true;
    }

    bool IOHandler_etaslvar_outputport::attach_to_etasl() {
        outp.resize(varnames.size());
        for (unsigned int i=0;i<varnames.size();++i) {
            outp[i] = (*ctx)->getOutputExpression<double>(varnames[i]); 
            if (!outp[i]) {
                errormsg=portname+" (etaslvar_outputport): eTaSL output variable '"+varnames[i]+"' does not exists";
                outp[i] = Constant(0.0);
                return false; 
            } 
        }
        return true;
    }
    
    bool IOHandler_etaslvar_outputport::initialize() {
        return true;
    }

    bool IOHandler_etaslvar_outputport::verify(){
        return true;
    }

    bool IOHandler_etaslvar_outputport::update(){
        for (unsigned int i=0;i<outp.size();++i) {
            values[i] = outp[i]->value();
        }
        if (varnames.size()!=1) {
           outPort.write( values );
        } else {
           double value;
           value = values[0];
           outPortScalar.write(value);
        }
        return true;
    }
    void IOHandler_etaslvar_outputport::finish(){
        if (varnames.size()!=1) {
           outPort.write( values );
        } else {
           double value;
           value = values[0];
           outPortScalar.write(value);
        }
    }
    bool IOHandler_etaslvar_outputport::detach_from_etasl() {
      return true;
    }
    int IOHandler_etaslvar_outputport::getPriorityLevel(){
        return 20;
    }
    bool IOHandler_etaslvar_outputport::cleanup_component() {
      tc->ports()->removePort(portname);
      return true;
    }
    IOHandler_etaslvar_outputport::~IOHandler_etaslvar_outputport(){
    }
    std::string IOHandler_etaslvar_outputport::getName() {
        return varnames[0];
    }



} // namespace KDL

#ifndef OROCOS_ORO_ETASL_COMPONENT_HPP
#define OROCOS_ORO_ETASL_COMPONENT_HPP

#include <rtt/RTT.hpp>
#include <expressiongraph/context.hpp>
#include <expressiongraph/solver.hpp>
#include <expressiongraph/context_scripting.hpp>
#include <expressiongraph/defaultobserver.hpp>
#include <expressiongraph/breakobserver.hpp>
#include <boost/make_shared.hpp>
#include <stdlib.h>
#include <ros/ros.h>
#include "IOHandler.hpp"
#include "solver_state.hpp"
#include "rttluacontext.hpp"
#include "etasl_iohandler_rtt.hpp"
#include <rtt/os/TimeService.hpp>
#include <rtt/Time.hpp>


/**
 * \brief Orocos component that can read and execute eTaSL task specification
 * files.
 *
 * events published on the eventPort by the component itself (next to eTaSL events):
 *    e_finished:  component finished by a exit event.  Component in stopped state.
 *    e_error: component finished by a irrepairable error.  Component in stopped state.
 * 
 * be sure to check the outcome of "configure()"  
 *
 * Typical order of calling (with depl the rtt deployer):
 * - creation of the component:
 * \code 
 *  depl:import("etasl_rtt")
 *  depl:loadComponent("etasl", "etasl_rtt")
 *  etasl = depl:getPeer("etasl")             
 * \endcode
 * - definition of the component:
 * \code
 *  etasl:readTaskSpecificationFile("task_description_example.lua")
 *  etasl:add_controller_inputport(...)
 *  ...
 *  depl:setActivity("etasl", 0.01, 50, rtt.globals.ORO_SCHED_RT)
 * \endcode
 * - configuration and initialization:
 * \code
 *  etasl:configure()  -- etasl model should be read before configure.
 *  etasl:initialize() -- initializes the controller and its feature variables and resets vel_scale.
 *  etasl:disable_controller_output() --  can only be called after initialize() because initialize resets vel_scale.
 *  \endcode
 * - using the component:
 *  \code 
 *  etasl:start()
 *  ...
 *  etasl:enable_controller_output()
 *  etasl:stop()       -- 
 * \endcode
 */
class etasl_rtt : public etasl_iohandler_rtt {

    RTT::OperationCaller< KDL::solver::Ptr ()> createSolver;

    double                      sample_time;
    bool                        controller_output_defined;
    std::string                 event_postfix;
    RTT::OutputPort<std::string> eventPort;
    RTT::OutputPort<double> elapsedTimePort;
    RTT::OutputPort<double> computingTimePort;
    RTT::os::TimeService::ticks timestamp_from_start;
    double                      time_start;
    RTT::os::TimeService::ticks timestamp_step;
    bool                        first_time_after_start;
    int                         timing_policy_nr;    
            /* interpreted from timing_policy in configure() 
             * 0: nominal
             * 1: measured
             */
    void emit_event(const std::string& event);
    Eigen::VectorXd             old_fvelocities;
    bool clean_interfaces();
    bool clean_specification();
    bool reinitialize_data_structures();   
    virtual void go_to_safe_state();
    virtual bool check_state_etaslvar_port(const std::string& name);
    virtual bool check_is_initialized(const std::string& name);
    virtual bool check_spec_loaded(const std::string& name);

    /* for transient weighting (runtime skill composition) */
    RTT::OutputPort<double> wa; // transient weight of skill A 
    RTT::OutputPort<double> wb; // transient weight of skill B 
    RTT::OutputPort<double> transient_time; // transient time
    
public:
    RTTLuaContext::Ptr             lua;
    boost::shared_ptr<KDL::SolverState::Ptr> cstate;
    boost::shared_ptr<KDL::solver> solver;
    bool                        etaslread;
    bool                        initialized;
    bool                        controller_input_defined;
 
    /**
     * \name Properties
     */
    /// @{
    double                      initialization_time; ///< maximum simulation time to spend in initialization
    double                      initialization_time_step; ///< sample time to use during initialization phase
    double                      convergence_criterion;    ///< initialization returns early if this convergence_criterion is met. (if the state changes less than convergence_criterion).
    bool                        debug_if_solver_error;    ///< call a debug consol inside the context when there is a solver error (ONLY FOR DEBUGGING, BE CAREFUL WITH A ROBOT CONNECTED).
    bool                        disable_solver;          ///< disable calling the solver, to be used when you are only interested in evaluating expressions and not in computing desired output velocities.
    int                         solve_max_iterations; ///< maximum iterations when calling solve()
    double                      solve_time_step;      ///< time step  to be used when calling solve()
    double                      solve_convergence_crit;     ///< convergence criterion when calling solve()
    std::string                 timing_policy;       ///< 'nominal' use the default period, 'measured': measure the time interval
    std::string                 skill_to_remove; /// < the skill ID which is under removal. Can be used to check if certain context's IOs need to be clean-up
    /// @}

    /**
     * \param name name of the component
     */
    etasl_rtt(std::string const& name);

    /**
     * \name operations to add IOHandlers:
     */
    ///@{
    virtual bool add_activation_inputport( const std::string& portname, const std::string& portdocstring, const std::string& initial_cmd);
    virtual bool add_controller_outputport( const std::string& portname, const std::string& portdocstring, const std::vector<std::string>& jointnames);
    virtual bool add_controller_motion_control_msgs_joint_velocities_outputport( const std::string& portname, const std::string& portdocstring, const std::vector<std::string>& jointnames);
    virtual bool add_controller_pos_outputport( const std::string& portname, const std::string& portdocstring, const std::vector<std::string>& jointnames);
    virtual bool add_controller_inputport( const std::string& portname, const std::string& portdocstring, const std::vector<std::string>& jointnames);

    virtual bool add_etaslvar_deriv_outputport( const std::string& portname, const std::string& portdocstring, const std::vector<std::string>& varnames);
    ///@}
 
    /**
     * \name operations related to solving position kinematics
     *
     * Typically used when using the solve() operation.
    */
    ///@{
 
    /**
     * \brief solves the etasl specification until convergence.
     *
     * Typically used for inverse position kinematics or something similar.
     * \param max_iterations gives the maximum of iterations to use
     * \param time_step gives the (simulated) timestep to use.  This only refers to the 
     *       timestep used in the numerical algorithm.  This does not refer to real time.
     *       The combination of the specified control constant in the etasl specification and
     *       this timestep determine the step size used the algorithm.
     * \param convergence_crit the iterations will stop when the norm of changes to the state
     *        is smaller than convergence_crit.
     * \param jnames: an ordered list of joint names determining the order for initial_val and values.
     * \param initial_val: initial value of the joints before solving starts.
     * \param values: resulting joint values after solving 
     * \param results returns the resulting value of the objective function, i.e. the
     *        square of the slack variable multiplied by the weight of the constraint.
     *        For position constraints, the resulting slack variable is the control constant 
     *        multiplied by the remaining position error (assuming there is no explicit time
     *        dependency)
     * \returns error code, with the following semantics:
     *    -  0 : success
     *    -  -1 : only in stopped state ( no initialize nodig).
     *    -  -2 : inputhandler failed.
     *    -  -3 : error during qpOASIS solve 
     *    -  -4 : not converged after max_iterations cycles
     *    -  -5 : outputhandler failed.
     *    -  -6 : initial_val is not of size 0 and is not equal in size to jnames
     *    -  -7 : joint names does not exist.
     * \todo should check the explicit time dependencies and give out a warning message if necessary.
     */ 
    virtual int solve( 
                const std::vector<std::string>& jnames, 
                const std::vector<double>& initial_val, 
                std::vector<double>& values,
                double& result );
    ///@}

    /**
     *\name additional operations:
    */
    ///@{
  
    /**
     * \brief ensures that the controller outputs zero velocities
     *
     * This has the following effects:
     *  - output velocities are zero
     *  - time progression is stopped
     *  - feature variable velocities are also zero, and thus not further integrated.
     *
     * These effects correspond to infinitily slowing down time.
     * \warning The joint velocities will only be zero the next time updateHook() is called.
     * \warning initialize() re-enables the controller output.
     */
    virtual void disable_controller_output();

    /**
     * \brief turns the controller output back on, 
     * \param safety_tolerance  if the norm of the output velocities is smaller then the given safety_tolerance.
     *
     * \see disable_controller_output();
     * \warning effects will be visible the next time updateHook() is called.
     */
    virtual bool enable_controller_output(double safety_tolerance);

    /**
     * \brief scales the controller output velocities.
     * \param vel_scale the velocities are scaled with this factor.
     *
     * In contrast to enable_controller_output there are no safetychecks performed.
     *
     * \warning it is recommended (but NOT ENFORCED) to only scale with 0 <= vel_scale <= 1.
     *
     * The effects of scaling are the following:
     * - output velocities are scaled with vel_scale
     * - feature variable velocities are scaled with vel_scale
     * - time progression is scaled vel_scale  : time += vel_scale * delta_time
     *
     * \see disable_controller_output();
     * \warning effects will be visible the next time updateHook() is called.
     * \warning initialize() re-enables the controller output and sets vel_scale == 1.
     */
    virtual void scale_controller_output(double vel_scale);

    /**
     * \brief the norm of the most recently calculated output velocities.
     */
    virtual double normOutputVelocities();


    /**
     * \brief activates groups inside the eTaSL specification.  
     *
     * This activation is specified using an activation language that consists of space or comma seperated
     * activation commands ("+") or deactivation commands ("-"), a command can end with a
     * star ("*") to match all groups starting with the given partial name.
     * 
     * Example:
     * \code
     *    +global.moving_B -global.movingB
     * \endcode
     *
     * Example deactivate everything under global:
     * \code
     *   -global.*
     * \endcode
     */
    virtual bool activation_command(const std::string& filename);

    /**
     * \brief reads in a task specification file into the eTaSL environment
     */
    virtual bool readTaskSpecificationFile(const std::string& filename);

    /**
     * \brief executes a string in the eTaSL environment
     */
    virtual bool readTaskSpecificationString(const std::string& cmd);

    /**
     * \brief add to skill stack list
     */
    virtual void addSkillToStackList(const std::string& skill_id, int priority);
    
    /**
     * \brief reads in a task specification file into the eTaSL environment during runtime
     */
    virtual bool readTaskSpecificationFileRunTime(const std::string& filename, const std::string& groupname, int priority, bool transient);

    /**
     * \brief remove skill from eTaSL during runtime.
     * Assumption: only one skill can be removed at a time. This means that this method
     * should NOT be called again before the to-be-removed skills have been completely
     * destroyed
     */ 
    virtual bool removeSkillRunTime(const std::string& skillname, bool transient);

    /**
     * \brief signals the solver to start calculating the transient skills
     * weighting
     */ 
    virtual bool activateTransientWeighting(const std::string& skillname, const std::vector<std::string>& varnames, std::vector<double> values);

    /**
     * \brief get skill id from the stack by giving an integer enumerator
     */ 
    virtual std::string getSkillFromStack(int count);

    
    /**
     * \brief remove monitor based on the groupname
     */ 
    virtual bool removeMonitors(const std::string& skillname);    

    virtual bool setMonitorArgument(const std::string& monitorname, const std::string& argument);
    
    /**
     * \brief go to an eTaSL environment to directly interrogate the eTaSL expressions.
     * The component should be in the stopped state.
     */
    virtual bool etasl_console();
    /**
     * \brief print the matrices involved in solving the current step (DEBUGGING)
     */
    virtual void printMatrices(); 
    virtual double getLocalTime();
   
    ///@}
    /**
     * operations to get the value of etasl variables (not etasl expressions!) 
     */
    /// @{
    
    /**
     * \brief gets the (double) value of a given lua_statement
     *
     * In contrast to the set/get_etaslvar_... routines, this routines
     * gets the value of an ordinary lua variable or statement ( not of an expression)
     * \warning returns NaN if statement is invalid, in lua you can test this by value~=value, 
     * which is only true for NaN.
     */
    virtual double get_etasl_value_double( const std::string& lua_statement ); 
     
    /**
     * \brief gets the (string) value of a given lua_statement
     *
     * In contrast to the set/get_etaslvar_... routines, this routines
     * gets the value of an ordinary lua variable or statement ( not of an expression)
     *
     * \warning returns an empty string if statement is invalid
     */
    virtual std::string get_etasl_value_string( const std::string& lua_statement ); 
     
 
    /// @}

 
    /** 
     * \name standard hooks:
     */
    ///@{
    /**
     * \brief configures the component according to the previously specified etasl model.
     *
     * readTaskSpecificationFile or readTaskSpecificationString should have been called before
     * in order to define the eTaSL model.  Possible ports handlers should have been added.
     */
    virtual bool configureHook();

    /**
     * \brief initializes the component.
     *
     * Mainly makes sure that the feature variables are consistent with the priority 1 constraints.
     * Calls all input I/O handlers, but no output I/O handlers are called, i.e. it does not 
     * put anything on its output ports.
     * \warning also resets vel_scale to 1.
     */
    virtual bool initialize();

    /**
     * \brief does nothing else but starting to trigger the updateHook()
     */
    virtual bool startHook();
    virtual void updateHook();

    /**
     * \brief does nothing else but stopping to trigger the updateHook()
     */
    virtual void stopHook();
    virtual void cleanupHook();
    ///@}
    virtual ~etasl_rtt();
};
#endif

#include "IOHandler_controller_outputport.hpp"
#include <algorithm>
namespace KDL {
    IOHandler_controller_outputport::IOHandler_controller_outputport(
                    RTT::TaskContext*   _tc,
                    boost::shared_ptr<SolverState::Ptr>    _state,
                    const std::vector<std::string>& _jointnames,
                    const std::string&  _portname,
                    const std::string&  _portdocstring
            ):
        tc(_tc),
        state(_state),
        jointnames(_jointnames),
        portname(_portname),
        portdocstring(_portdocstring) {
    }

    bool IOHandler_controller_outputport::initialize() {
        return true;
    }
    
    bool IOHandler_controller_outputport::configure_component(){
        tc->ports()->addPort(portname,outPort).doc(portdocstring);
        jvals.resize(jointnames.size(), 0.0);
        outPort.setDataSample( jvals );
        name_ndx.clear();
        for (unsigned int i=0;i<jointnames.size();++i) {
            name_ndx[ jointnames[i]]  =i;
        }
        return true;
    }
    bool IOHandler_controller_outputport::attach_to_etasl() {
        return true;
    }
    bool IOHandler_controller_outputport::detach_from_etasl() {
        return true;
    }
    bool IOHandler_controller_outputport::cleanup_component() {
        tc->ports()->removePort(portname);
        return true;
    }
    bool IOHandler_controller_outputport::verify(){
        return true;
    }
    bool IOHandler_controller_outputport::update(){
        for (unsigned int i=0;i<jointnames.size();++i) {
            std::map<std::string,int>::iterator it = (*state)->jindex.find(jointnames[i]);
            if (it!=(*state)->jindex.end()) {
                jvals[i] = (*state)->jvelocities[it->second];
            }
        }
        outPort.write( jvals );
        return true;
    }
    void IOHandler_controller_outputport::finish(){
        // ensure that zero velocity is sent to the ports:
        std::fill(jvals.begin(),jvals.end(), 0.0);
        outPort.write( jvals );
    }
    int IOHandler_controller_outputport::getPriorityLevel(){
        return 20;
    }
    IOHandler_controller_outputport::~IOHandler_controller_outputport(){
        tc->ports()->removePort(portname);
    }
    std::string IOHandler_controller_outputport::getName() {
        return portname;
    }


} // namespace KDL

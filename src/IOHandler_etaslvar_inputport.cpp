#include "IOHandler_etaslvar_inputport.hpp"

namespace KDL {
    IOHandler_etaslvar_inputport::IOHandler_etaslvar_inputport(
                RTT::TaskContext*          _tc,
                boost::shared_ptr<Context::Ptr>        _ctx,
                const std::vector<std::string>&   _varnames,
                const std::vector<double>&        _default_values,
                const std::string&                _portname,
                const std::string&                _portdocstring
    ): 
        tc(_tc),
        ctx(_ctx),
        varnames(_varnames),
        default_values(_default_values),
        portname(_portname),
        portdocstring(_portdocstring)
        {}
        
    bool IOHandler_etaslvar_inputport::configure_component() {
        if (default_values.size()==0) {
            default_values.resize(varnames.size());
            std::fill(default_values.begin(), default_values.end(), 0.0);
        }
        if (default_values.size()!=varnames.size()) {
            errormsg="default values array has the wrong size";
            return false;
        }
        if (varnames.size()!=1) {
          tc->ports()->addPort(portname,inPort).doc(portdocstring);
        } else {
          tc->ports()->addPort(portname,inPortScalar).doc(portdocstring);
        }
        values.resize(varnames.size());
        std::copy(default_values.begin(), default_values.end(), values.begin());
        return true;
    }

    bool IOHandler_etaslvar_inputport::attach_to_etasl() {
        inps.resize(varnames.size());
        for (unsigned int i=0;i<varnames.size();++i) {
            inps[i] = (*ctx)->getInputChannel<double>(varnames[i]); 
            if (!inps[i]) {
                errormsg=portname+" (etaslvar_inputport): eTaSL input variable '"+ varnames[i] +"' does not exists";
                return false;
            } else {
                inps[i]->setValue( default_values[i] );
            }
        }
        return true;

    }
    
    bool IOHandler_etaslvar_inputport::initialize() {
        return true;
    }
    bool IOHandler_etaslvar_inputport::verify() {
        return true;
    }
    bool IOHandler_etaslvar_inputport::update() {
        RTT::FlowStatus fs;
        // read values from port and set data
        if (varnames.size()!=1) {
            fs  = inPort.read( values );
        } else {
            double value;
            fs  = inPortScalar.read( value );
            values[0]=value;
        }
        if (fs==RTT::NewData) {
            for (unsigned int i=0;i<varnames.size();++i) {
                inps[i]->setValue(values[i]);
            }
        }
        return true;
    }

    void IOHandler_etaslvar_inputport::finish() {
    }
    
    bool IOHandler_etaslvar_inputport::detach_from_etasl() {
        return true;
    }

    int IOHandler_etaslvar_inputport::getPriorityLevel() {
        return 20;
    }
    bool IOHandler_etaslvar_inputport::cleanup_component() {
        tc->ports()->removePort(portname);
        return true;
    }
    IOHandler_etaslvar_inputport::~IOHandler_etaslvar_inputport() {
    }
    std::string IOHandler_etaslvar_inputport::getName() {
        return varnames[0];
    }
} // namespace KDL

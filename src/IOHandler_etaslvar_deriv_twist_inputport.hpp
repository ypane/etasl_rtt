#ifndef EXPRESSIONGRAPH_IOHANDLER_ETASLVAR_DERIV_TWIST_INPUTPORT_HPP
#define EXPRESSIONGRAPH_IOHANDLER_ETASLVAR_DERIV_TWIST_INPUTPORT_HPP
#include <rtt/RTT.hpp>
#include "IOHandler.hpp"
#include <expressiongraph/context.hpp>
#include <kdl/expressiontree_var.hpp>
#include <string>
#include <boost/shared_ptr.hpp>
#include <kdl/frames.hpp>

namespace KDL{

    /**
     * gets the values of the eTaSL inputchannels from an array input port.
     * No error is detected when the variables are not used within eTaSL. 
     *
     */
    class IOHandler_etaslvar_deriv_twist_inputport:
        public IOHandler {
            RTT::TaskContext*          tc;
            RTT::InputPort<KDL::Twist > inPort;
            boost::shared_ptr<Context::Ptr> ctx;
            std::string                varname;
            std::string                portname;
            std::string                portdocstring;
            KDL::Twist                 value;
            KDL::Twist                 default_value;
            VariableType<KDL::Frame>::Ptr  inps;
            bool                       derivatives; 
            int                        time_ndx;
    public:
            IOHandler_etaslvar_deriv_twist_inputport(
                RTT::TaskContext*          _tc,
                boost::shared_ptr<Context::Ptr> _ctx,
                const std::string&         _varname,
                const std::string&         _portname,
                const std::string&         _portdocstring
            );
            virtual bool initialize();
            virtual bool configure_component();
            virtual bool attach_to_etasl();
            virtual bool detach_from_etasl();
            virtual bool cleanup_component();
            virtual bool verify();
            virtual bool update();
            virtual void finish();
            virtual int getPriorityLevel();
            virtual ~IOHandler_etaslvar_deriv_twist_inputport();
            virtual std::string getName();
    };

}//namespace KDL

#endif


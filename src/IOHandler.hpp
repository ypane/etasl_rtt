#ifndef EXPRESSIONGRAPH_IO_HANDLER
#define EXPRESSIONGRAPH_IO_HANDLER

#include <boost/shared_ptr.hpp>
#include <vector>
#include <string>
#include <iostream>

namespace KDL {

/**
 * Abstract interface for an IOHandler, for both the ETaSL side and the ROS side.
 *
 * The priority levels ensure that e.g. etasl IO handlers are called before the
 * IO handler for e.g. a topic.  This ensures that buffers are filled before they
 * are used.
 *
 * An IOHandler can read or write from the associated buffer.
 */
class IOHandler {
    protected:
        std::string errormsg;  //!
        std::string returnmsg;
    public:

        /**
         * a smart pointer towards an object with this interface:
         */
        typedef boost::shared_ptr<IOHandler> Ptr;

        virtual const std::string& getErrorMsg() {
            if (errormsg.empty())
              returnmsg="(unknown)";
            else
              returnmsg = errormsg;
            errormsg.clear();
            return returnmsg;
        } 

        /**
         * performs the per-element initialization necessary for this IO handler
         * \returns true if succesfull, when this return false, the user of this class
         *      may not call update() or finish(). 
         */
        virtual bool initialize() {
            std::cerr << "IOHandler::initialize() should not be called directly" << std::endl;
        }

        virtual bool configure_component() {
          std::cerr << "IOHandler::configure_component() should not be called directly " << std::endl;
        }
        
        virtual bool attach_to_etasl() {
          std::cerr << "IOHandler::attach_to_etasl() should not be called directly " << std::endl;
        }
        
        virtual bool detach_from_etasl() {
          std::cerr << "IOHandler::detach_from_etasl() should not be called directly " << std::endl;
        }
        
        virtual bool cleanup_component() {
          std::cerr << "IOHandler::cleanup_component() should not be called directly " << std::endl;
        }
        
        /**
         * performs additional checks to ensure that update() can be safely executed.
         * This avoids these tests to be executed at each sample time.
         */
        virtual bool verify() {
            std::cerr << "IOHandler::verify() should not be called directly " << std::endl;
        }

        /**
         * performs the per-element update step necessary for this IO handler
         * \returns if this returns false, a run-time error will occurr.
         */
        virtual bool update() {
            std::cerr << "IOHandler::update() should not be called directly " << std::endl;
        }

        /**
         * performs the per-element finishing step to clean up for this IO handler.
         */
        virtual void finish() {
            std::cerr << "IOHandler::finish() should not be called directly" << std::endl;
        }

        /**
         * priority level, handlers with a low number priority number will be executed first.
         */
        virtual int getPriorityLevel() {
            std::cerr << "IOHandler::getPriorityLevel() should not be called directly " << std::endl;
        }

        /**
         * return the name of the IO handler 
         */
        virtual std::string getName() {  // TODO: change to pass by reference
        }
        
        /**
         * virtual destructor (necessary to ensure that always the proper
         * destructor is called)
         */
        virtual ~IOHandler() {}
};

} // namespace KDL

#endif 


#ifndef EXPRESSIONGRAPH_IOHANDLER_ETASLVAR_DERIV_INPUTPORT_HPP
#define EXPRESSIONGRAPH_IOHANDLER_ETASLVAR_DERIV_INPUTPORT_HPP
#include <rtt/RTT.hpp>
#include "IOHandler.hpp"
#include <expressiongraph/context.hpp>
#include <kdl/expressiontree_var.hpp>
#include <string>
#include <boost/shared_ptr.hpp>

namespace KDL{

    /**
     * gets the (partial) time derivatives for  the eTaSL inputchannels from an array input port.
     *
     * When only 1 variable is specified, the input port will be a double input port instead
     * of an array.
     */
    class IOHandler_etaslvar_deriv_inputport:
        public IOHandler {
            RTT::TaskContext*          tc;
            RTT::InputPort<std::vector<double> > inPort;
            RTT::InputPort< double > inPortScalar;
            boost::shared_ptr<Context::Ptr> ctx;
            std::vector<std::string>   varnames;
            std::string                portname;
            std::string                portdocstring;
            std::vector<double>        values;
            std::vector<double>        default_values;
            std::vector< VariableType<double>::Ptr > inps;
            bool                       derivatives; 
            int                        time_ndx;
    public:
            /**
             * default_values may be zero size, in this case 0.0 is taken for all variables.
             *
             */
            IOHandler_etaslvar_deriv_inputport(
                RTT::TaskContext*          _tc,
                boost::shared_ptr<Context::Ptr>   _ctx,
                const std::vector<std::string>&   _varnames,
                const std::string&                _portname,
                const std::string&                _portdocstring
            );
            virtual bool initialize();
            virtual bool configure_component();
            virtual bool attach_to_etasl();
            virtual bool detach_from_etasl();
            virtual bool cleanup_component();
            virtual bool verify();
            virtual bool update();
            virtual void finish();
            virtual int getPriorityLevel();
            virtual ~IOHandler_etaslvar_deriv_inputport();
            virtual std::string getName();
    };

}//namespace KDL

#endif


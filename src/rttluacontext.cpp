#include "rttluacontext.hpp" 
#include <expressiongraph/context.hpp>
#include <expressiongraph/context_scripting.hpp>

#include <lua.hpp>
#include <luabind/luabind.hpp>
#include <luabind/operator.hpp>
#include <luabind/class_info.hpp>
#include <luabind/out_value_policy.hpp>
#include <expressiongraph_lua/bind_kdl.hpp>
#include <luabind/shared_ptr_converter.hpp>
#include <luabind/adopt_policy.hpp>
//#include <ros/package.h>

using namespace KDL;
using namespace luabind;
using namespace std;

#include <map>

RTTLuaContext::RTTLuaContext() {
    L = luaL_newstate();
    luaL_openlibs(L);
    open(L);
    register_kdl(L);
    register_context(L);
    bind_class_info(L);
}

RTTLuaContext::~RTTLuaContext() {
    //lua_close(L); 
}

static void l_rtt_message (const char *pname, const char *msg) {
  RTT::log(RTT::Error) << pname << " : " << msg << RTT::endlog();
}


static int rtt_report (lua_State *L, int status) {
  if (status && !lua_isnil(L, -1)) {
    const char *msg = lua_tostring(L, -1);
    if (msg == NULL) msg = "(error object is not a string)";
    l_rtt_message("LuaContext::", msg);
    lua_pop(L, 1);
  }
  return status;
}


static int dolibrary (lua_State *L, const char *name) {
  lua_getglobal(L, "require");
  lua_pushstring(L, name);
  return rtt_report(L, lua_pcall(L, 1, 0, 0));
}




bool RTTLuaContext::call_console() {
    int error = dolibrary(L,"ilua");
    if (error !=0) {
        cerr << "could not find the ilua library" << endl;
        return false;
    }
    return true;
}



void RTTLuaContext::initContext(Context::Ptr ctx) {
    this->ctx = ctx; // keep reference.
    luabind::globals(L)["time"] = ctx->getScalarExpr("time");
    luabind::globals(L)["ctx"]  = ctx; 
}

int RTTLuaContext::executeFile(const std::string& name) {
   int error = luaL_loadfile(L,name.c_str());
    if (error != 0) {
        RTT::log(RTT::Error) <<  name << " : executeFile : error during loading/compiling :\n " << lua_tostring(L, -1) <<  RTT::endlog();
        lua_pop(L, 1);  /* pop error message from the stack */
        return 1;
    }
    error = lua_pcall(L,0,0,0);
    if (error != 0) {
        RTT::log(RTT::Error) <<  name << " : executeFile : error during execution :\n " << lua_tostring(L, -1) <<  RTT::endlog();
        lua_pop(L, 1);  /* pop error message from the stack */
        return 2;
    }
    return 0;
}

int RTTLuaContext::executeString(const std::string& str) {
   const char* buf = str.c_str();
   int error = luaL_loadbuffer(L,buf,str.length(),"executeString");
    if (error != 0) {
        RTT::log(RTT::Error) << "executeString : error during loading/compiling :\n " << lua_tostring(L, -1) <<  RTT::endlog();
        lua_pop(L, 1);  /* pop error message from the stack */
        return 1;
    }
    error = lua_pcall(L,0,0,0);
    if (error != 0) {
        RTT::log(RTT::Error) <<  "executeString : error during execution :\n " << lua_tostring(L, -1) <<  RTT::endlog();
        lua_pop(L, 1);  /* pop error message from the stack */
        return 2;
    }
    return 0;
}

int RTTLuaContext::executeAndReturnDouble(const std::string& str, double& value ) {
   const char* buf = str.c_str();
   int error = luaL_loadbuffer(L,buf,str.length(),"executeAndReturnDouble");
    if (error != 0) {
        RTT::log(RTT::Error) << "executeAndReturnDouble : error during loading/compiling :\n " << lua_tostring(L, -1) <<  RTT::endlog();
        lua_pop(L, 1);  /* pop error message from the stack */
        return 1;
    }
    error = lua_pcall(L,0,1,0);
    if (error != 0) {
        RTT::log(RTT::Error) <<  "executeAndReturnDouble : error during execution :\n " << lua_tostring(L, -1) <<  RTT::endlog();
        lua_pop(L, 1);  /* pop error message from the stack */
        return 2;
    }

    if (!lua_isnumber(L, -1)) {
        lua_pop(L,1);
        return 3;
    }
    value = lua_tonumber(L,-1);
    lua_pop(L,1); 

    return 0;
}


int RTTLuaContext::executeAndReturnString(const std::string& str, std::string& value ) {
   const char* buf = str.c_str();
   int error = luaL_loadbuffer(L,buf,str.length(),"executeAndReturnString");
    if (error != 0) {
        RTT::log(RTT::Error) << "executeAndReturnString : error during loading/compiling :\n " << lua_tostring(L, -1) <<  RTT::endlog();
        lua_pop(L, 1);  /* pop error message from the stack */
        return 1;
    }
    error = lua_pcall(L,0,1,0);
    if (error != 0) {
        RTT::log(RTT::Error) <<  "executeAndReturnString : error during execution :\n " << lua_tostring(L, -1) <<  RTT::endlog();
        lua_pop(L, 1);  /* pop error message from the stack */
        return 2;
    }

    const char* bufstr = lua_tostring(L,-1);
    lua_pop(L,1); 
    if (bufstr==0) {
        value = "";
        return 3; /* not a string */
    } else {
        value = bufstr;
        return 0;
    }
}



#include "IOHandler_controller_motion_control_msgs_joint_velocities_outputport.hpp"
#include <algorithm>
namespace KDL {
    IOHandler_controller_motion_control_msgs_joint_velocities_outputport::IOHandler_controller_motion_control_msgs_joint_velocities_outputport(
                    RTT::TaskContext*   _tc,
                    boost::shared_ptr<SolverState::Ptr>  _state,
                    const std::vector<std::string>& _jointnames,
                    const std::string&  _portname,
                    const std::string&  _portdocstring
            ):
        tc(_tc),
        state(_state),
        jointnames(_jointnames),
        portname(_portname),
        portdocstring(_portdocstring) {
    }

    bool IOHandler_controller_motion_control_msgs_joint_velocities_outputport::configure_component() {
        tc->ports()->addPort(portname,outPort).doc(portdocstring);
        jv.velocities.resize(jointnames.size(),0.0);
        jv.names.resize( jointnames.size() );
        std::copy(jointnames.begin(),jointnames.end(), jv.names.begin());
        outPort.setDataSample( jv );
        return true;
    }
    
    bool IOHandler_controller_motion_control_msgs_joint_velocities_outputport::attach_to_etasl() {
        return true;
    }
    
    bool IOHandler_controller_motion_control_msgs_joint_velocities_outputport::initialize() {
        return true;
    }

    bool IOHandler_controller_motion_control_msgs_joint_velocities_outputport::verify(){
        return true;
    }

    bool IOHandler_controller_motion_control_msgs_joint_velocities_outputport::update(){
        for (unsigned int i=0;i<jointnames.size();++i) {
            std::map<std::string,int>::iterator it = (*state)->jindex.find(jointnames[i]);
            if (it!=(*state)->jindex.end()) {
                jv.velocities[i] = (*state)->jvelocities[it->second];
            }
        }
        outPort.write( jv );
        return true;
    }

    void IOHandler_controller_motion_control_msgs_joint_velocities_outputport::finish(){
        std::fill(jv.velocities.begin(),jv.velocities.end(), 0.0);
        outPort.write( jv );
    }
    
    bool IOHandler_controller_motion_control_msgs_joint_velocities_outputport::detach_from_etasl() {
        return true;
    }
    
    bool IOHandler_controller_motion_control_msgs_joint_velocities_outputport::cleanup_component() {
        tc->ports()->removePort(portname);
        return true;
    }
    

    int IOHandler_controller_motion_control_msgs_joint_velocities_outputport::getPriorityLevel(){
        return 20;
    }

    IOHandler_controller_motion_control_msgs_joint_velocities_outputport::~IOHandler_controller_motion_control_msgs_joint_velocities_outputport(){
        tc->ports()->removePort(portname);
    }
    std::string IOHandler_controller_motion_control_msgs_joint_velocities_outputport::getName() {
        return portname;
    }
} // namespace KDL


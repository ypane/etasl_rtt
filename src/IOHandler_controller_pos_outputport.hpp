#ifndef EXPRESSIONGRAPH_IOHANDLER_CONTROLLER_POS_OUTPUTPORT_HPP
        #define EXPRESSIONGRAPH_IOHANDLER_CONTROLLER_POS_OUTPUTPORT_HPP

#include <rtt/RTT.hpp>
#include "IOHandler.hpp"
#include <expressiongraph/context.hpp>
#include <string>
#include <boost/shared_ptr.hpp>
#include "solver_state.hpp"

namespace KDL{

    /**
     * gets the joint position output of the controller and send it to an Orocos output port.
     * The port will be an array of doubles and the joints will be in the order as given
     * by jointnames.  The joints not present in jointnames will not be present at the port.
     * If you add non-existing names to jointnames, the corresponding value is always zero.
     * (such that there are no problems when a joint has disappeared from the state because of 
     * optimizations of the execution of the eTaSL specification).
     */
    class IOHandler_controller_pos_outputport:
        public IOHandler {
            RTT::TaskContext*          tc;
            RTT::OutputPort<std::vector<double> > outPort;
            boost::shared_ptr<SolverState::Ptr>     state;
            std::vector<std::string>   jointnames;
            std::map<std::string,int>  name_ndx;
            std::string                portname;
            std::string                portdocstring;
            std::vector<double>        jvals;
    public:
            typedef boost::shared_ptr<IOHandler> Ptr;

            IOHandler_controller_pos_outputport(
                    RTT::TaskContext*   _tc,
                    boost::shared_ptr<SolverState::Ptr> _state,
                    const std::vector<std::string>& _jointnames,
                    const std::string&  _portname,
                    const std::string&  _portdocstring
            );
            virtual bool initialize();
            virtual bool configure_component();
            virtual bool attach_to_etasl();
            virtual bool detach_from_etasl();
            virtual bool cleanup_component();
            virtual bool verify();
            virtual bool update();
            virtual void finish();
            virtual int getPriorityLevel();
            virtual ~IOHandler_controller_pos_outputport();
            virtual std::string getName();
    };

}//namespace KDL

#endif


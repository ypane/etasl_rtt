#ifndef EXPRESSIONGRAPH_IO_HANDLER_COLLECTION_HPP
#define EXPRESSIONGRAPH_IO_HANDLER_COLLECTION_HPP

#include <boost/shared_ptr.hpp>
#include <vector>
#include <string>
#include "IOHandler.hpp"

namespace KDL {

/**
 * Abstract interface to handle a collection of IOHandlers of a given type.
 * Manages this collection as a sorted list (using priority levels).  
 * There can be multiple IOHandlerCollection objects for multiple
 * types of IO.
 */
class IOHandlerCollection: public IOHandler {
    protected:
        typedef std::vector< IOHandler::Ptr> ListType;
        typedef std::vector< IOHandler::Ptr>::iterator ListIterator;
        ListType list;
        int priority_level;
    public:
        /**
         * a smart pointer towards an object with this interface:
         */
        typedef boost::shared_ptr<IOHandlerCollection> Ptr;


        IOHandlerCollection();

        /**
         * add an IOHandler to the collection.
         */
        virtual void addHandler(IOHandler::Ptr oh);


        /**
         * clears all IOHandlers from the list.
         */
        virtual void clear();

        /** 
         * initialize common data to this type
         * and calls the initialize of the underlying 
         * IOHandlers
         */
        virtual bool initialize();
        
        virtual bool configure_component();
        
        virtual bool attach_to_etasl();
        
        virtual bool detach_from_etasl();
        
        virtual bool cleanup_component();
        
        /** 
         * Clear IO channels of a given skill ID.
         * Can be used to remove IOs of a skill that
         * has been just removed during runtime
         */        
        virtual void clear_skill_io(std::string skillname);

        /** 
         * verifies the underlying 
         * IOHandlers in the proper order, to ensure
         * that update() can be called. This is to avoid doing checks
         * in update. It is only usefull to call  verify() once after initialize().
         */
        virtual bool verify();

        /** 
         * update common data to this type
         * and calls the update of the underlying 
         * IOHandlers in the proper order
         */
        virtual bool update();

         /** 
         * finializes common data to this type
         * and calls the update of the underlying 
         * IOHandlers
         */
       virtual void finish();

        /**
         * priority level, handlers with a low number priority number will be executed first.
         */
        virtual int getPriorityLevel();


        /**
         * virtual destructor
         */
       virtual ~IOHandlerCollection();
};


} // namespace KDL

#endif 


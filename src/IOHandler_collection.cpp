#include "IOHandler_collection.hpp"
#include <algorithm>
#include <vector>
#include <iostream>

namespace KDL {
    
    IOHandlerCollection::IOHandlerCollection():
        priority_level(10000000) {
            //std::cout << "IOHandlerCollection constructed" << std::endl;
        }
    
    bool compare( const IOHandler::Ptr& a, const IOHandler::Ptr& b) {
        return ( a->getPriorityLevel() < b->getPriorityLevel() );
    }


    void IOHandlerCollection::addHandler(IOHandler::Ptr oh) {
        list.push_back(oh);
        priority_level= std::min( priority_level, oh->getPriorityLevel() );
    }
    
    void IOHandlerCollection::clear() {
        /*std::cout << "IOHandlerCollection::clear() is called" << std::endl;*/
        for ( ListIterator it=list.begin();it!=list.end();++it) {
            it->reset();
        }
        list.clear();
    }

    bool IOHandlerCollection::initialize() {
        std::sort( list.begin(), list.end(), compare );
        for ( ListIterator it=list.begin();it!=list.end();++it) {
            if (!(*it)->initialize()) {
                errormsg = (*it)->getErrorMsg();
                return false;
            }
        }
        return true;
    }

    bool IOHandlerCollection::configure_component() {
      for ( ListIterator it=list.begin();it!=list.end();++it) {
            if (!(*it)->configure_component()) {
                errormsg = (*it)->getErrorMsg();
                return false;
            }
      }
      return true;
    }
    
    bool IOHandlerCollection::attach_to_etasl() {
      for ( ListIterator it=list.begin();it!=list.end();++it) {
            if (!(*it)->attach_to_etasl()) {
                errormsg = (*it)->getErrorMsg();
                return false;
            }
      }
      return true;
    }
    
    bool IOHandlerCollection::detach_from_etasl() {
      for ( ListIterator it=list.begin();it!=list.end();++it) {
            if (!(*it)->detach_from_etasl()) {
                errormsg = (*it)->getErrorMsg();
                return false;
            }
      }
      return true;
    }
    
    bool IOHandlerCollection::cleanup_component() {
      for ( ListIterator it=list.begin();it!=list.end();++it) {
            if (!(*it)->cleanup_component()) {
                errormsg = (*it)->getErrorMsg();
                return false;
            }
      }
      return true;
    }

    void IOHandlerCollection::clear_skill_io(std::string skillname) {
        ListIterator it = list.begin();
        size_t len = skillname.length();
        while (it != list.end())
        {
            if ((*it)->getName().compare(0, len, skillname)==0) {
                // erase() invalidates the iterator, use returned iterator
                it = list.erase(it);
            }
            // Notice that iterator is incremented only on the else part
            else {
                ++it;
            }
        }
    }
    
    bool IOHandlerCollection::update() {
        for ( ListIterator it=list.begin();it!=list.end();++it) {
            if (!(*it)->update()) {
                errormsg = (*it)->getErrorMsg();
                return false;
            }
        }
        return true;
    }
    bool IOHandlerCollection::verify() {
        for ( ListIterator it=list.begin();it!=list.end();++it) {
            if (!(*it)->verify()) {
                errormsg = (*it)->getErrorMsg();
                return false;
            }
        }
        return true;
    }


    void IOHandlerCollection::finish() {
        for ( ListIterator it=list.begin();it!=list.end();++it) {
            (*it)->finish();
        }
    }

    int IOHandlerCollection::getPriorityLevel() {
        return priority_level;
    }

    IOHandlerCollection::~IOHandlerCollection() {
            //std::cout << "IOHandlerCollection destructed" << std::endl;
    } 

};// namespace KDL

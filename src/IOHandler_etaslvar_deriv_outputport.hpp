#ifndef EXPRESSIONGRAPH_IOHANDLER_ETASLVAR_DERIV_OUTPUTPORT_HPP
#define EXPRESSIONGRAPH_IOHANDLER_ETASLVAR_DERIV_OUTPUTPORT_HPP

#include <rtt/RTT.hpp>
#include "IOHandler.hpp"
#include "solver_state.hpp"
#include <expressiongraph/context.hpp>
#include <string>
#include <boost/shared_ptr.hpp>

namespace KDL{

    /**
     * gets the values of outputexpressions in eTaSL sends it to an Orocos output port.
     * The port will be an array of doubles and the joints will be in the order as given
     * by the list of variable names.  
     *
     * The variables not present in eTaSL will trigger an error.
     *
     */
    class IOHandler_etaslvar_deriv_outputport:
        public IOHandler {
            RTT::TaskContext*          tc;
            RTT::OutputPort<std::vector<double> > outPort;
            RTT::OutputPort<double> outPortScalar;
            std::vector< Expression<double>::Ptr > outp; 
            boost::shared_ptr<Context::Ptr> ctx;
            boost::shared_ptr<SolverState::Ptr> state;
            std::vector<std::string>   varnames;
            std::string                portname;
            std::string                portdocstring;
            std::vector<double>        values;
            int                        time_ndx;
    public:
            typedef boost::shared_ptr<IOHandler> Ptr;

            IOHandler_etaslvar_deriv_outputport(
                    RTT::TaskContext*   _tc,
                    boost::shared_ptr<Context::Ptr> _ctx,
                    boost::shared_ptr<SolverState::Ptr> _state,
                    const std::vector<std::string>& _varnames,
                    const std::string&  _portname,
                    const std::string&  _portdocstring
            );
            virtual bool initialize();
            virtual bool configure_component();
            virtual bool attach_to_etasl();
            virtual bool detach_from_etasl();
            virtual bool cleanup_component();
            virtual bool verify();
            virtual bool update();
            virtual void finish();
            virtual int getPriorityLevel();
            virtual ~IOHandler_etaslvar_deriv_outputport();
            virtual std::string getName();
    };

}//namespace KDL

#endif


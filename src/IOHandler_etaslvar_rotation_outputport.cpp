#include "IOHandler_etaslvar_rotation_outputport.hpp"
#include <algorithm>
namespace KDL {
    IOHandler_etaslvar_rotation_outputport::IOHandler_etaslvar_rotation_outputport(
                    RTT::TaskContext*   _tc,
                    boost::shared_ptr<Context::Ptr> _ctx,
                    const std::string&  _varname,
                    const std::string&  _portname,
                    const std::string&  _portdocstring

            ):
        tc(_tc),
        ctx(_ctx),
        varname(_varname),
        portname(_portname),
        portdocstring(_portdocstring) {
    }
    
    bool IOHandler_etaslvar_rotation_outputport::configure_component() {
        tc->ports()->addPort(portname,outPort).doc(portdocstring);
        outPort.setDataSample( value );
        return true;
    }

    bool IOHandler_etaslvar_rotation_outputport::attach_to_etasl() {
        outp = (*ctx)->getOutputExpression<KDL::Rotation>(varname); 
        if (!outp) {
                errormsg=portname+" (etaslvar_rotation): eTaSL output variable '"+varname+"' does not exists or does not have expression_rotation type";
                outp = Constant(KDL::Rotation::Identity());
                return false;
        }
        return true;
    }
    
    bool IOHandler_etaslvar_rotation_outputport::initialize() {
        return true;
    }

    bool IOHandler_etaslvar_rotation_outputport::verify(){
        return true;
    }

    bool IOHandler_etaslvar_rotation_outputport::update(){
        value = outp->value();
        outPort.write( value );
        return true;
    }

    void IOHandler_etaslvar_rotation_outputport::finish(){
        outPort.write( value );
    }
    bool IOHandler_etaslvar_rotation_outputport::detach_from_etasl() {
        return true;
    }

    int IOHandler_etaslvar_rotation_outputport::getPriorityLevel(){
        return 20;
    }
    bool IOHandler_etaslvar_rotation_outputport::cleanup_component(){
        tc->ports()->removePort(portname);
        return true;
    }

    IOHandler_etaslvar_rotation_outputport::~IOHandler_etaslvar_rotation_outputport(){
        tc->ports()->removePort(portname);
    }
    std::string IOHandler_etaslvar_rotation_outputport::getName() {
        return varname;
    }
} // namespace KDL

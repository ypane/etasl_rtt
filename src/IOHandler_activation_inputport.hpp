#ifndef EXPRESSIONGRAPH_IOHANDLER_ACTIVATION_INPUTPORT_HPP
#define EXPRESSIONGRAPH_IOHANDLER_ACTIVATION_INPUTPORT_HPP

#include <rtt/RTT.hpp>
#include "IOHandler.hpp"
#include <expressiongraph/context.hpp>
#include <string>
#include <boost/shared_ptr.hpp>

namespace KDL{

    /**
     * gets the values from a string input port and send and interpretes it as
     * activation commands. 
     */
    class IOHandler_activation_inputport:
        public IOHandler {
            RTT::TaskContext*           tc;
            boost::shared_ptr<Context::Ptr> ctx;
            RTT::InputPort<std::string> cmdPort;
            std::string                 portname;
            std::string                 portdocstring;
            std::string                 initial_cmd;
            std::string                 cmd;
    public:
            IOHandler_activation_inputport(
                    RTT::TaskContext*               _tc,
                    boost::shared_ptr<Context::Ptr> _ctx,
                    const std::string&         _portname,
                    const std::string&    _portdocstring,
                    const std::string&       _initial_cmd
            );
            virtual bool initialize();
            virtual bool configure_component();
            virtual bool attach_to_etasl();
            virtual bool verify();
            virtual bool update();
            virtual void finish();
            virtual bool detach_from_etasl();
            virtual bool cleanup_component();
            virtual int getPriorityLevel();
            virtual ~IOHandler_activation_inputport();
            virtual std::string getName();
    };


}//namespace KDL

#endif


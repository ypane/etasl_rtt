#include <rtt/Logger.hpp>
#include "etasl_rtt-component.hpp"
#include <rtt/Component.hpp>
#include <iostream>
#include "IOHandler_activation_inputport.hpp"
#include "IOHandler_controller_outputport.hpp"

#include "IOHandler_controller_pos_outputport.hpp"
#include "IOHandler_controller_motion_control_msgs_joint_velocities_outputport.hpp"
#include "IOHandler_controller_inputport.hpp"
#include "IOHandler_etaslvar_outputport.hpp"
#include "IOHandler_etaslvar_inputport.hpp"
#include "IOHandler_etaslvar_deriv_inputport.hpp"
#include "IOHandler_etaslvar_frame_inputport.hpp"
#include "IOHandler_etaslvar_frame_outputport.hpp"
#include "IOHandler_etaslvar_deriv_twist_inputport.hpp"
#include "IOHandler_etaslvar_deriv_outputport.hpp"
#include "port_observer.hpp"
#include <expressiongraph/solver.hpp>
#include <limits>
#include <boost/algorithm/string.hpp>

using namespace KDL;
using namespace Eigen;
using namespace std;

bool initialize_feature_variables( Context::Ptr ctx, solver& solver, double initialization_time, double sample_time, double convergence_crit) {
    RTT::os::TimeService::ticks start_time = RTT::os::TimeService::Instance()->getTicks();
    // initialization:
    if (solver.getNrOfFeatureStates() > 0 ) {
        RTT::log(RTT::Info) << "Iniitialization started" << RTT::endlog();
        double t;
        for (t=0;t<initialization_time;t+=sample_time) {
            int retval = solver.updateStep(sample_time);
            if (retval!=0) {
                RTT::log(RTT::Error) << "initialize_feature_variables: solver encountered the following error during initialization (t="<<t<<" )" << RTT::endlog();
                RTT::log(RTT::Error) << solver.errorMessage(retval)  << RTT::endlog();
                RTT::log(RTT::Error) << ctx << RTT::endlog();
                return false;
            }
            double norm_change=solver.getNormChange()*sample_time;
            RTT::log(RTT::Info) << "norm change : " << norm_change  << RTT::endlog();
            if (norm_change <= convergence_crit) break;
        }
        double elapsed =  RTT::os::TimeService::Instance()->secondsSince( start_time);
        RTT::log(RTT::Info) << "Initialization time("<< ceil(t/sample_time) <<"  iterations) : " << elapsed << "[s]" << RTT::endlog();
    }
    solver.setInitialValues(); // sets the initial value fields of variables in the context.
    //solver.printMatrices(std::cout); 
    return true;
}


bool etasl_rtt::check_state_etaslvar_port(const std::string& name) {
    if (( this->getTaskState()!=PreOperational )||(!etaslread)) {
        RTT::log(RTT::Error) << name << " can only be used in the PreOperational & eTaSL_read state"<<RTT::endlog();
        return false;
    } else {
        return true;
    }
}

bool etasl_rtt::check_is_initialized(const std::string& name) {
    if (!initialized) {
        RTT::log(RTT::Error) << name << errormsg::gen_rtt_error(errormsg::ERROR_INIT,errormsg::YELLOW) << RTT::endlog();
        return false;
    }
    return true;
}

bool etasl_rtt::check_spec_loaded(const std::string& name) {
    if (!etaslread) {
        RTT::log(RTT::Error) << name << errormsg::gen_rtt_error(errormsg::ERROR_ELOAD) << RTT::endlog();
        return false;
    }
    return true;
}

etasl_rtt::etasl_rtt(std::string const& name) :
    etasl_iohandler_rtt(name),
    createSolver("createSolver"),
    cstate(new SolverState::Ptr()),
    initialization_time(3.0),
    initialization_time_step(0.05),
    convergence_criterion(1E-4),
    debug_if_solver_error(false),
    disable_solver(false),
    solve_max_iterations(200),
    solve_time_step(0.1),
    solve_convergence_crit(1E-5),
    timing_policy("measured_old"),
    skill_to_remove("NONE")
{
    RTT::log(RTT::Info) << "etasl_rtt::constructor" << RTT::endlog();

    this->requires("solver")->addOperationCaller(createSolver);
    this->addProperty("initialization_time",initialization_time).doc("maximum simulated  time duration to be used to initialize feature variables");
    this->addProperty("initialization_time_step",initialization_time_step).doc("Simulated time step to be used to initialize feature variables");
    this->addProperty("convergence_criterion",convergence_criterion).doc("convergence criteria for the initialization of feature variables");
    this->addProperty("debug_if_solver_error",debug_if_solver_error).doc("call a debug consol inside the context if a solver error occurrs (DEBUGGING, CAREFUL WHEN REAL ROBOT IS CONNECTED!) ");
    this->addProperty("disable_solver",disable_solver).doc("disable the solver but still compute the value of the expressions.  No feature variables allowed in specification.");
    this->addProperty("solve_max_iterations",solve_max_iterations)
        .doc("maximum iterations to use when calling solve()");
    this->addProperty("solve_time_step", solve_time_step)
        .doc("time step to use when calling solve(), choose this together with the control gains in your etasl specification");
    this->addProperty("solve_convergence_crit",solve_convergence_crit)
        .doc("converge_criterion to use when calling solve().  Will stop iterating if updates are smaller than this criterion");
    this->addProperty("event_postfix", event_postfix)
        .doc("postfix to be used when emitting events");
    event_postfix = getName();
    this->addProperty("timing_policy", timing_policy)
        .doc("Timing policy: how sample time is computed: 'nominal' uses getPeriod(), 'measured' uses time measurements, 'measured_old' for the timing_policy of old versions of etasl_rtt");
    this->addOperation("activation_command", &etasl_rtt::activation_command, this, RTT::OwnThread)
        .doc("sends an grouping activation command to the context to activate a particular group")
        .arg("cmd","activation command: comma or space seperated list of +groupname, -groupname, or +/-partial_name*");
    this->addOperation("readTaskSpecificationFile", &etasl_rtt::readTaskSpecificationFile, this, RTT::OwnThread)
        .doc("read an eTaSL task specification file (lua) ")
        .arg("filename","the name of the file to read");
    this->addOperation("readTaskSpecificationString", &etasl_rtt::readTaskSpecificationString, this, RTT::OwnThread)
        .doc("executes a fragment of an eTaSL specification string")
        .arg("cmd","a fragment of eTaSL specification string");
    this->addOperation("readTaskSpecificationFileRunTime", &etasl_rtt::readTaskSpecificationFileRunTime, this, RTT::OwnThread)
        .doc("read an eTaSL task specification file (lua) during runtime")
        .arg("filename","the name of the file to read");
    this->addOperation("removeSkillRunTime", &etasl_rtt::removeSkillRunTime, this, RTT::OwnThread)
        .doc("remove a skill from the solver during runtime")
        .arg("filename","the name of the skill to remove");
    this->addOperation("activateTransientWeighting", &etasl_rtt::activateTransientWeighting, this, RTT::OwnThread)
        .doc("signals the solver to start calculating the transient skills weighting");   
    this->addOperation("getSkillFromStack", &etasl_rtt::getSkillFromStack, this, RTT::OwnThread)
        .doc("get skill id from the stack by giving an integer enumerator");          
    this->addOperation("removeMonitors", &etasl_rtt::removeMonitors, this, RTT::OwnThread)
        .doc("remove monitors based on the groupname");    
    this->addOperation("setMonitorArgument", &etasl_rtt::setMonitorArgument, this, RTT::OwnThread)
        .doc("change a scalar monitor argument (supports runtime call)");    
    this->addOperation("addSkillToStackList", &etasl_rtt::addSkillToStackList, this, RTT::OwnThread)
        .doc("add to skill stack list")
        .arg("skill_id", "the unique id of the skill to be added")
        .arg("priority", "the priority level");
    this->addOperation("etasl_console", &etasl_rtt::etasl_console, this, RTT::OwnThread)
        .doc("goes to a lua console directly into the eTaSL environment, to interactivly query the specification");
    this->addOperation("printMatrices", &etasl_rtt::printMatrices, this, RTT::OwnThread)
        .doc("prints out the matrices involved in solving for the current step (DEBUGGING)");

    this->addOperation("getLocalTime", &etasl_rtt::getLocalTime, this, RTT::OwnThread)
        .doc("gets the local time of the component, i.e. since the start of the component");

    this->addOperation("initialize", &etasl_rtt::initialize, this, RTT::OwnThread)
        .doc("initializes feature variables with priority==0 constraints before starting");

    this->addOperation("solve", &etasl_rtt::solve, this, RTT::OwnThread)
        .doc("apply the constraints it is converged (i.e. inverse pos. kinematics)")
        .arg("jnames","ordered list of joint names")
        .arg("initial_val","initial values for the joint names, ignored if size==0")
        .arg("values","resulting joint values after solving")
        .arg("result","returns the weighted square of the slack variables, i.e. how much the (soft) constraints are violated");

    // factory operations for IOhandlers:
    this->addOperation("add_activation_inputport",&etasl_rtt::add_activation_inputport,this,RTT::OwnThread)
        .doc("adds an IOHandler that reads eTaSL activation commands from a (string) input port")
        .arg("portname","name of the port")
        .arg("portdocstring","documentation for the port")
        .arg("initial_cmd","activation command to be given to eTaSL at initialization time");
    this->addOperation("add_controller_outputport",&etasl_rtt::add_controller_outputport,this,RTT::OwnThread)
        .doc("adds an IOHandler that puts the computed joint velocities of the controller on an outputport")
        .arg("portname","name of the port")
        .arg("portdocstring","documentation for the port")
        .arg("jointnames","list of jointnames describing the contents of the outputport");
    this->addOperation("add_controller_pos_outputport",&etasl_rtt::add_controller_pos_outputport,this,RTT::OwnThread)
        .doc("adds an IOHandler that puts the joint POSITIONS on an outputport")
        .arg("portname","name of the port")
        .arg("portdocstring","documentation for the port")
        .arg("jointnames","list of jointnames describing the contents of the outputport");
    this->addOperation("add_controller_motion_control_msgs_joint_velocities_outputport",&etasl_rtt::add_controller_motion_control_msgs_joint_velocities_outputport,this,RTT::OwnThread)
        .doc("adds an IOHandler that puts the joint velocities on an outputport using JointState::Messages")
        .arg("portname","name of the port")
        .arg("portdocstring","documentation for the port")
        .arg("jointnames","list of jointnames describing the contents of the outputport");

    this->addOperation("add_controller_inputport",&etasl_rtt::add_controller_inputport,this,RTT::OwnThread)
        .doc("adds an IOHandler that read measured joint position values from an outputport")
        .arg("portname","name of the port")
        .arg("portdocstring","documentation for the port")
        .arg("jointnames","list of jointnames describing the contents of the inputport");
    this->addOperation("add_etaslvar_deriv_outputport",&etasl_rtt::add_etaslvar_deriv_outputport,this,RTT::OwnThread)
        .doc("adds an IOHandler that puts the velocity of an eTaSL output expression on an outputport")
        .arg("portname","name of the port")
        .arg("portdocstring","documentation for the port")
        .arg("varnames","list of variable names describing the contents of the outputport");

    this->addOperation("disable_controller_output",&etasl_rtt::disable_controller_output,this,RTT::OwnThread)
        .doc("the controller output velocities are disabled. Zero velocties are send out by this component");

    this->addOperation("enable_controller_output",&etasl_rtt::enable_controller_output,this,RTT::OwnThread)
        .doc("enable the controller output velocities.")
        .arg("safety_tolerance","only enable when the norm of the velocities is below this level");

    this->addOperation("scale_controller_output",&etasl_rtt::scale_controller_output,this,RTT::OwnThread)
        .doc("scales the controller output velocities.")
        .arg("vel_scale","scale factor for output vel., feature vel. and time progression");

    this->addOperation("normOutputVelocities",&etasl_rtt::normOutputVelocities,this,RTT::OwnThread)
        .doc("gives back the norm of the most recently computed output velocities");
    this->addOperation("get_etasl_value_double",&etasl_rtt::get_etasl_value_double, this, RTT::OwnThread).doc("returns a double value given a lua statement that returns a value").arg("lua_statement","lua statement that returns a double");

    this->addOperation("get_etasl_value_string",&etasl_rtt::get_etasl_value_string, this, RTT::OwnThread).doc("returns a string value given a lua statement that returns a value").arg("lua_statement","lua statement that returns a string");

    this->addOperation("clean_interfaces",&etasl_rtt::clean_interfaces, this, RTT::OwnThread).doc("cleanups all RTT input/output interfaces");
    this->addOperation("clean_specification",&etasl_rtt::clean_specification, this, RTT::OwnThread).doc("clean the loaded eTaSL task specification");
    
    this->addOperation("emit_event",&etasl_rtt::emit_event, this, RTT::OwnThread).doc("emit_event");

    this->addPort("eventPort",eventPort).doc("the component publishes its event on this string port");
    this->addPort("elapsedTimePort",elapsedTimePort).doc("the elapsed time since the last call to updateHook");
    clean_interfaces();
    reinitialize_data_structures();
    this->addPort("computingTimePort",computingTimePort).doc("the elapsed time since the last call to updateHook");
    
    /* for transient weighting (runtime skill composition) */
    this->addPort("wa", wa).doc("transient weight of skill A");
    this->addPort("wb", wb).doc("transient weight of skill B");
    this->addPort("transient_time", transient_time).doc("transient weight of skill B");    
}

void etasl_rtt::emit_event(const std::string& event) {
        std::string ename;
        if (event_postfix.size()!=0) {
            ename = event + "@" + event_postfix;
        } else {
            ename = event;
        }
        RTT::log(RTT::Info) << "(etasl_rtt) " << ename <<" generated." << RTT::endlog();
        eventPort.write(ename);
}

bool etasl_rtt::clean_interfaces() {
    if(!check_is_preoperational("clean_interfaces")) return false;
    if(!ihc->cleanup_component()) {
        RTT::log(RTT::Error) << "I-handlers: " << errormsg::gen_io_error(errormsg::ERROR_IO_CLEANUP,errormsg::BOLDMAGENTA) << " - " << ihc->getErrorMsg() << RTT::endlog();
        return false;
    }
    if(!ohc->cleanup_component()) {
        RTT::log(RTT::Error) << "O-handlers: " << errormsg::gen_io_error(errormsg::ERROR_IO_CLEANUP,errormsg::BOLDMAGENTA) << " - " << ohc->getErrorMsg() << RTT::endlog();
        return false;
    }
    ihc->clear();
    ohc->clear();
    controller_input_defined=false;
    controller_output_defined=false;
    return true;
}

bool etasl_rtt::clean_specification() {
    if (this->getTaskState()!=PreOperational) {
        RTT::log(RTT::Error) << "clean_specification operation is available only in stop state" << RTT::endlog();
        return false;
    }
    initialized=false;
    etaslread=false;
    reinitialize_data_structures();
    return true;
}

bool etasl_rtt::reinitialize_data_structures() {
    Context::Ptr ctx = create_context();
    *cctx = ctx;
    ctx->addType("robot");
    ctx->addType("feature");
    lua = boost::make_shared<RTTLuaContext>();
    // define a variable that only depends on time
    lua->initContext(ctx);
//     state.reset( new SolverState );
    *cstate = SolverState::Ptr( new SolverState );
//     ihc->clear();
//     ohc->clear();

    // install observers:
    Observer::Ptr obs = create_port_observer( ctx, &eventPort, "portevent","",false);
    obs = create_port_observer( ctx, &eventPort, "event",event_postfix,false, obs);
    obs = create_port_observer( ctx, &eventPort, "exit", event_postfix,true, obs);
    //obs = create_default_observer(ctx,"exit",obs);
    ctx->addDefaultObserver(obs);
    initialized=false;
    etaslread=false;
//     controller_input_defined=false;
//     controller_output_defined=false;
}

bool etasl_rtt::readTaskSpecificationFile(const std::string& filename) {
    if (this->getTaskState()!=PreOperational) {
        RTT::log(RTT::Error) << "readTaskSpecificationFile can only be used in the PreOperational state"<<RTT::endlog();
        return false;
    }
    int retval = lua->executeFile(filename);
    if (retval!=0) {
        RTT::log(RTT::Error) << getName() + " : error during readTaskSpecificationFile"<<RTT::endlog();
    } else {
        etaslread=true;
    }
    return retval==0;
}

bool etasl_rtt::readTaskSpecificationString(const std::string& cmd) {
    int retval = lua->executeString(cmd);
    if (retval!=0) {
        RTT::log(RTT::Error) << getName() + " : error during readTaskSpecificationString"<<RTT::endlog();
    } else {
        etaslread=true;
    }
    return retval==0;
}

void etasl_rtt::addSkillToStackList(const std::string& skill_id, int priority) {
    (*cctx)->priority_map.insert(std::pair<std::string, int>(skill_id, priority));    
}

bool etasl_rtt::readTaskSpecificationFileRunTime(const std::string& filename, const std::string& groupname, int priority, bool transient) {
    // check whether a runtime operation is still in process.
    // if yes, then return false
    if ((*cctx)->transient_flag) 
        return false;
    
    // assign a group name to the newly added specification
    std::string groupstr = "ctx:pushGroup('";
    groupstr.append(groupname);
    groupstr.append("')");
    readTaskSpecificationString(groupstr);
    
    if (this->getTaskState()!=Running) {
        RTT::log(RTT::Error) << "readTaskSpecificationFileRunTime can only be used in the Running state"<<RTT::endlog();
        return false;
    }
    int retval = lua->executeFile(filename);
    if (retval!=0) {
        RTT::log(RTT::Error) << getName() + " : error during readTaskSpecificationFileRunTime"<<RTT::endlog();
    } else {
        etaslread=true;
    }
          
    // compare the new skill priority with the existing ones
    if (transient) {
       (*cctx)->transient_skills.insert(std::pair<std::string, int>(groupname, -1));
       (*cctx)->transient_flag = true;            
       std::cout << "-----------!!!!!!!!!! setting transient flag to TRUE (ADDITION) ********" << std::endl;
    } else {
       for (std::map<std::string,int>::iterator it=(*cctx)->priority_map.begin(); it!=(*cctx)->priority_map.end(); ++it) {
          if (priority < it->second) {
             // If priority is higher than the lowest, then transient is needed
             (*cctx)->transient_skills.insert(std::pair<std::string, int>(groupname, -1));
             (*cctx)->transient_flag = true; 
             break; // exit the loop
          }
       }
    } 
    
    // update the priority level map
    (*cctx)->priority_map.insert(std::pair<std::string, int>(groupname, priority));
    
    // update the lowest priority skills
    (*cctx)->lowest_priority_level = 1;
    (*cctx)->lowest_priority_skills.clear();
    // first, find the lowest priority level
    for (std::map<std::string,int>::iterator it=(*cctx)->priority_map.begin(); it!=(*cctx)->priority_map.end(); ++it) {
       if ((*cctx)->lowest_priority_level < it->second) {
           (*cctx)->lowest_priority_level = it->second;
       }
    }    
    // then, bookkeep the skill whose priority level equals lowest_priority_level
    for (std::map<std::string,int>::iterator it=(*cctx)->priority_map.begin(); it!=(*cctx)->priority_map.end(); ++it) {
       if (it->second == (*cctx)->lowest_priority_level) {
           (*cctx)->lowest_priority_skills.push_back(it->first);           
       }
    }
    
    // activate the newly added group since it is not activated by default
    groupstr = "global.";
    groupstr.append(groupname);    
    (*cctx)->activate(groupstr); 
    
    // prepare for the execution with the full problem, and fills in variables from their initial field.
    retval = solver->prepareExecutionRunTime(*cctx);
    if (retval!=0) {
        RTT::log(RTT::Error) << getName() << " : etasl_rtt::initialize() - prepareExecution : the taskspecification contains priority levels that the numerical solver can't handle. "<< RTT::endlog();
        return false;
    }    
        
    (*cstate)->initialize( *cctx, *solver);

//     std::cout << (*cstate)->fvalues[0] << " " << (*cstate)->fvalues[1] << " " << (*cstate)->fvalues[2] << " " << (*cstate)->fvalues[3] <<
//     " " << (*cstate)->fvalues[4] << " " << (*cstate)->fvalues[5] << " " << std::endl;

//     disable_solver = true;
    // assign a group name to the newly added specification
    groupstr = "ctx:popGroup()";
    readTaskSpecificationString(groupstr);
    
    return retval==0;
}

bool etasl_rtt::removeSkillRunTime(const std::string& skillname, bool transient) {
    // check whether a runtime operation is still in process.
    // if yes, then return false
    if ((*cctx)->transient_flag) 
        return false;
        
    // TODO: check whether the skill exist in the stack
    
    
    // update the list of skills to be removed
    (*cctx)->skill_to_remove = skillname;
    skill_to_remove = skillname;
    
    // if transient is set to true, then update the transient skill list (and assign an arbitrary priority level)
    if (transient) {
       std::cout << "-----------!!!!!!!!!! setting transient flag to TRUE (REMOVAL) ********" << std::endl;
       (*cctx)->transient_skills.insert(std::pair<std::string, int>(skillname, -1));
       (*cctx)->transient_flag = true;              
    }
    
    // update the lowest priority skills
    (*cctx)->lowest_priority_level = 1;
    (*cctx)->lowest_priority_skills.clear();
    // first, find the lowest priority level
    for (std::map<std::string,int>::iterator it=(*cctx)->priority_map.begin(); it!=(*cctx)->priority_map.end(); ++it) {
       if ((*cctx)->lowest_priority_level < it->second) {
           (*cctx)->lowest_priority_level = it->second;
       }
    }    
    // then, bookkeep the lowest priority skills
    for (std::map<std::string,int>::iterator it=(*cctx)->priority_map.begin(); it!=(*cctx)->priority_map.end(); ++it) {
       if (it->second == (*cctx)->lowest_priority_level) {
           (*cctx)->lowest_priority_skills.push_back(it->first);           
       }
    } 
    
    // ask solver to remove the skill
    solver->removeSkillRunTime(*cctx);
    
    // immediately cleanup the IO handlers if no transient is asked
    if (!transient) {
        ihc->clear_skill_io(skillname);
        ohc->clear_skill_io(skillname);
        
        // also remove the etasl IO channels expressions
        (*cctx)->removeIOChannel(skillname);
    }
    
    return true;
}

bool etasl_rtt::activateTransientWeighting(const std::string& skillname, const std::vector<std::string>& varnames, std::vector<double> values) {
    solver->computeTransientInitialTime();
    (*cctx)->activate_transient_weighting = true;  
//     std::string varname = skillname+".w_enable";
// //     std::cout << "varname: " << varname << std::endl;
//     set_etaslvar(varname, 1);
    set_etaslvars(varnames, values);
    return true;
}

std::string etasl_rtt::getSkillFromStack(int idx){
//     std::cout << "Calling getSkillFromStack" << std::endl;
    int count = 0;
    std::string skill_id = "empty";
    for (std::map<std::string,int>::iterator it=(*cctx)->priority_map.begin(); it!=(*cctx)->priority_map.end(); ++it) {
       if (count==idx) {
           skill_id = it->first;
           return skill_id;
       }
       count++;
    }
    return skill_id;
}

bool etasl_rtt::removeMonitors(const std::string& skillname) {
  solver->deleteMonitors(skillname);
}

bool etasl_rtt::setMonitorArgument(const std::string& monitorname, const std::string& argument) {
  (*cctx)->setMonitorArgument(monitorname, argument);
}

bool etasl_rtt::etasl_console() {
    if (isRunning()) {
        RTT::log(RTT::Error) << "etasl_rtt::etasl_console not allowed while running" << RTT::endlog();
        return false;
    }
    go_to_safe_state();
    assert(lua!=NULL);
    assert((*cctx)!=NULL);
    int retval = lua->call_console();
    return retval==0;
}
void etasl_rtt::printMatrices() {
    if (!solver) {
        RTT::log(RTT::Error) << "etasl_rtt::printMatrices() called on unconfigured object." << RTT::endlog();
        return;
    }
    solver->printMatrices(std::cout);
    //std::cout << "Time  : "   << state->time << std::endl;
    //std::cout << "Robot variables : \n"   << state->jvalues.transpose() << std::endl;
    //std::cout << "Feature variables : \n" << state->fvalues.transpose() << std::endl;
}
bool etasl_rtt::add_activation_inputport( const std::string& portname, const std::string& portdocstring, const std::string& initial_cmd) {
    if (this->getTaskState()!=PreOperational) {
        RTT::log(RTT::Error) << "add_activation_inputport() can only be used in the PreOperational state"<<RTT::endlog();
        return false;
    }
    IOHandler::Ptr h( new KDL::IOHandler_activation_inputport( this, cctx, portname, portdocstring, initial_cmd) );
    if (h->configure_component() ) {
        ihc->addHandler( h );
        return true;
    } else {
        RTT::log(RTT::Error) << "add_activation_inputport() failed to configure RTT interfaces"<<RTT::endlog();
        return false;
    }
}

bool etasl_rtt::add_controller_outputport( const std::string& portname, const std::string& portdocstring, const std::vector<std::string>& jointnames) {
//     if ((this->getTaskState()!=PreOperational)) { //|| (!etaslread) ) {
//         RTT::log(RTT::Error) << "add_controller_outputport() can only be used in the PreOperational state"<<RTT::endlog();
//         return false;
//     }
    if (!check_is_preoperational("add_controller_outputport")) return false;
    IOHandler::Ptr h( new KDL::IOHandler_controller_outputport( this, cstate, jointnames, portname, portdocstring) );
    if (h->configure_component() ) {
        ohc->addHandler( h );
        controller_output_defined = true;
        return true;
    } 
    handler_config_failed(portname,"add_controller_outputport");
    return false;
}

bool etasl_rtt::add_controller_pos_outputport( const std::string& portname, const std::string& portdocstring, const std::vector<std::string>& jointnames) {
    if (!check_is_preoperational("add_controller_pos_outputport")) return false;
    IOHandler::Ptr h( new KDL::IOHandler_controller_pos_outputport( this, cstate, jointnames, portname, portdocstring) );
    if (h->configure_component() ) {
        ohc->addHandler( h );
        return true;
    } 
    handler_config_failed(portname,"add_controller_pos_outputport");
    return false;
}
bool etasl_rtt::add_controller_motion_control_msgs_joint_velocities_outputport( const std::string& portname, const std::string& portdocstring, const std::vector<std::string>& jointnames) {
//     if (this->getTaskState()!=PreOperational) {
//         RTT::log(RTT::Error) << "add_controller_motion_control_msgs_joint_velocities_outputport() can only be used in the PreOperational state"<<RTT::endlog();
//         return false;
//     }
    if (!check_is_preoperational("add_controller_motion_control_msgs_joint_velocities_outputport")) return false;
    IOHandler::Ptr h( new KDL::IOHandler_controller_motion_control_msgs_joint_velocities_outputport( this, cstate, jointnames, portname, portdocstring) );
    if (h->configure_component() ) {
        ohc->addHandler( h );
        controller_output_defined = true;
        return true;
    } 
    handler_config_failed(portname,"add_controller_motion_control_msgs_joint_velocities_outputport");
    return false;
}

bool etasl_rtt::add_controller_inputport( const std::string& portname, const std::string& portdocstring, const std::vector<std::string>& jointnames) {
    if (!check_is_preoperational("add_controller_inputport")) return false;
    IOHandler::Ptr h( new KDL::IOHandler_controller_inputport( this, cstate, jointnames, portname, portdocstring) );
    if (h->configure_component() ) {
        ihc->addHandler( h );
        controller_input_defined = true;
        return true;
    }
    handler_config_failed(portname,"add_controller_inputport");
    return false;
}

bool etasl_rtt::add_etaslvar_deriv_outputport( const std::string& portname, const std::string& portdocstring, const std::vector<std::string>& varnames) {
    if (!check_state_etaslvar_port("add_etaslvar_deriv_outputport")) return false;
    IOHandler::Ptr h( new KDL::IOHandler_etaslvar_deriv_outputport( this, cctx, cstate, varnames, portname, portdocstring) );
    if (h->configure_component() ) {
        ohc->addHandler( h );
        return true;
    }
    handler_config_failed(portname,"add_etaslvar_deriv_outputport");
    return false;
}








// TODO: ensure that the component is stopped, and that all output is properly set.
void etasl_rtt::go_to_safe_state() {
}

bool etasl_rtt::configureHook() {
    if (!etaslread) {
        RTT::log(RTT::Error) << "configureHook() can only be called when a specification has been read"<<RTT::endlog();
        return false;
    }
    if (timing_policy=="nominal") {
        timing_policy_nr = 0;
    } else if (timing_policy=="measured_old") {
        timing_policy_nr = 1;
    } else if (timing_policy=="measured") {
        timing_policy_nr = 2;
    } else {
        RTT::log(RTT::Error) << "configureHook() timing_policy property is invalid"<<RTT::endlog();
        return false;
    }
    if  ( (this->getTaskState()!=PreOperational) &&
        (this->getTaskState()!=Stopped)            ) {
        RTT::log(RTT::Error) << "configureHook() can only be called in the PreOperational or Stopped state"<<RTT::endlog();
        return false;
    }
    if (!ihc->attach_to_etasl() ) {
        RTT::log(RTT::Error) << "I-handlers: " << errormsg::gen_io_error(errormsg::ERROR_IO_ATTACH,errormsg::BOLDMAGENTA) << " - " << ihc->getErrorMsg() << RTT::endlog();
      return false;
    }
    if (!ihc->verify() ) {
        RTT::log(RTT::Error) << "I-handlers: " << errormsg::gen_io_error(errormsg::ERROR_IO_VERIFY,errormsg::BOLDMAGENTA) << " - " << ihc->getErrorMsg() << RTT::endlog();
        return false;
    }
    if (!ohc->attach_to_etasl() ) {
        RTT::log(RTT::Error) << "O-handlers: " << errormsg::gen_io_error(errormsg::ERROR_IO_ATTACH,errormsg::BOLDMAGENTA) << " - " << ohc->getErrorMsg() << RTT::endlog();
        return false;
    }
    if (!ohc->verify() ) {
        RTT::log(RTT::Error) << "O-handlers: " << errormsg::gen_io_error(errormsg::ERROR_IO_VERIFY,errormsg::BOLDMAGENTA) << " - " << ohc->getErrorMsg() << RTT::endlog();
        return false;
    }
    if (disable_solver) {
        std::vector<int>    fv_ndx;
        Context::Ptr ctx = *cctx;
        fv_ndx.clear();
        ctx->getScalarsOfType("feature",fv_ndx);
        if (fv_ndx.size()!=0) {
            RTT::log(RTT::Error) << "solver is disabled and there are feature variables defined.  "
                                 << "Probably not wat you want.  Any expression dependend on the "
                                 << "feature variables will be invalid because there is no solver." << RTT::endlog();
            return false;
        }
        if (controller_output_defined) {
            RTT::log(RTT::Warning) << "Solver is disabled and an output port for controller velocities is connected - this is probably not what you want " << RTT::endlog();
        }
    }
    if (!controller_input_defined) {
        RTT::log(RTT::Warning) << "No input is connected to the solver - this is probably not what you want" << RTT::endlog();
    }
    RTT::log(RTT::Info) << "etasl_rtt::configure()" << RTT::endlog();
    sample_time = this->getPeriod();

    initialized=false;
    if (solver) {
        RTT::log(RTT::Info) << "solver_type is " << solver->getName() << RTT::endlog();
    } else {
        RTT::log(RTT::Error) << "No solver has been specified \n" 
                             << "use one of the external solver components (e.g. etasl_solver_qpoases, etasl_solver_sot) to create \n" 
                             << "a solver and set it as the solver of this component \n"
                             << "(before calling configure() )\n"
                             << RTT::endlog();
        return false;
    }
    return true;
}


// the order of calling the different functions is a little bit tricky...
bool etasl_rtt::initialize() {
    if (this->getTaskState()!=Stopped) {
        RTT::log(RTT::Error) << "initialize() can only be called in the Stopped state"<<RTT::endlog();
        return false;
    }
    RTT::log(RTT::Info) << "etasl_rtt::initialize()" << RTT::endlog();
    
    // prepares matrices etc and initializes variables from their "initial" field (including feature variables)
    int retval = solver->prepareInitialization(*cctx);
    if (retval!=0) {
        RTT::log(RTT::Error) << getName() << " : etasl_rtt::initialize() - prepareInitialization : the taskspecification contains priority levels that the numerical solver can't handle. "<< RTT::endlog();
        return false;
    }
    // creates data structures for state and fills in values from the solver:
    (*cstate)->initialize( *cctx, *solver);
    (*cstate)->setTime(0.0);
    // typically a handler is provided to set the variable values in the state.
    if (!ihc->update() ) {
        RTT::log(RTT::Error) << "input handler failed to update: " << ihc->getErrorMsg() << RTT::endlog();
        emit_event("e_error");
        stop();
        return false;
    }
    // fills the state values into the solver data structure
    (*cstate)->setSolverInput(*solver);
    // function defined in this file:  evaluates reduced problem until feature variables are converged
    // (for the priority==0 constraints.
    initialize_feature_variables( *cctx, *solver, initialization_time, initialization_time_step, convergence_criterion);
    // prepare for the execution with the full problem, and fills in variables from their initial field.
    retval = solver->prepareExecution(*cctx);
    if (retval!=0) {
        RTT::log(RTT::Error) << getName() << " : etasl_rtt::initialize() - prepareExecution : the taskspecification contains priority levels that the numerical solver can't handle. "<< RTT::endlog();
        return false;
    }

    (*cstate)->initialize( *cctx, *solver);
    (*cstate)->setTime(0.0);
    (*cctx)->resetMonitors();
    (*cctx)->clearFinishStatus();
    initialized=true;
    // evaluate but do not write the output.  Just to initialise the QP solver, such that
    // the next time it will use hotstart()
    if (!ihc->update() ) {
        RTT::log(RTT::Error) << "Output handler failed to update: " << ohc->getErrorMsg() << RTT::endlog();
        emit_event("e_error");
        stop();
        return false;
    }
    (*cstate)->setSolverInput(*solver);
    if (!disable_solver) {
        int retval = solver->solve();
        if (retval!=0) {
            RTT::log(RTT::Error) << "solver encountered the following error during the first solve in initialize() (t=" 
                                 << (*cstate)->time << " ) \nmessage: " 
                                 << solver->errorMessage(retval).c_str() 
                                 << "\n stop() will be called on etasl rtt component and e_error event will be send"
                                 << "\n"
                                 << RTT::endlog();
            printMatrices();
            emit_event("e_error");
            stop();
            if (debug_if_solver_error) {
                etasl_console();
            }
            return false;
        }
    }
    return true;
}

bool etasl_rtt::startHook(){
  if (!check_is_initialized("etasl_rtt::startHook()")) return false;
  if(!ihc->initialize()) {
      RTT::log(RTT::Error) << "I-handlers: " << errormsg::gen_io_error(errormsg::ERROR_IO_INIT,errormsg::BOLDMAGENTA) << " - " << ihc->getErrorMsg() << RTT::endlog();
        return false;
  }
  if(!ohc->initialize()) {
      RTT::log(RTT::Error) << "O-handlers: " << errormsg::gen_io_error(errormsg::ERROR_IO_INIT,errormsg::BOLDMAGENTA) << " - " << ohc->getErrorMsg() << RTT::endlog();
        return false;
  }
  RTT::log(RTT::Info) << "etasl_rtt::startHook()" << RTT::endlog();
  timestamp_from_start = RTT::os::TimeService::Instance()->getTicks();
  time_start           = (*cstate)->time;
  timestamp_step = RTT::os::TimeService::Instance()->getTicks();
  first_time_after_start = true;
  old_fvelocities        = (*cstate)->fvelocities;
  return true;
}

double etasl_rtt::getLocalTime() {
    return (*cstate)->time;
}


// only to be called when component is not running.
bool etasl_rtt::activation_command(const std::string& cmd) {
    if (this->getTaskState()!=Stopped) {
        RTT::log(RTT::Error) << "etasl_rtt::activate_command needs a component that is in  the stopped state" << RTT::endlog();
        return false;
    }
    bool result = (*cctx)->activate_cmd(cmd);
    if (!result) {
        RTT::log(RTT::Warning) << "etasl_rtt::activate_command() did not succeed" << RTT::endlog();
    }
    return result;
}

/*
bool etasl_rtt::get_controller_pos(const std::vector<std::string>& jnames, std::vector<double>& values) {
        values.resize( jnames.size() );
        for (unsigned int i=0;i<jnames.size();++i) {
            std::map<std::string,int>::iterator it = state->jindex.find(jnames[i]);
            if (it!=state->jindex.end()) {
                values[i] = state->jvalues[it->second];
            } else {
                RTT::log(RTT::Error) << "get_controller_pos: unknown name of robot joint variable : "
                                     <<  jnames[i]
                                     << RTT::endlog();
                return false;
            }
        }
        return true;
}
*/
//
// expected nr of it:  err_init*exp(-K t) < conv_crit   ===> it = log( err_init / conv_crit) / K / time_step
// returns < 1 if there is an error or non-convergence.
//  0 : success
// -1 : only in stopped state ( no initialize nodig).
// -2 : inputhandler failed.
// -3 : error during qpOASIS solve
// -4 : not converged after max_iterations cycles
// -5 : outputhandler failed.
// -6 : initial_val is not of size 0 and is not equal in size to jnames
// -7 : joint names does not exist.
int etasl_rtt::solve(const std::vector<std::string>& jnames, const std::vector<double>& initial_val, std::vector<double>& values,
       double& result ){
    if (!check_is_stopped("solve()")) return -1;
//     if (this->getTaskState()!=Stopped) {
//         RTT::log(RTT::Error) << "solve() can only be called in the Stopped state"<<RTT::endlog();
//         return -1;
//     }
    int retval = solver->prepareExecution(*cctx);
    if (retval!=0) {
        RTT::log(RTT::Error) << getName() << " : etasl_rtt::solve() : the taskspecification contains priority levels that the numerical solver can't handle. "<< RTT::endlog();
        return false;
    }

    (*cstate)->initialize( *cctx, *solver);
    (*cstate)->setTime(0.0);
    if (!ihc->update() ) {
        RTT::log(RTT::Error) << "I-handlers: " << errormsg::gen_io_error(errormsg::ERROR_IO_UPDATE,errormsg::BOLDMAGENTA) << " - " << ihc->getErrorMsg() << RTT::endlog();
        return -2;
    }
    if (initial_val.size()!=0) {
        if (initial_val.size()!=jnames.size()) {
            RTT::log(RTT::Error) << "solve():initial_val should be either equal in size to jnames or of size 0" << RTT::endlog();
            return -6;
        }
        for (unsigned int i=0;i<jnames.size();++i) {
            std::map<std::string,int>::iterator it = (*cstate)->jindex.find(jnames[i]);
            if (it!=(*cstate)->jindex.end()) {
                (*cstate)->jvalues[it->second] = initial_val[i];
            } else {
                RTT::log(RTT::Error) << "solve: unknown name of robot joint variable : "
                                     <<  jnames[i]
                                     << RTT::endlog();
                return -7;
            }
        }
    }
    double _vel_scale = (*cstate)->vel_scale;
    (*cstate)->vel_scale = 1.0;
    bool converged = false;
    int i;
    //std::cout << "start iterating" << endl;
    for (i=0;i<solve_max_iterations;i++) {
        (*cstate)->setSolverInput(*solver);
        int retval = solver->solve();
        if (retval!=0) {
            /*RTT::log(RTT::Error) << "solved encountered error during solve" << RTT::endlog();
            RTT::log(RTT::Error) << qpOASESSolver::errorMessage(retval)  << RTT::endlog();
            RTT::log(RTT::Error) << ctx << RTT::endlog();*/
            return -3;
        }
        double norm_change=solver->getNormChange()*solve_time_step;
        //std::cout << "norm change " << norm_change << std::endl;
        if (norm_change <= solve_convergence_crit) {
            converged = true;
            break;
        }
        (*cstate)->getSolverOutput(*solver);
        (*cstate)->fvalues   += (*cstate)->fvelocities*solve_time_step;   // fvelocities are already scaled
        (*cstate)->jvalues   += (*cstate)->jvelocities*solve_time_step;
        (*cstate)->time      += solve_time_step;
        //std::cout << (*cstate)->jvalues.transpose() << std::endl;
    }
    //std::cout << "end iterating" << endl;
    result = solver->getWeightedResult();
    RTT::log(RTT::Info) << "Iterations: " <<  i << RTT::endlog();
    if (!ohc->update() ) {
        RTT::log(RTT::Error) << "O-handlers: " << errormsg::gen_io_error(errormsg::ERROR_IO_UPDATE,errormsg::BOLDMAGENTA) << " - " << ohc->getErrorMsg() << RTT::endlog();
        return -5;
    }
    values.resize( jnames.size() );
    for (unsigned int i=0;i<jnames.size();++i) {
        std::map<std::string,int>::iterator it = (*cstate)->jindex.find(jnames[i]);
        if (it!=(*cstate)->jindex.end()) {
            values[i] = (*cstate)->jvalues[it->second];
        } else {
            RTT::log(RTT::Error) << "get_controller_pos: unknown name of robot joint variable : "
                                 <<  jnames[i]
                                 << RTT::endlog();
            return -7;
        }
    }

    (*cstate)->vel_scale = _vel_scale;
    if (!converged) {
        return -4;
    }
}



/*
bool etasl_rtt::set_controller_pos(const std::vector<std::string>& jnames,
                                       const std::vector<double>&      values) {
        for (unsigned int i=0;i<jnames.size();++i) {
            std::map<std::string,int>::iterator it = (*cstate)->jindex.find(jnames[i]);
            if (it!=(*cstate)->jindex.end()) {
                (*cstate)->jvalues[it->second] = values[i];
            } else {
                RTT::log(RTT::Error) << "set_controller_pos: unknown name of robot joint variable : "
                                     <<  jnames[i]
                                     << RTT::endlog();
                return false;
            }
        }
        return true;
}
*/




void etasl_rtt::updateHook(){
    RTT::os::TimeService::ticks start_of_update = RTT::os::TimeService::Instance()->getTicks();
    if (first_time_after_start) {
        first_time_after_start = false;
        timestamp_step = RTT::os::TimeService::Instance()->getTicks();
        return;
    }

    if (!ihc->update() ) {
       RTT::log(RTT::Error) << "I-handlers: " << errormsg::gen_io_error(errormsg::ERROR_IO_UPDATE,errormsg::BOLDMAGENTA) << " - " << ihc->getErrorMsg() << RTT::endlog();
        emit_event("e_error");
        stop();
        return;
    }
    // order has changed:
    (*cstate)->setSolverInput(*solver);
    if (!disable_solver) {
        int retval = solver->solve();
        if (retval!=0) {
            RTT::log(RTT::Error) << "solved encountered error during computations in updateHook()(t=" 
                                 << (*cstate)->time 
                                 << " ) \nmessage: " << solver->errorMessage(retval).c_str() 
                                 << "\n stop() will be called on etasl rtt component and e_error event will be sent"
                                 << "\n"
                                 << *cctx
                                 << RTT::endlog();
            emit_event("e_error");
            stop();
            if (debug_if_solver_error) {
                etasl_console();
            }
            return;
        }
    } else {
        solver->evaluate_expressions();
    }
    old_fvelocities = (*cstate)->fvelocities;
    (*cstate)->getSolverOutput(*solver);
    if (!disable_solver) {
            double dt;
            if (timing_policy_nr==0) {
                elapsedTimePort.write(
                    RTT::os::TimeService::Instance()->secondsSince( timestamp_step) 
                );
                dt        = this->getPeriod();
                (*cstate)->fvalues   += (*cstate)->fvelocities * dt;   // fvelocities are already scaled
            } else if (timing_policy_nr==1) {
                dt        =  RTT::os::TimeService::Instance()->secondsSince( timestamp_step);
                elapsedTimePort.write(dt);
                (*cstate)->fvalues   += (*cstate)->fvelocities * dt;   // fvelocities are already scaled
            } else if (timing_policy_nr==2) {
                dt        =  RTT::os::TimeService::Instance()->secondsSince( timestamp_step);
                elapsedTimePort.write(dt);
                (*cstate)->fvalues   += old_fvelocities * dt;   // fvelocities are already scaled
            }
            timestamp_step = RTT::os::TimeService::Instance()->getTicks();
            (*cstate)->time      += (*cstate)->vel_scale   * dt;
            double comptime = RTT::os::TimeService::Instance()->secondsSince( start_of_update );
            computingTimePort.write(comptime);
                //time_start  + RTT::os::TimeService::Instance()->secondsSince( timestamp_from_start);
            
            /* for transient weighting (runtime skill composition) */
            wa.write(solver->wa_transient);
            wb.write(solver->wb_transient);
            transient_time.write(solver->transient_time);                        
    }
    (*cctx)->checkMonitors();
    if ((*cctx)->getFinishStatus()) {
        //emit_event("e_finished"); (already send by observer).
        stop();
        return;
    }
    //-------------------------------
    if (!ohc->update() ) {
        RTT::log(RTT::Error) << "O-handlers: " << errormsg::gen_io_error(errormsg::ERROR_IO_UPDATE,errormsg::BOLDMAGENTA) << " - " << ohc->getErrorMsg() << RTT::endlog();
        emit_event("e_error");
        stop();
        return;
    }
    return;
}

void etasl_rtt::stopHook() {
    ohc->finish();
    ihc->finish();

    time_start           =  time_start +
                            RTT::os::TimeService::Instance()->secondsSince( timestamp_from_start);
    RTT::log(RTT::Info) << "etasl_rtt::stopHook() finished  !" << RTT::endlog();
}

void etasl_rtt::disable_controller_output() {
    if (this->getTaskState()!=Running) {
        if ((this->getTaskState()!=Stopped)||(!initialized)) {
            RTT::log(RTT::Error) << "distable_controller_output can only be called in Running() or Stopped&Initialized state"<<RTT::endlog();
        }
    }
    (*cstate)->vel_scale = 0.0;
}


bool etasl_rtt::enable_controller_output(double safety_tolerance) {
    if (this->getTaskState()!=Running) {
        if ((this->getTaskState()!=Stopped)||(!initialized)) {
            RTT::log(RTT::Error) << "enable_controller_output can only be called in Running() or Stopped&Initialized state"<<RTT::endlog();
        }
    }
    if ((*cstate)->vel_norm < safety_tolerance) {
        (*cstate)->vel_scale = 1.0;
        return true;
    } else {
        RTT::log(RTT::Error) << getName() << " : Joint velocities were to high to enable controller outputs" << RTT::endlog();
        return false;
    }
}

void etasl_rtt::scale_controller_output(double vel_scale) {
    if (this->getTaskState()!=Running) {
        if ((this->getTaskState()!=Stopped)||(!initialized)) {
            RTT::log(RTT::Error) << "scale_controller_output can only be called in Running() or Stopped&Initialized state"<<RTT::endlog();
        }
    }
    (*cstate)->vel_scale = vel_scale;
}

double etasl_rtt::normOutputVelocities() {
    return (*cstate)->vel_norm;
}

void etasl_rtt::cleanupHook() {
    initialized=false;
    etaslread=false;
    if (!ihc->detach_from_etasl())
        RTT::log(RTT::Error) << "I-handlers: " << errormsg::gen_io_error(errormsg::ERROR_IO_DETACH,errormsg::BOLDMAGENTA) << " - " << ihc->getErrorMsg() << RTT::endlog();
    if (!ohc->detach_from_etasl())
      RTT::log(RTT::Error) << "O-handlers: " << errormsg::gen_io_error(errormsg::ERROR_IO_DETACH,errormsg::BOLDMAGENTA) << " - " << ohc->getErrorMsg() << RTT::endlog();
    
    reinitialize_data_structures();
    RTT::log(RTT::Info) << "etasl_rtt::cleanupHook()" << RTT::endlog();
}

double etasl_rtt::get_etasl_value_double( const std::string& lua_statement ) {
    double value;
    int retval = lua->executeAndReturnDouble( lua_statement, value);
    if (retval!=0) {
        return std::numeric_limits<double>::quiet_NaN();
    } else {
        return value;
    }
}

std::string etasl_rtt::get_etasl_value_string( const std::string& lua_statement ) {
    std::string value;
    int retval = lua->executeAndReturnString( lua_statement, value);
    if (retval!=0) {
        return "" ;
    } else {
        return value;
    }
}


etasl_rtt::~etasl_rtt() {
    RTT::log(RTT::Info) << "etasl_rtt::destructor" << RTT::endlog();
}

/*
 * Using this macro, only one component may live
 * in one library *and* you may *not* link this library
 * with another component library. Use
 * ORO_CREATE_COMPONENT_TYPE()
 * ORO_LIST_COMPONENT_TYPE(etasl_rtt)
 * In case you want to link with another library that
 * already contains components.
 *
 * If you have put your component class
 * in a namespace, don't forget to add it here too:
 */
//ORO_CREATE_COMPONENT(etasl_rtt)
ORO_CREATE_COMPONENT_LIBRARY()
ORO_LIST_COMPONENT_TYPE(etasl_rtt)

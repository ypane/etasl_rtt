License
--------

LGPL v3, see LICENSE.txt

Necessary repositories:
-----------------------

Installation and setup instructions can be found in the expressiongraph_install repository (CATKIN branch).

Currently working on Ubuntu 14.04 / ROS Indigo.  Not yet tested on other configurations, any
experience testing it on other configurations is welcome.



```
git@gitlab.mech.kuleuven.be:u0002405/etasl_rtt.git   - master
expressiongraph                                      - CATKIN branch
expressiongraph_context                              - CATKIN branch
expressiongraph_context_lua                          - CATKIN branch
expressiongraph_examples_lua                         - CATKIN branch
expressiongraph_lua                                  - CATKIN branch
expressiongraph_ros                                  - CATKIN branch
expressiongraph_ros_component                        - CATKIN branch
expressiongraph_spline                               - CATKIN branch
git@gitlab.mech.kuleuven.be:rob-expressiongraphs/luabind.git -  master branch
```

The deploy_... scripts in the main directory deploy different examples.  The corresponding view_...
python scripts can be used to visualize some of the output of running these examples.

The scripts directory contains etasl scripts and scripts to construct lua components.

Running an example
------------------

  - ```cd scripts/deploy```
  - ```./deploy_move_jointspace.sh``` (to deploy with the Orocos TaskBrowser)
  - ```./deploy_move_jointspace.sh -h``` for more options 
  - look inside scripts/deploy/deploy_move_jointspace.sh for command line usage options

Showing the context
-------------------

  - run an example using the interactive lua deployer
  - ```=etasl:etasl_console()```
  - ```print(ctx)```

or

  - run an example using the default task browser
  - ```etasl.etasl_console()```
  - ```print(ctx)```


etasl_rtt operations
--------------------

```

 Listing TaskContext e[U] :

 Configuration Properties: 
        int max_iterations = 300                  (Maximum iterations of the optimizer at each time step)
     string solver_type    = qpoases              (solver to use, qpoases(default) or external )
     double max_cpu_time   = 1000                 (Maximum time to spend on the optimization at each time step)
     double regularization = 0.0001               (Regularization factor to be used during optimization)
     double initialization_time = 3                    (maximum simulated  time duration to be used to initialize feature variables)
     double initialization_time_step = 0.05                 (Simulated time step to be used to initialize feature variables)
     double convergence_criterion = 0.0001               (convergence criteria for the initialization of feature variables)
       bool debug_if_solver_error = false                (call a debug consol inside the context if a solver error occurrs (DEBUGGING, CAREFUL WHEN REAL ROBOT IS CONNECTED!) )
       bool disable_solver = false                (disable the solver but still compute the value of the expressions.  No feature variables allowed in specification.)
        int solve_max_iterations = 200                  (maximum iterations to use when calling solve())
     double solve_time_step = 0.1                  (time step to use when calling solve(), choose this together with the control gains in your etasl specification)
     double solve_convergence_crit = 1e-05                (converge_criterion to use when calling solve().  Will stop iterating if updates are smaller than this criterion)
     string event_postfix  = e                    (postfix to be used when emitting events)

 Provided Interface:
  Attributes   : (none)

 Data Flow Ports: 
 Out(C)      string eventPort      => 
 Out(C)      double elapsedTimePort => 0

 Services: 
  elapsedTimePort ( the elapsed time since the last call to updateHook ) 
  eventPort      ( the component publishes its event on this string port ) 

 Requires Operations :  (none)
 Requests Services   :  solver[!]  tf[!]

 Peers        : (none)


Operations:

 activate( ) : bool
   Activate the Execution Engine of this TaskContext.
 activation_command( string const& cmd ) : bool
   sends an grouping activation command to the context to activate a particular group
   cmd : activation command: comma or space seperated list of +groupname, -groupname, or +/-partial_name*
 add_activation_inputport( string const& portname, string const& portdocstring, string const& initial_cmd ) : bool
   adds an IOHandler that reads eTaSL activation commands from a (string) input port
   portname : name of the port
   portdocstring : documentation for the port
   initial_cmd : activation command to be given to eTaSL at initialization time
 add_controller_inputport( string const& portname, string const& portdocstring, strings const& jointnames ) : bool
   adds an IOHandler that read measured joint position values from an outputport
   portname : name of the port
   portdocstring : documentation for the port
   jointnames : list of jointnames describing the contents of the inputport
 add_controller_jointstate_inputport( string const& portname, string const& portdocstring, strings const& jointnames ) : bool
   adds an IOHandler that read measured jointstate position values from an outputport
   portname : name of the port
   portdocstring : documentation for the port
   jointnames : list of jointnames describing the contents of the inputport
 add_controller_jointstate_output( string const& portname, string const& portdocstring, strings const& jointnames ) : bool
   adds an IOHandler that puts the joint POSITIONS on an outputport using JointState::Messages
   portname : name of the port
   portdocstring : documentation for the port
   jointnames : list of jointnames describing the contents of the outputport
 add_controller_motion_control_msgs_joint_velocities_outputport( string const& portname, string const& portdocstring, strings const& jointnames ) : bool
   adds an IOHandler that puts the joint velocities on an outputport using JointState::Messages
   portname : name of the port
   portdocstring : documentation for the port
   jointnames : list of jointnames describing the contents of the outputport
 add_controller_outputport( string const& portname, string const& portdocstring, strings const& jointnames ) : bool
   adds an IOHandler that puts the computed joint velocities of the controller on an outputport
   portname : name of the port
   portdocstring : documentation for the port
   jointnames : list of jointnames describing the contents of the outputport
 add_controller_pos_outputport( string const& portname, string const& portdocstring, strings const& jointnames ) : bool
   adds an IOHandler that puts the joint POSITIONS on an outputport
   portname : name of the port
   portdocstring : documentation for the port
   jointnames : list of jointnames describing the contents of the outputport
 add_etaslvar_deriv_geometry_msgs_twist_inputport( string const& portname, string const& portdocstring, string const& varname ) : bool
   adds an IOHandler that gets derivative values from a port into eTaSL
   portname : name of the port
   portdocstring : documentation for the port
   varname : variable name describing the contents of the inputport
 add_etaslvar_deriv_inputport( string const& portname, string const& portdocstring, strings const& varnames ) : bool
   adds an IOHandler that gets derivative values from a port into eTaSL
   portname : name of the port
   portdocstring : documentation for the port
   varnames : list of variable names describing the contents of the inputport
 add_etaslvar_deriv_outputport( string const& portname, string const& portdocstring, strings const& varnames ) : bool
   adds an IOHandler that puts the velocity of an eTaSL output expression on an outputport
   portname : name of the port
   portdocstring : documentation for the port
   varnames : list of variable names describing the contents of the outputport
 add_etaslvar_deriv_twist_inputport( string const& portname, string const& portdocstring, string const& varname ) : bool
   adds an IOHandler that gets derivative values from a port into eTaSL
   portname : name of the port
   portdocstring : documentation for the port
   varname : variable name describing the contents of the inputport
 add_etaslvar_frame_inputport( string const& portname, string const& portdocstring, string const& varname, unknown_t const& defaultvalue ) : bool
   adds an IOHandler that gets values from a port into eTaSL
   portname : name of the port
   portdocstring : documentation for the port
   varname : variable describing the contents of the inputport
   defaultvalue : value describing the default value (can be empty)
 add_etaslvar_frame_outputport( string const& portname, string const& portdocstring, string const& varname ) : bool
   adds an IOHandler that gets values of type Frame from eTaSL to an outputport
   portname : name of the port
   portdocstring : documentation for the port
   varname : variable describing the contents of the outputport
 add_etaslvar_geometry_msgs_pose_inputport( string const& portname, string const& portdocstring, string const& varname, unknown_t const& defaultvalue ) : bool
   adds an IOHandler that gets values from a port into eTaSL
   portname : name of the port
   portdocstring : documentation for the port
   varname : variable describing the contents of the inputport
   defaultvalue : value describing the default value (can be empty)
 add_etaslvar_geometry_msgs_pose_outputport( string const& portname, string const& portdocstring, string const& varname ) : bool
   adds an IOHandler that sets values from eTaSL on a port
   portname : name of the port
   portdocstring : documentation for the port
   varname : variable describing the contents of the inputport
 add_etaslvar_inputport( string const& portname, string const& portdocstring, strings const& varnames, array const& defaultvalues ) : bool
   adds an IOHandler that gets values from a port into eTaSL
   portname : name of the port
   portdocstring : documentation for the port
   varnames : list of variable names describing the contents of the inputport
   defaultvalues : list of values describing the default value (can be empty)
 add_etaslvar_outputport( string const& portname, string const& portdocstring, strings const& varnames ) : bool
   adds an IOHandler that puts the an eTaSL output expression on an outputport
   portname : name of the port
   portdocstring : documentation for the port
   varnames : list of variable names describing the contents of the outputport
 add_transform_broadcast( strings const& parents, strings const& childs, strings const& varnames ) : bool
   broadcast a frame from etasl to tf
   parents : vector of the transforms parent frame names
   childs : vector of the transforms child frame names
   varnames : vector of the names of the etasl variable
 add_transform_lookup( string const& parent, string const& child, string const& varname ) : bool
   looks up a frame from tf and passes it to etasl
   parent : tfs parent frame
   child : tfs child frame
   varname : name of the etasl variable
 cleanup( ) : bool
   Reset this TaskContext to the PreOperational state ( =cleanupHook() ).
 configure( ) : bool
   Configure this TaskContext (= configureHook() ).
 disable_controller_output( ) : void
   the controller output velocities are disabled. Zero velocties are send out by this component
 enable_controller_output( double safety_tolerance ) : bool
   enable the controller output velocities.
   safety_tolerance : only enable when the norm of the velocities is below this level
 error( ) : void
   Enter the RunTimeError state (= errorHook() ).
 etasl_console( ) : bool
   goes to a lua console directly into the eTaSL environment, to interactivly query the specification
 getCpuAffinity( ) : uint
   Get the configured cpu affinity.
 getLocalTime( ) : double
   gets the local time of the component, i.e. since the start of the component
 getPeriod( ) : double
   Get the configured execution period. -1.0: no thread associated, 0.0: non periodic, > 0.0: the period.
 get_etasl_value_double( string const& lua_statement ) : double
   returns a double value given a lua statement that returns a value
   lua_statement : lua statement that returns a double
 get_etasl_value_string( string const& lua_statement ) : string
   returns a string value given a lua statement that returns a value
   lua_statement : lua statement that returns a string
 get_etaslvar( string const& varname ) : double
   gets the value of an eTaSL output variable of type double
   varname : name of the variable
 get_etaslvar_frame( string const& varname ) : unknown_t
   gets the value of an eTaSL output variable of type Frame
   varname : name of the variable
 get_etaslvar_rotation( string const& varname ) : unknown_t
   gets the value of an eTaSL output variable
   varname : name of the variable
 get_etaslvar_vector( string const& varname ) : unknown_t
   gets the value of an eTaSL output variable of type Vector
   varname : name of the variable
 inFatalError( ) : bool
   Check if this TaskContext is in the FatalError state.
 inRunTimeError( ) : bool
   Check if this TaskContext is in the RunTimeError state.
 initialize( ) : bool
   initializes feature variables with priority==0 constraints before starting
 isActive( ) : bool
   Is the Execution Engine of this TaskContext active ?
 isConfigured( ) : bool
   Is this TaskContext configured ?
 isRunning( ) : bool
   Is this TaskContext started ?
 list_etasl_inputs__rotation( ) : strings
   lists all rotation etasl input variables and returns them as a vector of strings
 list_etasl_inputs_all( ) : strings
   lists all etasl input variables and returns them as a vector of strings
 list_etasl_inputs_frame( ) : strings
   lists all frame etasl input variables and returns them as a vector of strings
 list_etasl_inputs_scalar( ) : strings
   lists all scalar etasl input variables and returns them as a vector of strings
 list_etasl_inputs_vector( ) : strings
   lists all vector etasl input variables and returns them as a vector of strings
 list_etasl_outputs__rotation( ) : strings
   lists all rotation etasl output variables and returns them as a vector of strings
 list_etasl_outputs_all( ) : strings
   lists all etasl output variables and returns them as a vector of strings
 list_etasl_outputs_frame( ) : strings
   lists all frame etasl output variables and returns them as a vector of strings
 list_etasl_outputs_scalar( ) : strings
   lists all scalar etasl output variables and returns them as a vector of strings
 list_etasl_outputs_vector( ) : strings
   lists all vector etasl output variables and returns them as a vector of strings
 loadService( string const& service_name ) : bool
   Loads a service known to RTT into this component.
   service_name : The name with which the service is registered by in the PluginLoader.
 normOutputVelocities( ) : double
   gives back the norm of the most recently computed output velocities
 printMatrices( ) : void
   prints out the matrices involved in solving for the current step (DEBUGGING)
 readTaskSpecificationFile( string const& filename ) : bool
   read an eTaSL task specification file (lua) 
   filename : the name of the file to read
 readTaskSpecificationString( string const& cmd ) : bool
   executes a fragment of an eTaSL specification string
   cmd : a fragment of eTaSL specification string
 scale_controller_output( double vel_scale ) : void
   scales the controller output velocities.
   vel_scale : scale factor for output vel., feature vel. and time progression
 setCpuAffinity( uint cpu ) : bool
   Set the cpu affinity.
   cpu : Cpu mask.
 setPeriod( double s ) : bool
   Set the execution period in seconds.
   s : Period in seconds.
 set_etaslvar( string const& varname, double value ) : bool
   sets the value of an eTaSL input variable
   varname : name of the variable
   value : value to be used
 set_etaslvar_frame( string const& varname, unknown_t const& value ) : bool
   sets the value of an eTaSL input variable of type KDL::Frame
   varname : name of the variable
   value : value to be used
 set_etaslvar_rotation( string const& varname, unknown_t const& value ) : bool
   sets the value of an eTaSL input variable of type KDL::Rotation
   varname : name of the variable
   value : value to be used
 set_etaslvar_vector( string const& varname, unknown_t const& value ) : bool
   sets the value of an eTaSL input variable of type KDL::Vector
   varname : name of the variable
   value : value to be used
 solve( strings const& jnames, array const& initial_val, array & values, double & result ) : int
   apply the constraints it is converged (i.e. inverse pos. kinematics)
   jnames : ordered list of joint names
   initial_val : initial values for the joint names, ignored if size==0
   values : resulting joint values after solving
   result : returns the weighted square of the slack variables, i.e. how much the (soft) constraints are violated
 start( ) : bool
   Start this TaskContext (= startHook() + updateHook() ).
 stop( ) : bool
   Stop this TaskContext (= stopHook() ).
 trigger( ) : bool
   Trigger the update method for execution in the thread of this task.
 Only succeeds if the task isRunning() and allowed by the Activity executing this task.
 update( ) : bool
   Execute (call) the update method directly.
 Only succeeds if the task isRunning() and allowed by the Activity executing this task.

```

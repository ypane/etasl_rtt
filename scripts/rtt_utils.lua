--
-- utilities for RTT
--
--  s,d : easy declaration of Orocos array of strings/doubles
--  report_all_ports:  report all ports of a component to a reporter
--  interactive(comp) : interactive access to a lua component, state machine or eTaSL component
--
require("rtt")
require("rttlib")
require("kdlutils")


--kdlutils has loaded KDL typekit:
Frame    = rtt.provides("KDL"):provides("Frame")
Rotation = rtt.provides("KDL"):provides("Rotation")
Vector   = rtt.provides("KDL"):provides("Vector")
Twist    = rtt.provides("KDL"):provides("Twist")
Wrench   = rtt.provides("KDL"):provides("Wrench")

-- to easily specify an Orocos array of strings:
function s( stringtable )
    local v = rtt.Variable("strings")
    v:fromtab(stringtable)
    return v
end

-- to easily specify an Orocos array of doubles:
function d( floattable )
    local v = rtt.Variable("array")
    v:fromtab(floattable)
    return v
end

-- to report all reasonable ports of a component to a reporter component.
function report_all_ports(depl,rpt, comp)
    depl:connectPeers(comp:getName(), rpt:getName())
    local prts = comp:getPortNames()
    for i=1, #prts do
        local port = comp:getPort(prts[i])
        if port:info()["porttype"]=="out" then
            local porttype = port:info()["type"]
            if porttype=="double" or porttype=="KDL.Frame" or porttype=="array" 
               or porttype=="KDL.Rotation" or porttype=="KDL.Vector" then
                    rpt:reportPort(comp:getName(),prts[i])
                    rtt.logl("Info","Reporting " .. prts[i])
            end
        end
    end 
end



function interactive(cmp)
    if tmp==nil then
        print("interactive(cmp):  go to an interactive session inside the given component")
        print("    cmp can be a state machine component, an rtt component, or an eTaSL component")
        print()
    end
    if cmp.exec_str~=nil then
        cmp:exec_str([[
            require("irttlua_recursive")
            run_ilua({prompt="RTT Comp. >> ",prompt2="RTT Comp. .. "})
            print("")
        ]])
        require("rtt_completer")  
        set_rlcompleter(_G)
        return
    end
    if cmp.readTaskSpecificationString~=nil then
        cmp:readTaskSpecificationString([[
            require("irttlua_recursive")
            run_ilua({prompt="ETASL >> ",prompt2="ETASL .. "})
            print("")
        ]])
        require("rtt_completer")  
        set_rlcompleter(_G)
        return
    end
    print("do not know how to go interactive with this component")
end



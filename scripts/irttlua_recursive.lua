#!/usr/bin/env rttlua
-- EA: added os package:
require "os"

require "pretty"
--
-- ilua.lua
--
-- A more friendly Lua interactive prompt
-- doesn't need '='
-- will print out tables recursively
--
-- Steve Donovan, 2007
-- Chris Hixon, 2010
--
-- Small adaptations by Erwin Aertbelien, 2011
--  (regarding history and completion of luabind classes)


-- create another environment, _E, to help prevent coding mistakes
require "rtt_completer"
change_completer=set_rlcompleter(_G)

readline = rlcompleter.readline
saveline = function (arg) 
                if arg~="" and arg~=nil then 
                    rlcompleter.add_history(arg) 
                end
            end


my_global = _G  -- changed by EA
local g = _G
local _E = {}
g.setmetatable(_E, {
    __index = g,
    --__newindex = function(t,k,v)
    --    return g.error("Attempt to assign to global variable '"..k.."'", 2)
    --end
})
if _E~=nil then 
    g.setfenv(1, _E)
end

-- imported global functions
local format = g.string.format
local sub = g.string.sub
local rep = g.string.rep
local match = g.string.match
local find = g.string.find
local sort = g.table.sort
local append = g.table.insert
local concat = g.table.concat
local write = g.io.write
local read = g.io.read
local flush = g.io.flush
local floor = g.math.floor
local print = g.print
local loadstring = g.loadstring
local type = g.type
local select = g.select
local setfenv = g.setfenv
local tostring = g.tostring
local getmetatable = g.getmetatable
local setmetatable = g.setmetatable
local pairs = g.pairs
local ipairs = g.ipairs
local rawset = g.rawset
local rawget = g.rawget
local require = g.require
local error = g.error
local dofile = g.dofile
local pcall = g.pcall

-- imported global vars
local debug = g.debug
local string = g.string
local os = g.os
local io = g.io
local arg = g.arg
local _VERSION = g._VERSION

-- local vars
-- local readline, saveline
local identifier = "^[_%a][_%w]*$"

--
-- local functions 
--

-- returns an array that is a slice of an array ary,
-- beginning at index b, ending at index e, with stride of s.
-- b, e, and s are optional and default to:
-- b = begging of array, e = end of array, s = 1
-- can also be used for reverse, like slice(a, #a, 1, -1)
local function slice(ary, b, e, s)
    local n, b, e, s = {}, b or 1, e or #ary, s or 1
    for i = b, e, s do
        n[#n + 1] = ary[i]
    end
    return n
end

-- trim whitespace from both ends of a string
local function trim(str)
    return str:gsub("^%s*(.-)%s*$", "%1")
end

-- function varsub(str, repl)
-- replaces variables in strings like "%20s{foo} %s{bar}" using the table repl
-- to look up replacements. use string:format patterns followed by {variable}
-- and pass the variables in a table like { foo="FOO", bar="BAR" }
-- variable names need to match Lua identifiers ([_%a][_%w]-)
-- missing variables or errors in formatting will result in empty strings
-- being inserted for the corresponding placeholder pattern
local function varsub(str, repl)
    return str:gsub("(%%.-){([_%a][_%w]-)}", function(f,k)
        local r, ok = repl[k]
        ok, r = pcall(format, f, r)
        return ok and r or ""
    end)
end

-- encodes a string as you would write it in code,
-- escaping control and other special characters
local function escape_string(str)
    local es_repl = { ["\n"] = "\\n", ["\r"] = "\\r", ["\t"] = "\\t",
        ["\\"] = "\\\\", ['"'] = '\\"' }
    str = str:gsub('(["\r\n\t\\])', es_repl)
    str = str:gsub("(%c)", function(c)
        return format("\\%d", c:byte())
    end) 
    return format('"%s"', str)
end

--
-- Ilua class
--

local Ilua = {}

-- defaults
Ilua.defaults = {
    -- evaluation related
    prompt = '>> ',         -- normal prompt
    prompt2 = '.. ',        -- prompt during multiple line input
    strict = true,          -- set to true to turn undeclared variable checks
    chunkname = "stdin",    -- name of the evaluated chunk when compiled
    result_var = "_",       -- the variable name that stores the last results
    verbose = false,        -- currently unused

    -- internal, for reference only
    savef = nil,
    line_handler_fn = nil,
    global_handler_fn = nil,
    num_prec = nil,
    num_all = nil,
}

-- things to expose to the enviroment
Ilua.expose = {
    ["Ilua"] = true, ["ilua"] = true, ["Pretty"] = true,
    ["p"] = true, ["ls"] = true, ["dir"] = true,
    ["slice"] = true
}

function Ilua:new(params)
    local obj = {}
    params = params or {}
    setmetatable(obj, self)
    self.__index = self
    obj:init(params)
    return obj
end

function Ilua:init(params)
    for k, v in pairs(self.defaults) do
        self[k] = v
    end
    for k, v in pairs(params) do
        self[k] = v
    end

    -- init collections
    self.collisions = {}
    self.lib = {}
    self.declared = {}

    -- setup environment, use global if requested 
    if self.global_env then
        self.env = g
    else -- else, if an environment was provided, use that, or create new one
        self.env = self.env or setmetatable({}, { __index = g })
    end
    self:setup_strict()

    -- expose some things to the environment 
    local expose = self.expose 
    self.env.Ilua = expose["Ilua"] and Ilua or nil
    self.env.ilua = expose["ilua"] and self or nil
    self.env.Pretty = expose["Pretty"] and Pretty or nil
    self.env.slice = expose["slice"] and slice or nil

    -- setup pretty print objects
    local oh = function(str)
        if str and str ~= "" then self:output(str) end
    end
    self.p = Pretty:new { output_handler = oh }
    self.env.p = expose["p"] and self.p or nil
    if not self.disable_ls then
        self.ls = Pretty:new { compact=true, depth=1, output_handler = oh }
        self.env.ls = expose["ls"] and self.ls or nil
    end
    if not self.disable_dir then
        self.dir = Pretty:new { compact=false, depth=1, key="%-20s",
            function_info=true, table_info=true, output_handler = oh }
        self.env.dir = expose["dir"] and self.dir or nil
    end
end

-- this is mostly meant for the ilua launcher/main
-- a separate Ilua instance may need to do something different so wouldn't call this
function Ilua:start()
    -- startup message
    if not self.disable_startup_message then
        self:output('ILUA: ' .. _VERSION)
        local jit = rawget(g, "jit") -- hack to show luajit info if it finds it
        if jit then
            local version = rawget(jit, "version")
            local arch = rawget(jit, "arch")
            self:output(version .. (arch and " ("..arch..")" or ""))
        end
    end

    -- to allow for configuration of the ilua instance, require a module 'ilua_instance' 
    if not self.disable_instance_config then
        pcall(function()    
            require 'ilua_instance'
        end)
    end

    -- transcript
    if self.file then
        print('saving transcript "'..self.file..'"')
        self.savef = io.open(self.file,'w')
        self.savef:write('! ilua ', concat(self.args,' '),'\n')
    end

    -- inject libs already loaded on command line 
    if self.inject_libs then
        for i, v in ipairs(self.inject_libs) do
            self:inject(unpack(v))
        end
    end

    -- load postponed libs
    if self.load_libs then
        for i, lib in ipairs(self.load_libs) do
            require(lib)
        end
    end

    -- import postponed libs
    if self.import_libs then
        for i, lib in ipairs(self.import_libs) do
            self:import(lib, true)
        end
    end

    -- any inject complaints?
    self:inject()

    -- load postponed files
    if self.load_files then
        for i, file in ipairs(self.load_files) do
            dofile(file)
        end
    end

    -- inject helpers
    if self.inject_helpers then
        self:helpers()
    end

    -- EA: load history
    -- rlcompleter.read_history(".ilua_history")
end

-- injects some shortcut variables and functions
function Ilua:helpers()
    local e = self.env
    -- object aliases
    for k, v in pairs { i = self, I = Ilua, P = Pretty, env = e, e = e } do
        e[k] = v
    end
    -- methods turned into functions
    for i, k in ipairs { 'vars', 'v' } do
        e[k] = function(...) return (self[k])(self, ...) end
    end
end
    
function Ilua:output(str)
    if self.savef then
        self.savef:write(str, "\n")
    end
    print(str)
end

function Ilua:precision(len,prec,all)
    if not len then self.num_prec = nil
    else
        self.num_prec = '%'..len..'.'..prec..'f'
    end
    self.num_all = all
end	

function Ilua:line_handler(handler)
    self.line_handler_fn = handler
end

function Ilua:global_handler(handler)
    self.global_handler_fn = handler
end

function Ilua:print_variables()
    for name,v in pairs(self.declared) do
        print(name,type(self.env[name]))
    end
end

Ilua.v = Ilua.print_variables
Ilua.vars = Ilua.print_variables

function Ilua:get_input()
    local lines, i, input, chunk, err = {}, 1
    while true do
        input = readline((i == 1) and self.prompt or self.prompt2)
        if not input then return end
        lines[i] = input
        input = concat(lines, "\n")
        chunk, err = loadstring(format("return(%s)", input), self.chunkname)
        if chunk then return input end
        chunk, err = loadstring(input, self.chunkname)
        if chunk or not err:match("'<eof>'$") then
            return input
        end
        lines[1] = input
        i = 2
    end
end

function Ilua:wrap(...)
    self.p(...)
    self.env[self.result_var] = select(1, ...)
end

function Ilua:eval_lua(line)
    if self.savef then
        self.savef:write(self.prompt, line, '\n')
    end
    -- is the line handler interested?
    if self.line_handler_fn then
        line = self.line_handler_fn(line)
        -- returning nil here means that the handler doesn't want
        -- Lua to see the string
        if not line then return end
    end
    -- EA: is it an OS call ? 
    if line:sub(1,1)=='!' then
        local a=os.execute(line:sub(2,-1))
        print("==> executed. exitvalue=" .. a)
        return
    end
    -- is it an expression?
    local chunk, err = loadstring(format("ilua:wrap(%s)", line), self.chunkname)
    if err then -- otherwise, a statement?
        chunk, err = loadstring(format("ilua:wrap((function() %s end)())", line), self.chunkname)
    end
    if err then
        self:output(err)
        return
    end
    -- compiled ok, evaluate the chunk
    -- ?? EA ??  setfenv(chunk, self.env)
    local ok, res = pcall(chunk)
    if not ok then
        self:output(res)
    end
end

-- require a lib and inject it into ilua namespace
function Ilua:import(lib, dont_complain)
    local tbl = g.require(lib)
    if type(tbl) ~= 'table' then
        tbl = g[lib]
    end
    self:inject(tbl, dont_complain, lib)
end

-- inject @tbl into the ilua namespace
function Ilua:inject(tbl, dont_complain, lib)
    lib = lib or '<unknown>'
    if type(tbl) == 'table' then
        for k,v in pairs(tbl) do
            local val = rawget(self.env, k)
            -- NB to keep track of collisions!
            if val and k ~= '_M' and k ~= '_NAME' and
                    k ~= '_PACKAGE' and k ~= '_VERSION' then
                self.collisions[k] = {lib, self.lib[k]}
            end
            self.env[k] = v
            self.lib[k] = lib
        end
    end
    if not dont_complain then
        for name, coll in pairs(self.collisions) do
            local lib, oldlib = coll[1], coll[2]
            write('warning: ',lib,'.',name,' overwrites ')
            if oldlib then
                write(oldlib,'.',name,'\n')
            else
                write('global ',name,'\n')
            end
        end
    end
end

--
-- checks uses of undeclared global variables (if strict is on)
-- All global variables must be 'declared' through a regular assignment
-- (even assigning nil will do) in a main chunk before being used
-- anywhere.
function Ilua:setup_strict()
    local mt = getmetatable(self.env)
    if mt == nil then
        mt = {}
        setmetatable(self.env, mt)
    end

    local function what ()
        local d = debug.getinfo(3, "S")
        return d and d.what or "C"
    end

    mt.__newindex = function (t, n, v)
        self.declared[n] = true
        rawset(t, n, v)
    end

    mt.__index = function (t, n)
        if not n then return end
        if not self.declared[n] and what() ~= "C" then
            local lookup = self.global_handler_fn and self.global_handler_fn(n)
            if lookup then return lookup end
            if not self.global_env then
                lookup = g[n]
                if lookup then return lookup end
            end
            if self.strict then
                error("variable '"..tostring(n).."' is not declared", 2)
            end
            return nil
        end
        return rawget(t, n)
    end
end

function Ilua:run()
    while true do    
        local input = self:get_input()
        if not input or trim(input) == 'quit' then break end
        self:eval_lua(input)
        saveline(input) 
        -- add each line save history to file.
        rlcompleter.write_history(".ilua_history")
    end

    if self.savef then
        self.savef:close()
    end
end

--
-- "main" from here down
--

local function quit(code,msg)
    io.stderr:write(msg,'\n')
    os.exit(code)
end



function g.run_ilua(params)

    if params==nil then
        params={}
    end


    -- expose the main classes to global env so modules/files included can see them
    g.Ilua = Ilua
    g.Pretty = Pretty

    -- try to bring in any ilua configuration files; don't complain if this is unsuccessful.
    -- note that the only things accessible at this point are the Ilua and Pretty classes,
    -- which should be good enough to set/override defaults
    -- (for configuring the default instance, use the module 'ilua_instance' - see Ilua:start())
    pcall(function()
        require 'ilua_system' -- system wide defaults
    end)
    pcall(function()
        require 'ilua_global' -- user defaults 
    end)

--[[    -- Unix readline support, if readline.so is available...
    local rl
    local err = pcall(function()
        rl = require('libreadline_c')
        readline = rl.readline
        saveline = rl.add_history
    end)
    if not rl then
        readline = function(prompt)
            write(prompt)
            return read()
        end
        saveline = function(s) end
    end
--]]

    -- EA:
    -- end of EA

    -- process command-line parameters
    if arg then
        params['args'] = arg
        local i = 1
        local postpone = true

        local function parm_value(opt,parm,def)
            local val = parm:sub(3)
            if #val == 0 then
                i = i + 1
                if i > #arg then 
                    if not def then
                        quit(-1,"expecting parameter for option '-"..opt.."'")
                    else
                        return def
                    end
                end
                val = arg[i]
            end
            return val
        end
        -- EA: set by default global_env and strict=false
        params['strict'] =  false
        params['global_env'] = true 
        --------------------------------------------------
        while i <= #arg do
            local v = arg[i]
            local opt = v:sub(1,1)
            if opt == '-' then
                opt = v:sub(2,2)			
                if opt == 'h' then
                    quit(0,"ilua [ [ -h | -g | -H | -l <lib> | -L <lib> | -t | -T | -s | -i | -p | <file> ] ... ]")
                elseif opt == 'g' then
                    params['global_env'] = true
                elseif opt == 'H' then
                    params['inject_helpers'] = true
                elseif opt == 'l' then
                    local lib = parm_value(opt,v)
                    if postpone then
                        params['load_libs'] = params['load_libs'] or {}
                        append(params['load_libs'], lib)
                    else
                        require(lib)
                    end
                elseif opt == 'L' then
                    local lib = parm_value(opt,v)
                    if postpone then
                        params['import_libs'] = params['import_libs'] or {}
                        append(params['import_libs'], lib)
                    else
                        local tbl = require(lib)
                        -- we cannot always trust require to return the table!
                        if type(tbl) ~= 'table' then
                            tbl = g[lib]
                        end
                        params['inject_libs'] = params['inject_libs'] or {}
                        append(params['inject_libs'], {tbl, true, lib})
                    end
                elseif opt == 't' then
                    params['file'] = parm_value(opt,v,"ilua.log")
                elseif opt == 'T' then
                    params['file'] = 'ilua_'..os.date ('%y_%m_%d_%H_%M')..'.log'
                elseif opt == 's' then
                    params['strict'] = false
                elseif opt == 'v' then
                    params['verbose'] = true
                elseif opt == 'i' then
                    postpone = false
                elseif opt == 'p' then
                    postpone = true
                end
            else -- a lua file to be executed immediately or later, depending on current value of postpone
                if postpone then
                    params['load_files'] = params['load_files'] or {}
                    append(params['load_files'], v)
                else
                    dofile(v)
                end
            end
            i = i + 1
        end
    end

    -- create an Ilua instance
    local ilua = Ilua:new(params)

    -- expose ilua to global environment so any modules/files loaded can see it
    g.ilua = ilua

    ilua:start()
    ilua:run()

end

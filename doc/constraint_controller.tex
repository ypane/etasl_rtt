\documentclass[10pt,a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage[left=2cm,right=2cm,top=2cm,bottom=2cm]{geometry}
\usepackage{listings}
\usepackage{color}

\definecolor{mygreen}{rgb}{0,0.6,0}
\definecolor{mygray}{rgb}{0.5,0.5,0.5}
\definecolor{mymauve}{rgb}{0.58,0,0.82}

\lstset{ %
  backgroundcolor=\color{white},   % choose the background color; you must add \usepackage{color} or \usepackage{xcolor}
  basicstyle=\scriptsize,        % the size of the fonts that are used for the code
  breakatwhitespace=false,         % sets if automatic breaks should only happen at whitespace
  breaklines=true,                 % sets automatic line breaking
  captionpos=b,                    % sets the caption-position to bottom
  commentstyle=\color{mygreen},    % comment style
  deletekeywords={...},            % if you want to delete keywords from the given language
  escapeinside={\%*}{*)},          % if you want to add LaTeX within your code
  extendedchars=true,              % lets you use non-ASCII characters; for 8-bits encodings only, does not work with UTF-8
  frame=single,	                   % adds a frame around the code
  keepspaces=true,                 % keeps spaces in text, useful for keeping indentation of code (possibly needs columns=flexible)
  keywordstyle=\color{blue},       % keyword style
  language=C++,                 % the language of the code
  otherkeywords={string,strings,array,*,...},           % if you want to add more keywords to the set
  numbers=none,                    % where to put the line-numbers; possible values are (none, left, right)
  numbersep=0pt,                   % how far the line-numbers are from the code
  numberstyle=\tiny\color{mygray}, % the style that is used for the line-numbers
  rulecolor=\color{black},         % if not set, the frame-color may be changed on line-breaks within not-black text (e.g. comments (green here))
  showspaces=false,                % show spaces everywhere adding particular underscores; it overrides 'showstringspaces'
  showstringspaces=false,          % underline spaces within strings only
  showtabs=false,                  % show tabs within strings adding particular underscores
  stepnumber=1,                    % the step between two line-numbers. If it's 1, each line will be numbered
  stringstyle=\color{mymauve},     % string literal style
  tabsize=4,	                   % sets default tabsize to 2 spaces
  title=\lstname                   % show the filename of files included with \lstinputlisting; also try caption instead of title
}
\setlength\parindent{0pt}
\newcommand{\etasl}[0]{eTaSL }

\title{A generic system for specifying constraint controllers within eTaSL}
\author{Erwin Aertbeli\"en}
\date{9 dec 2015}

\begin{document}
	\maketitle


\section{Introduction}
Currently the constraint controller is not flexible:
\begin{itemize}
    \item controller constants cannot be changed on-line.
    \item currently limited to a proportional controller, it would be useful to be able to define different controllers.
    \item no explicit way to deal with measurements. 
\end{itemize}
The \textbf{goal} is to resolve these problems while maintaining maximal 
compatibility with previous software using eTaSL.


\section{Controller}

Facilities needed:
\begin{itemize}
    \item expressiongraph functionality for wrenches and twists
    \item getRot() to translate rotations in a set of scalar constraints.
    \item be able to efficiently use the constraints upper and lower limits
\end{itemize}

\vspace{12pt}

The basic constraint specification contains the following concepts:
\begin{itemize}
    \item \emph{model} $L( \mathbf{q}, \boldsymbol{\chi}_{f}, t)$ describes an \emph{expression} that predicts the measurement from the robot joint state and feature variables. 
    \item \emph{measurement} $m$ describes an \emph{expression} the represents the measurement; none of its derivatives is ever used.  In purely geometric constraints (not using any measurement), this can be identical to the model.
    \item \emph{target\_upper} $d_{U}$ value for the upper bound constraint 
    \item \emph{target\_lower} $d_{L}$ value for the lower bound constraint. Can be equal to $d_U$ for an equality constraint.
\end{itemize}

\vspace{12pt}

For each of the upper and lower constraints a \emph{constraint controllers} is called.
The constraint controller generates a constraint in joint velocity space that is sent to the optimizer that computes the joint velocities.
It has the following inputs:
\begin{itemize}
    \item \emph{error} $e_L =  d_L-m$ and $e_U =  d_U - m$, the \emph{value} of the difference between the \emph{measurement} and the upper and lower target. 
    \item \emph{feedforward} $f$, the value computed by $-\frac{\partial L}{\partial t}$  
\end{itemize}

The constraint in joint velocity space (and feature space) is then expressed as:
\begin{align}
	C( e_L, -\frac{\partial L}{\partial t}) \leq
    \frac{\partial L}{\partial \mathbf{q}} \dot{\mathbf{q}} +
    \frac{\partial L}{\partial \boldsymbol{\chi}_f} \dot{\boldsymbol{\chi}}_f \leq
    C( e_U, -\frac{\partial L}{\partial t}),
\end{align}
where $C$ is a controller (possibly with an internal state).
The time derivative of $L$ includes all other dependencies to e.g. uncertainty variables, input trajectories.




Different uses for this controller:
\begin{itemize}
    \item The default controller $C$ is a proportional controller with
    feedforward.  It uses one control parameter $K$, and is equal to:
    \begin{align}
       C(e,f)= K( e ) + f,
    \end{align}
     If the model $L$ perfectly models the measurement $m$ and the controller output velocities are perfectly applied, the resulting behavior of the error $e$ is a first order linear system, with time constant $\tau=K^{-1}$.

    \item Geometric constraint:
    	\begin{itemize}
    		\item the model $L$ is equal to $e( \mathbf{q}, \boldsymbol{\chi}_{f}, t)$, an expression for the pose (or position) of the end effector.
    		\item the measurement $m$ is equal to $L$
    		\item target is the desired pose or position.
    	\end{itemize}
   

    \item A force controller $C$, with the force sensed at the
          end effector and the stiffness $\mathbf{K}_e$ localized at the end effector can be implemented with the default controller:
		\begin{itemize}
			\item model $L$ is equal to $\mathbf{K}_e  e( \mathbf{q}, \boldsymbol{\chi}_{f}, t)$,with $e$ an expression for the position deviation at the contact point (up to a constant, since the value is of no importance and only the Jacobian is important).
			\item measurement $m$ is equal to the measured force
			\item target is the desired force 
		\end{itemize}    
		
 	\item To estimate a force at a given point, when the force measurements at other
 	points are given.


\end{itemize}


\section{Integration within the software}
Principles:
\begin{itemize}
    \item Arbitrary number of parameters.
    \item Using a named list 
    \item Parameters are expressions.
    \item Controller refered to by name.
    \item Factories for the controller are registered in a registry.  Either creation by cloning a prototype
          or a factory class.
    \item Making sure that old-style constraint definitions are still valid, with the same semantics.
    \item The controller-model to adapt the right hand side of the constraints.
    \item Should be run-time efficient. Preferably no hash-lookups at run-time.
\end{itemize}



Content of a constraint:
\begin{itemize}
    \item \emph{name}: the name of the constraint,
    \item \emph{priority}:  0, 1 or 2.  0 and 1 are the same (hard) priority, but 0 is involved in the initialisation problem.  2 is a lower priority (soft).
    \item \emph{model}:  expression graph representing the model (only Jacobian is used)
    \item \emph{meas}: expressiongraph representing the measurement (only value is used)
    \item \emph{target\_lower}, \emph{target\_upper} : the C++ constraint class only accepts values for this.  The lua layer translates a \emph{target\_lower} and/or \emph{target\_upper} expression
          into an equivalent constraint accepted by the C++ constraint class. 
    \item \emph{controller\_upper}, \emph{controller\_lower}:  a pointer to a Controller class, this controller class contains its parameters using expression graphs.
    \item \emph{controller\_parameters}: a list of parameters.  The lua layer accepts also \emph{parameter}\_upper or ...\_lower to direct the parameters to the appropriate controller.
\end{itemize}

First implemented in branch \emph{controller} of the different packages, after compatibility testing, this will move to the main branch.

\end{document}


Refactoring the I/O-handlers
============================

This documents gives a quick summary of our redesign of the I/O-handler mechanism.

New methods in the I/O-handler abstract interface
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The I/O-handlers will have more granularity with respect to their initialization.  Previously
and "initialize" method was used.  The resulting separation is described as follows:

* configure_component: exposes RTT intefaces in the RTT-component (where needed for the I/O-handler). 
  This includes port creation, operation and property exposition.

* attach_to_etasl: makes sure that the internal datastructures of the I/O-handler 
  are attached to the underlying eTaSL data structure and context.

* initialize: one-call before 
  This can include setting port sizes

The following methods are , respectively:

* finish: eg, set joint values to 0;
* detach_from_etasl  : cleanup and detach from eTaSL.
* cleanup_component  : cleanup configuration of component that uses the I/O-handler


You can call configure_component() once, and then attach_to_etasl/detach_from_etasl multiple times.

Note that `initialize` and `finish` assume different meaning in the I/O handler life-cycle wrt the previous version.

The other methods of the I/O-handlers remain as previosly defineed(update, verify(), getPriorityLevel).

In practice, the constructors for the I/O-handlers will need to take a reference to the smart pointer.
(because, when using it in the etasl-rtt component,  the smart pointer will only be filled 
in later and can be NIL when the constructor for the I/O-handler is called).

Usage within the eTaSL-rtt component
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

1. construction of component
2. read specification file
3. add_xxxxx   (i.e. calling factory-methods for the I/O-handlers)

     - will create I/O-handler using new constructor
     - will call configure_component()

4. configure()

     - will call attach_to_etasl for all handlers 
     - will call verify

5. initialize()

     - will call update() of the I/O-handlers

6. start()
     - calls `initialize`
     - will call update() of the I/O-handlers

7. updateHook()

     - will call update() of the I/O-handlers

8. stopHook()
     - calls `finish`

9. cleanupHook()

With this set-up, we can switch step (2) and step(3).  This allows us to cleanup the eTaSL while keeping
the port configuration.


Refactoring dependent packages
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This affects the following packages:

- etasl_iohandler_geometry
- etasl_rtt
- etasl_iohandler_jointstate
- skin_etasl_integration/etasl_iohandler_skin  (ERWIN)
- pointcloud iohandler (ERWIN)

Current Refactoring Status
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

+-----------------------------------+-------------+-----------+-------------+
| Handler Name                      | Impl | Test |   Author  |  pkg        |
+===================================+=============+===========+=============+
| controller_inputport              | YES  | YES  |   Enea    |  etasl_rtt  |
+-----------------------------------+----- +------+-----------+-------------+
| controller_outport                | YES  | YES  |  Enea     |  etasl_rtt  |
+-----------------------------------+------+------+-----------+-------------+
| controller_pos_outputport         | YES  | NO   |  Enea     |  etasl_rtt  |
+-----------------------------------+------+------+-----------+-------------+
| etaslvar_inputport                | YES  | YES  |  Enea     |  etasl_rtt  |
+-----------------------------------+-------------+-----------+-------------+
| etaslvar_outputport               | YES  | YES  |  Enea     |  etasl_rtt  |
+-----------------------------------+-------------+-----------+-------------+
| etaslvar_rotation_outputport      | YES  | NO   |  Enea     |  etasl_rtt  |
+-----------------------------------+-------------+-----------+-------------+
| etaslvar_frame_inputport          | YES  | NO   |  Enea     |  etasl_rtt  |
+-----------------------------------+-------------+-----------+-------------+
| etaslvar_frame_outputport         | YES  | YES  |  Enea     |  etasl_rtt  |
+-----------------------------------+-------------+-----------+-------------+
| etaslvar_vector_outputport        | YES  | NO   |  Enea     |  etasl_rtt  |
+-----------------------------------+-------------+-----------+-------------+
| activation_inputport              | YES  | NO   |  Enea     |  etasl_rtt  |
+-----------------------------------+-------------+-----------+-------------+
| etaslvar_deriv_inputport          | YES  | NO   |  Enea     |  etasl_rtt  |
+-----------------------------------+-------------+-----------+-------------+
| etaslvar_deriv_outputport         | YES  | NO   |  Enea     |  etasl_rtt  |
+-----------------------------------+-------------+-----------+-------------+
| etaslvar_deriv_twist_inputport    | YES  | NO   |  Enea     |  etasl_rtt  |
+-----------------------------------+-------------+-----------+-------------+
| controller_jointstate_inputport   | YES  | YES  |  Enea     | jointstate  |
+-----------------------------------+------+------+-----------+-------------+
| controller_jointstate_outputport  | YES  | YES  |  Enea     | jointstate  |
+-----------------------------------+------+------+-----------+-------------+
| geometry_msgs_pose_inputport      | YES  | YES  |  Enea     | geometry    |
+-----------------------------------+------+------+-----------+-------------+
| geometry_msgs_pose_outputport     | YES  | YES  |  Enea     | geometry    |
+-----------------------------------+------+------+-----------+-------------+
| geometry_msgs_twist_inputport     | YES  | YES  |  Enea     | geometry    |
+-----------------------------------+------+------+-----------+-------------+
| transform_broadcast               | YES  | YES  |  Enea     | geometry    |
+-----------------------------------+------+------+-----------+-------------+
| transform_lookup                  | YES  | YES  |  Enea     | geometry    |
+-----------------------------------+------+------+-----------+-------------+


